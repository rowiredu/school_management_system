<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Entrance University College of Health Science </title>

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <style>
        .loginColumns {
            max-width: 500px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }

        .logininfo {
            color: navy;
            font-size: 24px;
        }
    </style>
</head>

<body class="gray-bg">
<?php $this->load->view('uniform'); ?>

    <div class="loginColumns animated fadeInDown">
        <!--img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/-->
        
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <center>
                        <h1><?php echo lang('forgot_password_heading');?></h1>
                        <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                    </center>
                    
                    <div id="infoMessage" class="logininfo"><?php echo $message;?> </div>
                    <?php echo form_open("admission/auth/forgot_password");?>
                        <div id="infoMessage"><?php echo $message;?></div>
                        <div class="form-group">
                        <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
                            <?php echo form_input($identity , '' , 'class="form-control"');?>
                        </div>

                        <div class="form-group row m-t-20">
                            <?php echo form_submit('submit', lang('forgot_password_submit_btn') , array('class' => 'btn btn-primary block full-width m-b '));?>
                        </div>
                        
                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <a class="btn btn-sm btn-white btn-block" href="<?=site_url('admission/auth/login')?>">login your account</a>
                                <a class="btn btn-sm btn-white btn-block" href="<?=site_url('admission/auth/register')?>">Create an account</a>
                            </div>
                        </div>
                    <?php echo form_close();?>
                    <p class="m-t">
                        <small>Powered by Zentech</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-10">
                &copy; Copyright Entrance University College of Health Science
            </div>
            <div class="col-md-2 text-right">
               <small>© <?=date('Y')?></small>
            </div>
        </div>
    </div>

</body>


</html>
