<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Entrance University College of Health Science </title>

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <style>
        .loginColumns {
            max-width: 700px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }


        .logininfo {
            color: #f41111b0;
            background: #8080802e;
            font-size: 14px;
            margin: 15px;
            padding: 15px;
            text-align: center;
        }

        #container {
        width: 100%;
        height: 100%;
        position: relative;
        /* The image used */
        background-image: url(<?=base_url('assets/bg.jpg')?>);

        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        }

        #navi,
        #infoi {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #navi {
            z-index: 10;
        }

        #infoi {
            overflow: hidden;
            bottom: -1px;
            background: #000;
            opacity: 0.1;
        }

        .ibox-content {
            border-radius: 20px;
        }

    </style>
</head>

<body id="container">
    
    <?php $this->load->view('uniform'); ?>

    <div id="infoi">
    </div>


    <div class="loginColumns animated fadeInUp">
        <!--img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/-->
        
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <center>
                        <h1 style="color:navy;font-weight:500;">Create an account</h1>
                    </center>
                    <div id="infoMessage" class="logininfo">
                        <?php 
                            echo $message; 
                            echo validation_errors(); 
                        ?> 
                    </div>

                    <?=form_open("admission/auth/register");?>
                        <div id="infoMessage"><?php echo $message;?></div>

                        <div class="form-group col-md-6">
                            <label><?php echo lang('create_user_fname_label', 'first_name');?></label>
                            <?php echo form_input($first_name ,'','class="form-control"');?>
                        </div>
                        <div class="form-group col-md-6">
                            <label> <?php echo lang('create_user_lname_label', 'last_name');?>  </label>
                            <?php echo form_input($last_name ,'','class="form-control"');?>
                        </div>
                        <div class="form-group col-md-6">
                            <label> <?php echo lang('create_user_email_label', 'email');?>* </label>
                            <?php echo form_input($email ,'','class="form-control"');?>
                        </div>

                        <div class="form-group col-md-6">
                            <label style="width:100%;"> <?php echo lang('create_user_phone_label', 'phone');?>  </label>
                            <span style="clear:both"></span>
                            <select class="form-control select2 col-md-3" style="width: 20%;float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 0px 0px 20px;" name="phone_code" required>
                                <option value="233"> 233 </option>
                                <optgroup label="Country">
                                    <?php foreach($countries as $key => $values){ ?>
                                        <option value="<?=$values->phonecode?>" > <?=$values->phonecode?> </option>
                                    <?php } ?>
                                </optgroup>
                            </select>
                        
                            <?php echo form_input($phone ,'','class="form-control" style="width: 80%; float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 20px 0px 0px;"');?>
                        </div>
                        <div class="form-group col-md-6">
                            <label> <?php echo lang('create_user_password_label', 'password');?>  </label>
                            <?php echo form_input($password ,'','class="form-control"');?>
                        </div>
                        <div class="form-group col-md-6">
                            <label> <?php echo lang('create_user_password_confirm_label', 'password_confirm');?>  </label>
                            <?php echo form_input($password_confirm ,'','class="form-control"');?>
                        </div>
                        <div class="form-group" style="text-align:center;">
                            <label> 
                                male 
                                <input type="radio" name="gender" value="male" />
                                <span class="checkmark"></span>
                            </label>
                            <label> 
                                female  
                                <input type="radio" name="gender" value="female" />
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group" style="text-align:center;">
                            <div class="checkbox i-checks"><label> <input type="checkbox" required><i></i> Agree the terms and policy </label></div>
                        </div>
                        <?php echo form_submit('submit', lang('create_user_submit_btn') , 'class="btn btn-primary block full-width m-b"');?>

                        <p class="text-muted text-center"><small>Already have an account?</small></p>
                        <a class="btn btn-sm btn-white btn-block" href="<?=site_url('admission')?>">Login</a>
                    <?php echo form_close();?>
        
                    <p class="m-t">
                        <small>Powered by Zentech</small>
                    </p>
                    <div class="row">
                        <div class="col-md-10">
                        &copy; <small>Copyright Entrance University College of Health Science</small></div>
                        <div class="col-md-2 text-right">
                        <small>© <?=date('Y')?></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<hr/>-->
    </div>

</body>


</html>