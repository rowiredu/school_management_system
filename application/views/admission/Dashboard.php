<style>
    * {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    }

    body {
        background: linear-gradient(243.34deg, #f8f8fc 40.27%, rgba(255, 255, 255, 0) 40.38%) repeat scroll 0% 0%, white none repeat scroll 0% 0%;
    }

    .p-dp {
        /*height: 220px;
        width: 180px;*/
        border-radius: 100%;
        border: 1px solid #e0e5ec;
        width: 120px;
        height: 120px;
        object-fit: cover;
        padding: 5px;
        /*box-shadow: 0px 60px 90px 0px #f4f4f4;*/
    }

    .img-container {
        z-index: 1000000;
        opacity: 0.85;
    }

    .profile-overview {
        float: right;
    }

    .apply-btn {
        padding: 15px;
        font-size: 25px;
        font-weight: bold;
    }

    .ibox-content, .social-feed-box {
        /*box-shadow: 0px 30px 10px #f6f6f6;*/
        border-radius: 20px;
        border: none;
    }

/**abstract design 
    .ibox-con {
        background: linear-gradient(243.34deg, #f8f8fc 40.27%, rgba(255, 255, 255, 0) 40.38%) repeat scroll 0% 0%, white none repeat scroll 0% 0%;
    }
/**end of abstract design */

    .social-feed-box {
        box-shadow: 0px 120px 30px 0px #f1f1f1;
    }

    ._icons {
        font-size: 16px;
        color: #f1f1f1;
        margin-right: 18px;
    }

    .shift-right {
        float: right;
        color: #676a6c;
    }

    .hd-rs {
        border-bottom: 20px solid #f6f6f6;
        padding-bottom: 8px;
        color: #4a6767;
        /*color: black;*/
        opacity: 0.8;
    }

    .hrs {
        border: none;
    }

    .sub-hds {
        opacity: 0.8;
    }

    .app-status {
        border-radius: 20px;
        background-image: linear-gradient(#5e5dbc, #5e5dbc, #4443a2);
        border: none;
        box-shadow: 0px 10px 10px -3px #c9c9e9;
        padding: 8px;
        margin-bottom: 20px;
    }

    .how-to-container {
        background: #35347f;
        border-radius: 20px;
    }

    .how-to {
        /*background: #5e4ccc;*/
        background: linear-gradient(-202.34deg, #5756b9 40.27%, rgba(255, 255, 255, 0) 40.38%) repeat scroll 0% 0%, #5a59bb none repeat scroll 0% 0%;
        padding-top: 8px;
        padding-bottom: 8px;
        color: #fff;
        opacity: 0.9;
        border-radius: 2px;
        border-radius: 16px;
        box-shadow: 0px 10px 20px -15px #8281cb, 0px -5px 20px -15px #8281cb;
    }

    .how-to-header {
        padding: 8px;
        background: #35347e;
        /*background:#4443a2;*/
        border-radius: 16px;
        color: white;
        opacity: 0.8;
        width: 100%;
        border: none;
        text-align: center;
    }

    .htl {
        line-height: 1.5;
        margin-bottom: 5px;
    }

    .htl:hover {
        font-weight: bold;
    }

    .progress-bar {
        background: #5e5dbc;
        border-radius: 10px;
        box-shadow: 0px 30px 20px -15px #8281cb, 0px -5px 20px -15px #8281cb;
    }

    .progress {
        border-radius: 10px;
    }

    .awaiting {
        font-size: 20px;
        padding: 8px;
        background: #a5a5da;
        border-radius: 20px;
    }

    .rs {
        border: none;
        color: #4a6767;
        opacity: 0.9;
    }

    .fa-exclamation-triangle{
        padding-right: 15px;
        font-size: 26px;
        color: #ffb3b3;
        box-shadow: 0px 30px 20px -15px #ff4d4d, 0px -5px 20px -15px #ffb3b3;
    }

    .background-logo {
        position: absolute;
        bottom: 20px;
        right: 0;
        opacity: 0.919;
        overflow: hidden;
    }

    .overlayed {
        z-index: 1;
    }

    #logo-image {
        width: 500px;
    }

    .navbar-default {
        background: linear-gradient(202.34deg, white 40.27%, rgba(41, 30, 102,0) 40.38%) repeat scroll 0% 0%, #fafafa none repeat scroll 0% 0%;
    }

</style>
        <?php 
            if(isset($form_data)){
                if($form_data->status == '')
                {
                    $l = (object)array_merge(array('user' => $user_details),array('others' => $ind_prop));
                    $l->user->date_of_birth = '';
                }
                else
                {
                    $l = (object)json_decode($form_data->json_values);
                }
            }
        ?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Dashboard</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?=base_url();?>">Home</a>
                    </li>
                    <li>
                        <a>Admissions</a>
                    </li>
                    <li class="active">
                        <strong>Dashboard</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="ibox">
                                <div class="img-container">
                                    <img src="<?=$user_picture?>" class="p-dp" alt="profile" data-toggle="modal" data-target="#profile_picture">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div class="social-feed-box fw-background">
                                <div class="ibox-content ibox-con">
                                    <h3 class="hd-rs"><span>Welcome, </span><strong><?=$user_details->first_name?>  <?=$user_details->last_name?>!</strong></h3>
                                    <!--<h4><span>Welcome </span><strong><?=$user_details->first_name?>  <?=$user_details->last_name?></strong></h4>-->
                                    <p><i class="fa fa-envelope _icons"></i>Email address :- <?=$user_details->username?>  </p>
                                    <p><i class="fa fa-calendar _icons"></i>Date of birth :- <?= isset($l->user->date_of_birth) ? $l->user->date_of_birth : '';?>  </p>
                                    <p><i class="fa fa-phone _icons"></i>Mobile number :- <?=$user_details->phone?>  </p>
                                    <p><i class="fa fa-info-circle _icons"></i>Biography :- <?=$user_details->bio?>  </p>
                                </div>
                            </div>
                            <div class="social-feed-box">
                                <div class="ibox-content ibox-con">
                                    <h3 class="hd-rs hrs">Progress of your application</h3>
                                    
                                    <?php
                                        if(isset($form_data->active_form))
                                        {
                                            if($form_data->active_form < 5 && $form_data->status == 'in_progress')
                                            {
                                                echo '<div class="progress" style="padding: 2px;"><div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="' . ($form_data->active_form - 1) * 20 . '" aria-valuemin="0" aria-valuemax="100" style="width: ' .($form_data->active_form - 1) * 20 . '%"></div></div>'.
                                                '<div background: opacity: 0;"><strong>Completed </strong><span>'. ($form_data->active_form - 1) * 20 .'.0%</span></div>';
                                            }
                                            else if($form_data->active_form == 5 && $form_data->status == 'in_progress')
                                            {
                                                echo '<div class="progress" style="padding: 2px;"><div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div></div>'.
                                                '<div background: opacity: 0;"><strong>Completed </strong><span>80.0%</span></div>';
                                            }
                                            else if($form_data->active_form == 5 && $form_data->status == 'submitted')
                                            {
                                                echo '<div class="progress" style="padding: 2px;"><div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>'.
                                                '<div background: opacity: 0;"><strong>Completed </strong><span>100.0%</span></div>';
                                            }
                                        }else 
                                        {
                                            echo '<div class="progress" style="padding: 2px;"><div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background: #edebf9;"></div></div>'.
                                            '<div background: opacity: 0;"><strong>Completed </strong><span>0.0%</span></div>';

                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="row centered">
                                <div class="col-lg-6">
                                <?php
                                if($within_admission == false){
                                    echo "<div class='social-feed-box col-md-12'><div class='ibox-content' style='display: flex; flex-direction: row;'><i class='fa fa-exclamation-triangle'></i><h4 class='rs'>Waiting for admission opening</h4></div></div>";
                                }
                                ?>
                                <h2 class="no-margins">
                                        <?php 
                                             if($within_admission != false){
                                                if($form_data->status == ''){
                                                    echo '<a href="'.site_url('admission/admission').'" class="btn btn-primary btn-block app-status"><i class="fa fa-share"> </i> Apply for '.$within_admission->year.'</a>';
                                                } else if($form_data->status == 'in_progress') {
                                                    echo '<a href="'.site_url('admission/admission').'" class="btn btn-primary btn-block app-status">Continue Application for '.$within_admission->year.'</a>';
                                                } else  {
                                                    echo '<a href="'.site_url('admission/admission').'" class="btn btn-primary btn-block app-status"><i class="fa fa-share"> </i> Check Status  for '.$within_admission->year.'</a>';
                                                }
                                            }
                                        ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 m-b-lg">
                    <div class="social-feed-box">
                    <div class="background-logo"><div class="logo"><img id="logo-image" src="<?=base_url('assets/logo_part.png')?>"></div></div>
                        <div class="ibox-content how-to">
                        <h3 class="hd-rs how-to-header">How to apply</h3>
                            <h5 class="sub-hds">To apply for an admission year, please follow these steps.</h5>
                            <div class="how-to-container">
                                <ul class="how-to">
                                    <li class="htl">Click on the Apply for admission year button.</li>
                                    <li class="htl">Fill out your First name, last name, middle names, sex, marital status, place of birth, nationality, no. of children, email, phone, office, fax, residential area, permanent address.  </li>
                                    <li class="htl">Click Save and continue.</li>
                                    <li class="htl">Fill out your emergency contact one, emergency contact two, source of information, present occupation, is English your first language, fill out do you have any special needs or require support as a consequence of any disability or medical conditions?</li>
                                    <li class="htl">Click Save and continue.</li>
                                    <li class="htl">Select your first programme choice, second programme choice, third programme choice.</li>
                                    <li class="htl">Click Save and continue.</li>
                                    <li class="htl">Select your certification, field of study, name of institution, institution type, year completed, certification and fill out your subjects and their corresponding grades.</li>
                                    <li class="htl">Click Save and continue.</li>
                                    <li class="htl">Apply your pin</li>
                                    <li class="htl">Press submit to submit your forms.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


