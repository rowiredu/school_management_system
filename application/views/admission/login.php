<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Entrance University College of Health Science </title>

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <style>
        #container {
            width: 100%;
            height: 100%;
            position: relative;
            /* The image used */
            background-image: url(<?=base_url('assets/bg.jpg')?>);

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            margin: 0;
            padding: 0;
        }

        #navi,
        #infoi,
        #cover2 {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #navi {
            z-index: 10;
        }

        #infoi {
            overflow: hidden;
            bottom: -1px;
            background: black;
            opacity: 0.1;
        }

        #cover2 {
            z-index: -1;
        }

        .loginColumns {
            max-width: 500px;
        }

        .ibox-content {
            border-radius: 20px;
        }

    </style>
</head>

<body id="container">
    <?php $this->load->view('uniform');?>
    
    <div id="infoi">
    </div>

    <!--
    <div id="cover2" class="animated fadeInDown">
        <svg viewBox="0 0 1920 890" preserveAspectRatio="xMidYMid meet" text-rendering="geometricPrecision"><defs><clipPath id="favicon-clip-5"></clipPath><clipPath id="favicon-clip-4"></clipPath><clipPath id="favicon-clip-3"></clipPath><clipPath id="favicon-clip-2"><circle r="500" cx="512.5" cy="16.5"></circle></clipPath><clipPath id="favicon-clip-1"></clipPath></defs><g transform="translate(960,490)"><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(199)"><circle r="11" cx="832.9920000000001" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(194.5)"><circle r="27" cx="684" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(173.1)"><circle r="14" cx="813.12" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(157.9)"><circle r="11" cx="745.9200000000001" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(337)"><circle r="18" cx="854.4" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(351.6)"><circle r="13" cx="734.4" cy="0" fill="#253569"></circle></g></g><g class="ghost-wrap" transform="translate(0, 0)"><g class="ghost" transform="rotate(16)"><circle r="25" cx="768" cy="0" fill="#253569"></circle></g></g></g><g transform="translate(960,540)"><g class="planet-wrap"><g class="planet" transform="rotate(14.999999999999998)" fill-opacity="1.00" stroke-opacity="0.419"><circle class="planet-circle" r="10" cx="360" cy="0" fill="#253569" transform="translate(16.96327990158043, 0) scale(0.9528797780511654,0.9528797780511654)"></circle><g class="text-wrap" transform="translate(12.266702535935394, 93.17485623690746) rotate(-14.999999999999998)"><circle r="9.5" cx="387.5" cy="16.5" fill="#fff"></circle></g><rect class="planet-overlay" pointer-events="all" x="350" y="-13.7890625" width="95.64459228515625" height="27.578125" transform="translate(12.266702535935394, 93.17485623690746) rotate(-14.999999999999998)" style="fill: transparent;"></rect></g></g><g class="planet-wrap"><g class="planet" transform="rotate(151.99999999999997)" fill-opacity="1.00" stroke-opacity="0.419"><circle class="planet-circle" r="20" cx="455" cy="0" stroke="#1c2752" stroke-width="18" fill="red" transform="translate(0, 0) scale(1,1)"></circle><g class="text-wrap" transform="translate(856.7411547508117, 213.60956106758044) rotate(-151.99999999999997)"></g></g></g><g class="planet-wrap"><g class="planet" transform="rotate(180)" fill-opacity="1.00" stroke-opacity="0.419"><circle class="planet-circle" r="30" cx="550" cy="0" stroke="#1c2752" stroke-width="18" fill="#4494eb" transform="translate(0, 0) scale(1,1)"></circle><g class="text-wrap" transform="translate(1100, 6.394884621840902e-14) rotate(180)"><circle r="9.5" cx="597.5" cy="16.5" fill="#fff"></circle></g></g></g><g class="planet-wrap"><g class="planet" transform="rotate(-139)" fill-opacity="1.00" stroke-opacity="0.419"><circle class="planet-circle" r="40" cx="455" cy="0" stroke="#1c2752" stroke-width="500" fill="#edebf9" transform="translate(0.1708885832637037, 0) scale(0.9996244207114989,0.9996244207114989)"></circle><g class="text-wrap" transform="translate(798.3928590013612, -298.5068581906809) rotate(139)"><circle r="9.5" cx="512.5" cy="16.5" fill="#fff"></circle></g></g></g><g class="planet-wrap"><g class="planet" transform="rotate(-18.000000000000014)" fill-opacity="1.00" stroke-opacity="0.419"><circle class="planet-circle" r="50" cx="550" cy="0" stroke="#1c2752" stroke-width="120" fill="#ea514d" transform="translate(23.96418843991041, 0) scale(0.9564287498256921,0.9564287498256921)"></circle><g class="text-wrap" transform="translate(26.918916037665554, -169.95934690622119) rotate(18.000000000000014)"></g></g></g></g></svg>    
    </div>
    -->
        
    <div class="loginColumns animated fadeInUp">
        <!--img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/-->
        
        <div class="row">
            <div class="col-md-12 bordered">
                <div class="ibox-content">
                    <div id="infoMessage" class="logininfo"><?php echo $message;?> </div>

                    <?php echo form_open("admission/auth/login");?>
                        <div class="form-group">
                            <label for="identity"> <?php echo _l('E-mail');?> </label>
                            <?php echo form_input($identity , '' , 'class="form-control"');?>
                        </div>

                        <div class="form-group">
                            <label for="pin"><?php echo lang('login_password_label', 'password');?></label>
                            <?php echo form_input($password , '' , 'class="form-control" ');?>
                        </div>

                        <div class="form-group m-t-20">
                            <div class="custom-control custom-checkbox">
                                <?php echo form_checkbox('remember', '1', FALSE, 'class="custom-control-input" id="customControlInline"');?>
                                <label class="custom-control-label" for="customControlInline">Remember me</label>
                            </div>
                        </div>

                        <div class="form-group m-t-20">
                            <?php echo form_submit('submit', lang('login_submit_btn') , array('class' => 'btn btn-primary block full-width m-b '));?>
                        </div>

                        <div class="form-group m-t-10 mb-0">
                            <div class="m-t-20">
                                <a href="<?=site_url('admission/auth/forgot_password')?>" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                <a class="btn btn-sm btn-white btn-block" href="<?=site_url('admission/auth/register')?>">Create an account</a>
                            </div>
                        </div>
                    <?php echo form_close();?>
        
                    <p class="m-t">
                        <small>Powered by Zentech</small>
                    </p>
                    <div class="row">
                        <div class="col-md-10">
                        &copy; <small>Copyright Entrance University College of Health Science</small></div>
                        <div class="col-md-2 text-right">
                        <small>© <?=date('Y')?></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


</html>
