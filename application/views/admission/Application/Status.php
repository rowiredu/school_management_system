    <?php
    /*
        status == in_progress || failed_to_submit       == 20
        status == submitted                             == 40    
        status == qualified || unqualified            == 90
        status == accepted                              == 100%
        bg-danger 

    */
        $color = "";
        if(isset($form_data)) :
            switch ($form_data->status) {
                case 'in_progress':
                    $relevant_val = '50';
                    $message = 'In process of filling the forms';
                    break;
                case 'failed_to_submit':
                    $relevant_val = '0';
                    $message = 'You have failed to submit your forms';
                    $color = 'style="background:#ab0d1b;"';
                    break;
                
                case 'submitted':
                    $relevant_val = '100';
                    $message = ' You have successfully submitted your forms, Admission is Pending';
                    break;
                
                case 'qualified':
                    $relevant_val = '100';
                    $message = ' You have been successfully admitted into Entrance University College of Health Science';
                    break;
                
                case 'unqualified':
                    $relevant_val = '100';
                    $message = "We are sorry you didn't meet up with the entry requirement";
                    $color = 'style="background:#ab0d1b;"';
                    break;
                
                case 'accepted':
                    $relevant_val = '100';
                    $message = "Thanks for Accepting the admisison into Entrance University College of Health Science, We wish you a wonderful learning experience";
                    $color = 'style="background:#ab0d1b;"';
                    break;
                
                default:
                    $relevant_val = '1';
                    $message = "Sorry";
                    $color = 'style="background:#ab0d1b;"';
                    break;
            }
        endif;
    ?>



        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2> <?=$within_admission->year?> Admission </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="#!">Home</a>
                    </li>
                    <li class="active">
                        <strong> Status  </strong>
                    </li>
                    <?php
                        if(isset($form_data)) :
                            switch ($form_data->status) {
                                case 'in_progress':
                                    if((int)$form_data->payment_id !== 0){
                                        ?>
                                            <li> <a href="<?=site_url('admission/admission/submission')?>" onclick="javascript:return confirm('Are you sure you want to submit this Document? \n\n\n There is no going back.')"> <i class="fa fa-diamond"></i> Submit Forms</a></li>
                                        <?php
                                    }
                                    break;
                                case 'qualified':
                                    ?> <li> <a href="<?=site_url('admission/admission/acceptance')?>" onclick="javascript:return confirm('Are you sure you want to accept this Document? ')"> <i class="fa fa-diamond"></i> Accept Admission</a> </li> <?php
                                    break;
                                case 'accepted':
                                    ?> Congratulation <?php
                                    break;
                            }
                        endif;
                    ?>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInUp">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-b-md">
                                        <!--a href="#" class="btn btn-white btn-xs pull-right">Edit Status</a-->
                                        <h2> <?=$user_details->first_name?>  <?=$user_details->last_name?>  </h2>
                                        <a href="<?=site_url('admission/admission/printing')?>" class="btn btn-primary"><i class="fa fa-print"> </i> Print Application</a>
                                    </div>
                                    <dl class="dl-horizontal">
                                        <dt>Status:</dt> <dd><span class="label label-primary"><?=humanize($form_data->status)?></span></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>info:</dt> <dd><span class="label label-primary"  ><?=$message?></span></dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">

                                        <dt>Applicant Name :</dt> <dd> <?=$user_details->first_name?>  <?=$user_details->last_name?> </dd>
                                        <dt>Bio:</dt> <dd>  <?=$user_details->bio?></dd>
                                    </dl>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        <dt>Completed:</dt>
                                        <dd>
                                            <div class="progress progress-striped active m-b-sm">
                                                <div style="width: <?=$relevant_val?>%" class="progress-bar" <?=$color?>></div>
                                            </div>
                                            <small>
                                                Completed 
                                                <strong><?=$relevant_val?>%</strong>
                                            </small>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--div class="col-lg-3">
                <div class="wrapper wrapper-content project-manager">
                    <h4>Project description</h4>
                    <img src="img/zender_logo.png" class="img-responsive">
                    <p class="small">
                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look
                        even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing
                    </p>
                    <p class="small font-bold">
                        <span><i class="fa fa-circle text-warning"></i> High priority</span>
                    </p>
                    <h5>Project tag</h5>
                    <ul class="tag-list" style="padding: 0">
                        <li><a href="#"><i class="fa fa-tag"></i> Zender</a></li>
                        <li><a href="#"><i class="fa fa-tag"></i> Lorem ipsum</a></li>
                        <li><a href="#"><i class="fa fa-tag"></i> Passages</a></li>
                        <li><a href="#"><i class="fa fa-tag"></i> Variations</a></li>
                    </ul>
                    <h5>Project files</h5>
                    <ul class="list-unstyled project-files">
                        <li><a href="#"><i class="fa fa-file"></i> Project_document.docx</a></li>
                        <li><a href="#"><i class="fa fa-file-picture-o"></i> Logo_zender_company.jpg</a></li>
                        <li><a href="#"><i class="fa fa-stack-exchange"></i> Email_from_Alex.mln</a></li>
                        <li><a href="#"><i class="fa fa-file"></i> Contract_20_11_2014.docx</a></li>
                    </ul>
                    <div class="text-center m-t-md">
                        <a href="#" class="btn btn-xs btn-primary">Add files</a>
                        <a href="#" class="btn btn-xs btn-primary">Report contact</a>

                    </div>
                </div>
            </div>
        </div-->