
<style>
   .loader{
    width: 100px;
    border-radius: 20px;
    border: 1px solid #ffffff;
    background: #E0E5EC;
    }

    /* LOADER 4 */ 
    #loader-4 span{
    display: inline-block;
    width: 20px;
    height: 20px;
    border-radius: 100%;
    background-color: #a3b1c6;
    margin: 8px 5px;
    opacity: 0;
    /*box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);*/
    }

    #loader-4 span:nth-child(1){
    animation: opacitychange 1s ease-in-out infinite;
    }

    #loader-4 span:nth-child(2){
    animation: opacitychange 1s ease-in-out 0.33s infinite;
    }

    #loader-4 span:nth-child(3){
    animation: opacitychange 1s ease-in-out 0.66s infinite;
    }

    @keyframes opacitychange{
    0%, 100%{
        opacity: 0;
    }

    60%{
        opacity: 1;
    }
    }

    .wizard > .steps > ul > li {
        width: 20%;
    }
    .clear{
        clear:both;
    }
    .wizard > .content {
        background: #9f9ee7c4;
        /*background: white;*/
    }
    .seven{
        width:14%;
        float:left;
    }
    .five{
        width:20%;
        float:left;
    }
    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 0px 0px 0px 20px;
    }
    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 36px;
        user-select: none;
        -webkit-user-select: none;
    }

    .container_{
        box-shadow: 1px 0px 15px 1px #f6f6f6;
    }

    fieldset {
        margin-top: 0px;
        border-radius: 20px  
    }

    .slidedown-header {
        padding: 8px;
    }

    .page-header {
        font-weight: bold;
        opacity:  0.8;
        /*color: #a3b1c6;*/
    }

    .slidedown-controller {
        display: flex;
        flex-direction:  row;
        align-items: center;
        height: 70px;
        border-bottom: 1px solid #f6f6f6;
        background-color: #3f3e97; 
        opacity: 0.9;
    }

    .myarrow1,
    .myarrow2,
    .myarrow3,
    .myarrow4,
    .myarrow5,
    {
        float: 3%;
    }

    .myheader {
        width: 97%;
        text-align: center;
        color: #f6f6f6;
    }

    .fa-arrow-down, .fa-arrow-up {
        color: white;
    }

    .hidden-forms-1,
    .hidden-forms-2,
    .hidden-forms-3,
    .hidden-forms-4,
    .hidden-forms-5 {
        /*display: none;*/
        margin-top: 20px;
        position: relative;
    }

    .sub-fin {
        margin-left: 20px;
    }

    .activity-indicator
    {
        margin: 0;
        background: white;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%) 
    }

    .a1-0,
    .a2-0,
    .a3-0,
    .a4-0,
    .a5-0{
        position: absolute; 
        width: 100%; 
        opacity: 0.8; 
        height: 100%; 
        background-color: white; 
        z-index: 10000;
    }

    #next-button-1,
    #next-button-2,
    #next-button-3,
    #next-button-4,
    #next-button-5,
    #loader,
    #previous-button-1,
    #previous-button-2,
    #previous-button-3,
    #previous-button-4 {
        padding: 6px;
        color: white;
        border-radius: 5px;
        background: #3f3e97;
        border: 1px solid #3f3e97;
        margin-bottom: 20px;
    }

    #add-form-repeater{
        background: white ;
        width: 80px;
        border: 1px solid #3f3e97;
    }

    .myright-arrow{
        padding-left: 8px;
    }

    .myleft-arrow {
        padding-right: 8px;
    }

    .emergency-wrapper {
        margin-left: 15px;
    }

    .edu-wrapper {
        margin-left: 15px;
    }

    .edu_wrapper {
        display: table;
        margin: 0 auto;
        width: 100%;
    }

    #add-form-repeater {
        margin-bottom: 20px;
        margin-top: 20px;
    }

    .repeat-separator-top{
        padding-top: 20px
    }

    .repeat-separator-bottom{
        padding-bottom: 20px;
    }

    .card {
        z-index: 0;
        border: none;
        border-radius: 0.5rem;
        position: relative
    }

    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: lightgrey;
    }

    #progressbar .active {
        color: #676A6C;
        opacity: 0.8;
    }

    #progressbar li {
        list-style-type: none;
        font-size: 12px;
        width: 20%;
        float: left;
        position: relative;
    }

    #progressbar #personal1:before {
        font-family: FontAwesome;
        content: "\f007";
    }

    #progressbar #personal2:before {
        font-family: FontAwesome;
        content: "\f007";
    }

    #progressbar #program:before {
        font-family: FontAwesome;
        content: "\f02d";
    }

    #progressbar #pin:before {
        font-family: FontAwesome;
        content: "\f084";
    }

    #progressbar #educational:before {
        font-family: FontAwesome;
        content: "\f19d";
    }

    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 18px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px;
        opacity: 0.8;
        /*neumorphic design*/
        box-shadow: 0px 10px 20px -15px #8281cb, 0px -5px 20px -15px #8281cb;
    }

    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: lightgray;
        position: absolute;
        left: 0;
        top: 25px;
        z-index: -1;
        opacity: 0.8;
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background:  #3f3e97;
        opacity: 0.9;
    }

    .hd {
        margin-bottom: 20px;
        /*font-weight: bold;*/
    }

    .p-dp {
        /*height: 220px;
        width: 180px;*/
        border-radius: 100%;
        border: 1px solid #e0e5ec;
        width: 120px;
        height: 120px;
        object-fit: cover;
        padding: 5px;
        float: left;
        /*box-shadow: 0px 60px 90px 0px #f4f4f4;*/
    }

    #required {
        color: #cc5965;
        font-size: bold;
    }

    .ghanaian_applicant {
        font-size: 13px;
    }

    .dt-rpt {
        color: #f1f1f1;
    }

    @media screen and (max-width: 600px) {
        .dseven {
            width: 100% !important;
        }

        .justify-content-center, .fa-arrow-up, .fa-arrow-down {
            display: none;
        }



    }
    

</style>

<?php
    if(isset($form_data->active_form)){
        $form_data->active_form = $form_data->active_form;
    }else{
        $form_data->active_form = '1';
    }

    if( $form_data->status == '' ){
        //merge-one-array
        $l = (object)array_merge(array('user' => $user_details),array('others' => $ind_prop));
        $l->properties = (object)array();
        $l->program_choice = (object)array();
        
        
        $l->properties->Source_of_information           = '';
        $l->properties->specified_source_of_information = '';
        $l->properties->present_occupation              = '';
        $l->properties->physical_challenge              = '';
        $l->properties->pursue                          = '';
        $l->properties->impression                      = '';
        $l->properties->achievement                     = '';
        $l->properties->english_first_language          = 'Yes';
        
        $l->user->middle_name                           = '';
        $l->user->marital_status                        = '';
        $l->user->no_of_children                        = '';
        $l->user->place_of_birth                        = '';
        $l->user->permanent_address                     = '';
        $l->user->residential_address                   = '';
        $l->user->nationality                           = '';
        $l->user->phone_country_code                    = '';
        $l->user->personal_line_country_code            = '';
        $l->user->personal_line                         = '';
        $l->user->office_line                           = '';
        $l->user->fax                                   = '';

        $l->program_choice->first_programme_choice      = '';
        $l->program_choice->second_programme_choice     = '';
        $l->program_choice->third_programme_choice      = '';
        $l->program_choice->session                     = '';
       
        $l->others->emergency[0]->name                  = '';
        $l->others->emergency[1]->name                  = '';
        $l->others->emergency[0]->relation              = '';
        $l->others->emergency[1]->relation              = '';
        $l->others->emergency[0]->specified_relation    = '';
        $l->others->emergency[1]->specified_relation    = '';

    
    } else {
        $l = (object)json_decode($form_data->json_values);

        if(!isset($l->program_choice->first_programme_choice)){
            $l->program_choice->first_programme_choice              = '';
            $l->program_choice->second_programme_choice             = '';
            $l->program_choice->third_programme_choice              = '';
            $l->program_choice->session                             = '';
        } 
        if(!isset($l->user->personal_line_country_code)){
            $l->user->phone_country_code                            = '233';
            $l->user->personal_line_country_code                    = '233';
        } 


        if(startsWith($l->user->phone , "233")){
            //remove 233
            $l->user->phone = substr($l->user->phone, 3);
        } else if(startsWith($l->user->phone , "+233")){
            //remove 233
            $l->user->phone = substr($l->user->phone, 4);
        } else if(startsWith($l->user->phone , "+")){
            //remove 233
            $l->user->phone = substr($l->user->phone, 1);
        }
        $l->user->phone = (int)$l->user->phone;


    }
?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2><?=$within_admission->year?> Admission</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="#!">Home</a>
                    </li>
                    <li>
                        <a>Online</a>
                    </li>
                    <li class="active">
                        <strong>Application</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2"> </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12 container_">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5> Online Application
                            </h5>
                            <?//=print_r($ind_prop);?>
                        </div>
                        <div class="ibox-content">
                            <h3 class="page-header">
                                Application Forms
                            </h3>
                             <!-- start progress bar -->
                            <div class="row justify-content-center">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                                    <div class="card">
                                        <div class="col-md-12 mx-0">
                                            <!-- progressbar -->
                                            <ul id="progressbar">
                                                <li class="active" id="personal1"><label>Personal details</label></li>
                                                <li id="personal2"><label>Personal details continued</label></li>
                                                <li id="program"><label>Programme</label></li>
                                                <li id="educational"><label>Educational background</label></li>
                                                <li id="pin"><label>Apply pin</label></li>
                                            </ul> <!-- fieldsets -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end progress bar -->

                            <?php echo form_open_multipart('admission/admission/save_forms' , 'name="admission_forms" id="form" class="wizard-big"  ');?>
                                <!--<input type="submit" class="btn btn-primary btn-block" value="save and continue later">-->


                                <div class="slidedown-controller">
                                    <h3 class="slidedown-header myheader">Personal details </h3>
                                    <i class="fa fa-arrow-up myarrow1"></i>
                                </div>
                                
                                <fieldset>
                                    <div class="hidden-forms-1 <?=$form_data->active_form?>">
                                        <h2 class="hd">Personal details</h2>
                                        <div class="row centerform">
                                            <div class="col-lg-8">
                                                <div class="form-group col-md-6">
                                                    <label for="first_name">First name(s) 
                                                        <span id="required">*</span>
                                                    </label>
                                                    <input required type="text" value="<?=_cl($l->user->first_name)?>" name="user[first_name]" placeholder="First name" class="first_name form-control" id="first_name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="last_name">Last Name/Surname(s)
                                                        <span id="required">*</span>
                                                    </label>
                                                    <input required type="text" value="<?=$l->user->last_name?>" name="user[last_name]" placeholder="Last name..." class="last_name form-control" id="last_name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="middle_name">Middle Name(s)</label>
                                                    <input type="text" value="<?=$l->user->middle_name?>" name="user[middle_name]" placeholder="Middle Name..." class="middle_name form-control" id="middle_name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="date_of_birth">Date of birth
                                                        <span id="required">*</span>
                                                    </label>
                                                    <input required type="" value="<?=$l->user->date_of_birth?>" name="user[date_of_birth]" placeholder="Date of Birth" class="date_of_birth form-control" id="date_of_birth">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="sex" >Sex
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select required type="text" value="" name="user[gender]" class="sex form-control" id="sex">
                                                        <option value=''>select gender</option>
                                                        <option <?=$l->user->gender == "male" ? 'selected': ''?> >male</option>
                                                        <option <?=$l->user->gender == "female" ? 'selected': ''?>>female</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="sex" >Marital Status
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select required type="text" name="user[marital_status]" class="marital_status form-control required" id="marital_status">
                                                        <option value=''>select Marital Status</option>
                                                        <option <?=$l->user->marital_status == "single" ? 'selected': ''?> >single</option>
                                                        <option <?=$l->user->marital_status == "married" ? 'selected': ''?> >married</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="place_of_birth">Place of Birth
                                                        <span id="required">*</span>
                                                    </label>
                                                    <input type="text" required value="<?=$l->user->place_of_birth?>" name="user[place_of_birth]" placeholder="Town/Region/Country" class="place_of_birth form-control required" id="place_of_birth">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="nationality">Nationality
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select class="form-control nationality required" name="user[nationality]" id="nationality" required>
                                                        <option value="" > Please Select your country or origin </option>
                                                        <optgroup label="Currency">
                                                            <?php foreach($countries as $key => $values){ ?>
                                                                <option value="<?=$values->name?>" <?=$values->name == $l->user->nationality ? "selected" : ""?>> <?=$values->name?> </option>
                                                            <?php } ?>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="no_of_children">No. of Children</label>
                                                    <input type="number" value="<?=$l->user->no_of_children?>" name="user[no_of_children]" placeholder="Children" class="no_of_children form-control" id="no_of_children">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="e_mail">Email
                                                        <span id="required">*</span>
                                                    </label>
                                                    <input required type="email" value="<?=$l->user->email?>" name="user[email]" placeholder="Email" class="e_mail form-control" id="email">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="phone">Phone 
                                                        <span id="required">*</span>
                                                    </label>
                                                    <span class="clear" id="phone_clear"> </span>
                                                    <select class="form-control select2 col-md-3 required" style="width: 20%;float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 0px 0px 20px;" name="user[phone_country_code]" required>
                                                        <option value="233"> 233 </option>
                                                        <optgroup label="Country">
                                                            <?php foreach($countries as $key => $values){ ?>
                                                                <option value="<?=$values->phonecode?>" <?=$values->phonecode == $l->user->phone_country_code ? "selected" : ""?>> <?=$values->phonecode?> </option>
                                                            <?php } ?>
                                                        </optgroup>
                                                    </select>
                                                    <input required type="number" id="phone" value="<?=$l->user->phone?>" name="user[phone]" placeholder="Personal Line"  class="form-control phone col-md-9" style="width: 80%; float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 20px 0px 0px;"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="personal_line">Phone </label>
                                                    <span class="clear" > </span>
                                                    <select class="form-control select2 col-md-3 required" style="width: 20%;float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 0px 0px 20px;" name="user[personal_line_country_code]" required>
                                                        <option value="233"> 233 </option>
                                                        <optgroup label="Country">
                                                            <?php foreach($countries as $key => $values){ ?>
                                                                <option value="<?=$values->phonecode?>" <?=$values->phonecode == $l->user->personal_line_country_code ? "selected" : ""?>> <?=$values->phonecode?> </option>
                                                            <?php } ?>
                                                        </optgroup>
                                                    </select>
                                                    <input type="number" id="personal_line" value="<?=$l->user->personal_line?>" name="user[personal_line]" placeholder="Alt Personal Line"  class="form-control personal_line col-md-9" style="width: 80%; float:left; padding-right: 0px; padding-left: 0px; border-radius: 0px 20px 0px 0px;"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="office_line">Office</label>
                                                    <input type="text" value="<?=$l->user->office_line?>" name="user[office_line]" placeholder="Office Line" class="office_line form-control" id="office_line">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="fax">Fax</label>
                                                    <input type="text" value="<?=$l->user->fax?>" name="user[fax]" placeholder="Fax" class="fax form-control" id="fax">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="Residential Address">Residential Address
                                                        <span id="required">*</span>
                                                    </label>
                                                    <textarea name="user[residential_address]" class="residential_address form-control required" id="residential_address"
                                                    placeholder="Residential Address to which all communication in connection with this application should be sent" ><?=$l->user->residential_address?></textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="permanent_address">Permanent Address</label>
                                                    <textarea  name="user[permanent_address]" class="permanent_address form-control" id="permanent_address"
                                                    placeholder="Permanent Address (if different from one above)" ><?=$l->user->permanent_address?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="img-container">
                                                    <img src="<?=$user_picture?>" class="p-dp" alt="profile" data-toggle="modal" data-target="#profile_picture">
                                                    <input required type="file" name="profile" class="dropify"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-navigators">
                                            <button type="button" id="next-button-1">Save & continue<i class="fa fa-arrow-right myright-arrow"></i></button>
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <div class="slidedown-controller">
                                        <h3 class="slidedown-header myheader">Personal details continued</h3>
                                        <i class="fa fa-arrow-down myarrow2"></i>
                                    </div>
                                    <div class="row hidden-forms-2 <?=$form_data->active_form?>">
                                        <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-md-6 left">
                                                            <h4>Emergency Contact[1]:</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_name">Name
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="text" required value="<?=$l->others->emergency[0]->name?>" name="others[emergency][0][name]" placeholder="Name" class="emergency_contact_name form-control" id="emergency_contact_name">
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_relation">Relationship
                                                                <span id="required">*</span>
                                                            </label>
                                                            <!--<input type="text" required value="<?=$l->others->emergency[0]->relation?>" name="others[emergency][0][relation]" placeholder="Relation" class="emergency_contact_relation form-control" id="emergency_contact_relation">-->
                                                            <select required type="text" value="" name="others[emergency][0][relation]" class="emergency_contact_relation form-control" id="s_e0">
                                                                <option value=''>Select your relationship</option>
                                                                <option <?=$l->others->emergency[0]->relation == "Father" ? 'selected': ''?> >Father</option>
                                                                <option <?=$l->others->emergency[0]->relation == "Mother" ? 'selected': ''?> >Mother </option>
                                                                <option <?=$l->others->emergency[0]->relation == "Uncle" ? 'selected': ''?> >Uncle</option>
                                                                <option <?=$l->others->emergency[0]->relation == "Aunt" ? 'selected': ''?> >Aunt</option>
                                                                <option <?=$l->others->emergency[0]->relation == "Other" ? 'selected': ''?> >Other </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6" id="d_e0">
                                                            <label for="emergency_contact_relation">Other (Please specify your relationship).
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="text" value="<?=$l->others->emergency[0]->specified_relation?>" name="others[emergency][0][specified_relation]" placeholder="Specify your relation" class="specified_relation form-control" id="i_e0" required>
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contect_phone">Tel
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="number" required value="<?=$l->others->emergency[0]->phone?>" name="others[emergency][0][phone]" placeholder="Telephone" class="emergency_contect_phone form-control" id="emergency_contect_phone">
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_email">E-mail</label>
                                                            <input type="email" value="<?=$l->others->emergency[0]->email?>" name="others[emergency][0][email]" placeholder="E-mail" class="emergency_contact_email form-control" id="emergency_contact_email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-md-6 left">
                                                            <h4>Emergency Contact[2]:</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_name">Name
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="text" required value="<?=$l->others->emergency[1]->name?>" name="others[emergency][1][name]" placeholder="Name" class="emergency_contact_name form-control" id="emergency_contact_name_">
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_relation">Relationship
                                                                <span id="required">*</span>
                                                            </label>
                                                            <!--<input type="text" required value="<?=$l->others->emergency[1]->relation?>" name="others[emergency][1][relation]" placeholder="Relation" class="emergency_contact_relation form-control" id="emergency_contact_relation_">-->
                                                            <select required type="text" value="" name="others[emergency][1][relation]" class="emergency_contact_relation form-control" id="s_e1">
                                                                <option value=''>Select your relationship</option>
                                                                <option <?=$l->others->emergency[1]->relation == "Father" ? 'selected': ''?> >Father</option>
                                                                <option <?=$l->others->emergency[1]->relation == "Mother" ? 'selected': ''?> >Mother </option>
                                                                <option <?=$l->others->emergency[1]->relation == "Uncle" ? 'selected': ''?> >Uncle</option>
                                                                <option <?=$l->others->emergency[1]->relation == "Aunt" ? 'selected': ''?> >Aunt</option>
                                                                <option <?=$l->others->emergency[1]->relation == "Other" ? 'selected': ''?> >Other </option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6" id="d_e1">
                                                            <label for="emergency_contact_relation">Other (Please specify your relationship).
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="text" value="<?=$l->others->emergency[1]->specified_relation?>" name="others[emergency][1][specified_relation]" placeholder="Specify your relation" class="specified_relation form-control" id="i_e1" required>
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contect_phone">Tel
                                                                <span id="required">*</span>
                                                            </label>
                                                            <input type="number" value="<?=$l->others->emergency[1]->phone?>" name="others[emergency][1][phone]" placeholder="Telephone" class="emergency_contect_phone form-control" id="emergency_contact_phone_" required>
                                                        </div>
                                                        <div class="form-group col-md-6 left">
                                                            <label for="emergency_contact_email">E-mail</label>
                                                            <input type="email" value="<?=$l->others->emergency[1]->email?>" name="others[emergency][1][email]" placeholder="E-mail" class="emergency_contact_email form-control" id="emergency_contact_email_">
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group col-md-4">
                                                <label for="Source_of_information"> Where did you hear about us?
                                                    <span id="required">*</span>
                                                </label>
                                                <select required type="text" value="" name="properties[Source_of_information]" class="Source_of_information form-control" id="Source_of_information">
                                                        <option value=''>Select where you heard about us</option>
                                                        <option <?=$l->properties->Source_of_information == "Search engine" ? 'selected': ''?> >Search engine </option>
                                                        <option <?=$l->properties->Source_of_information == "Youtube" ? 'selected': ''?> >Youtube </option>
                                                        <option <?=$l->properties->Source_of_information == "Website" ? 'selected': ''?> >Website </option>
                                                        <option <?=$l->properties->Source_of_information == "Other" ? 'selected': ''?> >Other </option>
                                                </select>
                                
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="present_occupation">Present Occupation </label>
                                                <input type="text" value="<?=$l->properties->present_occupation?>" name="properties[present_occupation]" placeholder="Present Occupation" class="present_occupation form-control" id="last_name">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="english_first_language">Is English your first language. </label>
                                                <select required type="text" value="" name="properties[english_first_language]" class="english_first_language form-control" id="last_name">
                                                    <option <?=$l->properties->english_first_language == "Yes" ? 'selected': ''?> >Yes </option>
                                                    <option <?=$l->properties->english_first_language == "No" ? 'selected': ''?> >No </option>                                                
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-md-12" id="hd-at">
                                                <label for="specified_source_of_information">Other (Please specify where you heard about us).
                                                    <span id="required">*</span>
                                                </label>
                                                <input type="text" value="<?=$l->properties->specified_source_of_information?>" name="properties[specified_source_of_information]" placeholder="Specify where you heard about us" class="specified_source_of_information form-control" id="or_info" required>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="physical_challenge">Do you have any special needs or require support as a consequence of any disability or medical conditions? </label>
                                                <!--<input type="text" value="<?=$l->properties->physical_challenge?>" name="properties[physical_challenge]" placeholder="Do you have any special needs or require support as a consequence of any disability or medical conditions?" class="physical_challenge form-control" id="last_name">-->
                                                <select type="text" value="" name="properties[physical_challenge]" class="physical_challenge form-control">
                                                    <option <?=$l->properties->physical_challenge == "No" ? 'selected': ''?>>No</option>
                                                    <option <?=$l->properties->physical_challenge == "Yes" ? 'selected': ''?>>Yes</option>
                                                </select>
                                            </div>
                                            <div class="form-navigators">
                                                <button type="button" id="previous-button-1"><i class="fa fa-arrow-left myleft-arrow"></i>Previous</button>
                                                <button type="button" id="next-button-2">Save & continue<i class="fa fa-arrow-right myright-arrow"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <!--<h1>Programme Choice  </h1>-->
                                <div class="slidedown-controller">
                                    <h3 class="slidedown-header myheader">Programme</h3>
                                    <i class="fa fa-arrow-down myarrow3"></i>
                                </div>

                                <fieldset>
                                    <div class="hidden-forms-3 <?=$form_data->active_form?>">
                                        <h2 class="hd">Programme</h2>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group col-md-4">
                                                    <label for="first_programme_choice" > <?=humanize("first Programme Choice")?>
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select required type="text" value="" name="program_choice[first_programme_choice]" class="first_programme_choice form-control" id="first_programme_choice">
                                                        <option value=''>select Programme</option>
                                                        <option <?=$l->program_choice->first_programme_choice == "Doctor of Pharmacy" ? 'selected': ''?> >Doctor of Pharmacy </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="second_programme_choice" > <?=humanize("second Programme Choice")?>
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select required type="text" value="" name="program_choice[second_programme_choice]" class="second_programme_choice form-control" id="second_programme_choice">
                                                        <option value=''>select Programme</option>
                                                        <option <?=$l->program_choice->second_programme_choice == "Doctor of Pharmacy" ? 'selected': ''?> >Doctor of Pharmacy </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="third_programme_choice" ><?=humanize("third Programme Choice")?>
                                                        <span id="required">*</span>
                                                    </label>
                                                    <select required type="text" value="" name="program_choice[third_programme_choice]" class="third_programme_choice form-control" id="third_programme_choice">
                                                        <option value=''><?=humanize("select Programme")?></option>
                                                        <option <?=$l->program_choice->third_programme_choice == "Doctor of Pharmacy" ? 'selected': ''?> >Doctor of Pharmacy </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-navigators">
                                            <button type="button" id="previous-button-2"><i class="fa fa-arrow-left myleft-arrow"></i>Previous</button>
                                            <button type="button" id="next-button-3">Save & continue<i class="fa fa-arrow-right myright-arrow"></i></button>
                                        </div>
                                    </div>
                                </fieldset>
                                
                                <!--<h1> Educational Background </h1>-->
                                <div class="slidedown-controller">
                                    <h3 class="slidedown-header myheader">Educational Background</h3>
                                    <i class="fa fa-arrow-down myarrow4"></i>
                                </div>
                                <div class="hidden-forms-4 <?=$form_data->active_form?>" id="edu-wrapper">
                                   <h2 class="hd">Educational Background</h2>
                                   <?php //var_dump($courses);?>
                                    <fieldset class="repeater">
                                        <div data-repeater-list="qualification" class="edu-wrapper">
                                            <?php if(isset($l->others->qualification) && count($l->others->qualification) !== 0){ ?>
                                                <?php foreach($l->others->qualification as $q){ ?>
                                                    <?php $grades = json_decode(json_encode($q->grades) , true); ?>
                                                    <div data-repeater-item class="row repeat-separator-top">
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label> CERTIFICATION </label> 
                                                            <select required name="study_level" class="form-control certification" onchange="crt_lvl(this)">
                                                                <option value="" <?=$q->study_level == "" ? 'selected': ''?>></option>
                                                                <option value="A-level" <?=$q->study_level == "A-level" ? 'selected': ''?>>A-level</option>
                                                                <option value="O-level" <?=$q->study_level == "O-level" ? 'selected': ''?>>O-level</option>
                                                                <option value="WASSCE" <?=$q->study_level == "WASSCE" ? 'selected': ''?>>WASSCE</option>
                                                                <option value="SSSCE" <?=$q->study_level == "SSSCE" ? 'selected': ''?>>SSSCE</option>
                                                                <option value="Diploma" <?=$q->study_level == "Diploma" ? 'selected': ''?>>Diploma</option>
                                                                <option value="HND" <?=$q->study_level == "HND" ? 'selected': ''?>>HND</option>
                                                                <option value="Degree" <?=$q->study_level == "Degree" ? 'selected': ''?>>Degree</option>
                                                                <option value="Matured" <?=$q->study_level == "Matured" ? 'selected': ''?>>Matured</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label>   FIELD OF STUDY</label> 
                                                            <input required type="text" placeholder="Enter Study Field" value="<?=$q->study_field?>" name="study_field" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label>   NAME OF INSTITUTION </label> 
                                                            <input required type="text" placeholder="Enter Institution Name" value="<?=$q->institution_name?>" name="institution_name" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label>  INSTITUTION TYPE </label> 
                                                            <select required name="institution_type" class="form-control">
                                                                <option value=""></option>
                                                                <option value="Government" <?=$q->institution_type == "Government" ? 'selected': ''?>>Government</option>
                                                                <option value="Semi-Government" <?=$q->institution_type == "Semi-Government" ? 'selected': ''?>>Semi-Government</option>
                                                                <option value="Private" <?=$q->institution_type == "Private" ? 'selected': ''?>>Private</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label>  YEAR COMPLETED </label> 
                                                            <input required type="text" placeholder="Enter Year Completed " value="<?=$q->year?>" name="year" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label> CERTIFICATION </label> 
                                                            <input type="file" id="file" name="qualification" />
                                                            <?=isset($q->picture) ? form_hidden('picture', $q->picture) : "";?>
                                                        </div>
                                                        <div class="form-group col-md-6" style="width: 14% !important;">
                                                            <label> click to delete this particular row </label> 
                                                            <input data-repeater-delete type="button" value="Delete" class="btn btn-primary btn-block dt-rpt" disabled/>
                                                        </div>
                                                        
                                                        <div class="row repeat-separator-bottom edu_wrapper">
                                                            <label class="col-md-3" style="width: 100% !important; margin-bottom: 20px; border-bottom: 0px solid #f4f4f4;"> GRADES  </label>
                                                            <div class="row">
                                                                <div  class="col-md-6">
                                                                    <table>
                                                                    <tr>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Subject</label></td>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Grades</label></td>
                                                                    </tr>

                                                                        <?php  
                                                                            foreach(range(1, 5) as $number) {
                                                                                $grades[$number]['grades_lable'] = isset($grades[$number]['grades_lable']) ? $grades[$number]['grades_lable'] : "";
                                                                                $grades[$number]['grades_grades'] = isset($grades[$number]['grades_grades']) ? $grades[$number]['grades_grades'] : "";
                                                                        ?>  
                                                                            <tr>
                                                                                <td>
                                                                                    <!--<input type="text" placeholder="Enter Subject" value="<?=$grades[$number]['grades_lable']?>" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">-->
                                                                                    
                                                                                    <select type="text" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                        <option value="">Subject</option>
                                                                                        <?php 
                                                                                            $encoded_subjects = $courses->{0}->{'courses'};
                                                                                            $decoded_subjects = json_decode($encoded_subjects);
                                                                                            $w_s = $decoded_subjects[0]->wassce;
                                                                                            $al_s = $decoded_subjects[0]->a_level;
                                                                                            if($q->study_level == 'WASSCE' || true) {
                                                                                            foreach($w_s as $s) {?>
                                                                                            <option class="ws" <?=$grades[$number]['grades_lable'] == $s ? 'selected' : ''?>><?=$s?></option>
                                                                                        <?php
                                                                                            }
                                                                                        } if($q->study_level == 'A-Level' || true){
                                                                                            foreach($al_s as $als) {
                                                                                        ?>
                                                                                            <option class="als" <?=$grades[$number]['grades_lable'] == $als ? 'selected' : ''?>><?=$als?></option>
                                                                                        <?php
                                                                                            }
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" placeholder="Enter grades" value="<?=$grades[$number]['grades_grades']?>" name="grades_grades<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                </td>
                                                                            </tr>
                                                                        
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                    </table>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <table>
                                                                    <tr>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Subject</label></td>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Grades</label></td>
                                                                    </tr>

                                                                        <?php 
                                                                            foreach(range(6, 10) as $number) {
                                                                                $grades[$number]['grades_lable'] = isset($grades[$number]['grades_lable']) ? $grades[$number]['grades_lable'] : "";
                                                                                $grades[$number]['grades_grades'] = isset($grades[$number]['grades_grades']) ? $grades[$number]['grades_grades'] : "";
                                                                        ?>  
                                                                            <tr>
                                                                                <td class="td">                                                                                    
                                                                                    <select type="text" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                        <option value="">Subject</option>
                                                                                        <?php 
                                                                                            $encoded_subjects = $courses->{0}->{'courses'};
                                                                                            $decoded_subjects = json_decode($encoded_subjects);
                                                                                            $w_s = $decoded_subjects[0]->wassce;
                                                                                            $al_s = $decoded_subjects[0]->a_level;
                                                                                            if($q->study_level == 'WASSCE' || true) {
                                                                                            foreach($w_s as $s) {
                                                                                        ?>
                                                                                            <option class="ws" <?=$grades[$number]['grades_lable'] == $s ? 'selected' : ''?>><?=$s?></option>
                                                                                        <?php
                                                                                            }
                                                                                        } if($q->study_level == 'A-Level' || true){
                                                                                            foreach($al_s as $als) {
                                                                                        ?>
                                                                                            <option class="als" <?=$grades[$number]['grades_lable'] == $als ? 'selected' : ''?>><?=$als?></option>
                                                                                        <?php
                                                                                            }
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                    
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" placeholder="Enter grades" value="<?=$grades[$number]['grades_grades']?>" name="grades_grades<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                </td>
                                                                            </tr>
                                                                        
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?=isset($q->picture) ? "<a target='_blank' href='".base_url($q->picture)."'>".img(array('src'=>$q->picture, 'height'=>'100px'))."</a>" : "";?>

                                                <?php } ?>   
                                            <?php } else { ?>   
                                            
                                                <div data-repeater-item class="row repeat-separator-top">
                                                    <div class="form-group  seven">
                                                        <label>Study Level</label> 
                                                        <select required name="study_level" class="form-control certification" onchange="crt_lvl(this)">
                                                            <option value=""></option>
                                                            <option value="A-level" >A-level</option>
                                                            <option value="O-level" >O-level</option>
                                                            <option value="WASSCE" >WASSCE</option>
                                                            <option value="WASSCE" >SSSCE</option>
                                                            <option value="Diploma" >Diploma</option>
                                                            <option value="HND">HND</option>
                                                            <option value="Degree" >Degree</option>
                                                            <option value="Matured" >Matured</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label>   FIELD OF STUDY </label> 
                                                        <input required type="text" placeholder="Enter Study Field" name="study_field" class="form-control">
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label>   NAME OF INSTITUTION </label> 
                                                        <input required type="text" placeholder="Enter Institution Name" name="institution_name" class="form-control">
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label>  INSTITUTION TYPE</label> 
                                                        <select required name="institution_type" class="form-control">
                                                            <option value=""></option>
                                                            <option value="Government">Government</option>
                                                            <option value="Semi-Government">Semi-Government</option>
                                                            <option value="Private">Private</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label>  YEAR COMPLETED </label> 
                                                        <input type="text" placeholder="Enter Year Completed " name="year" class="form-control">
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label> CERTIFICATION </label> 
                                                        <input type="file" id="file" name="qualification"/>
                                                        <?=isset($q->picture) ? form_hidden('picture', $q->picture) : "";?>
                                                    </div>
                                                    <div class="form-group  seven">
                                                        <label> click to delete this particular row </label> 
                                                        <input data-repeater-delete type="button" value="Delete" class="btn btn-primary btn-block dt-rpt" disabled/>
                                                    </div>

                                                    <div class="row repeat-separator-bottom edu_wrapper">
                                                            <label class="col-md-3" style="width: 100% !important; margin-bottom: 20px; border-bottom: 0px solid #f4f4f4;"> GRADES  </label>
                                                            <div class="row">
                                                                <div  class="col-md-6">
                                                                    <table>
                                                                    <tr>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Subject</label></td>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Grades</label></td>
                                                                    </tr>
                                                                    <?php  
                                                                        foreach(range(1, 5) as $number) {
                                                                                $grades[$number]['grades_lable'] = isset($grades[$number]['grades_lable']) ? $grades[$number]['grades_lable'] : "";
                                                                                $grades[$number]['grades_grades'] = isset($grades[$number]['grades_grades']) ? $grades[$number]['grades_grades'] : "";
                                                                        ?>  
                                                                            <tr>
                                                                                <td>
                                                                                    <!--<input type="text" placeholder="Enter Subject" value="<?=$grades[$number]['grades_lable']?>" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">-->
                                                                                    
                                                                                    <select type="text" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                        <option value="">Subject</option>
                                                                                        <?php 
                                                                                            $encoded_subjects = $courses->{0}->{'courses'};
                                                                                            $decoded_subjects = json_decode($encoded_subjects);
                                                                                            $w_s = $decoded_subjects[0]->wassce;
                                                                                            $al_s = $decoded_subjects[0]->a_level;
                                                                                            if($q->study_level == 'WASSCE' || true) {
                                                                                            foreach($w_s as $s) {?>
                                                                                            <option class="ws" <?=$grades[$number]['grades_lable'] == $s ? 'selected' : ''?>><?=$s?></option>
                                                                                        <?php
                                                                                            }
                                                                                        } if($q->study_level == 'A-Level' || true){
                                                                                            foreach($al_s as $als) {
                                                                                        ?>
                                                                                            <option class="als" <?=$grades[$number]['grades_lable'] == $als ? 'selected' : ''?>><?=$als?></option>
                                                                                        <?php
                                                                                            }
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" placeholder="Enter grades" value="<?=$grades[$number]['grades_grades']?>" name="grades_grades<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                </td>
                                                                            </tr>
                                                                        
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </table>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <table>
                                                                    <tr>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Subject</label></td>
                                                                        <td class="col-md-3" style="width: 10% !important;"><label>Grades</label></td>
                                                                    </tr>

                                                                    <?php 
                                                                        foreach(range(6, 10) as $number) {
                                                                                $grades[$number]['grades_lable'] = isset($grades[$number]['grades_lable']) ? $grades[$number]['grades_lable'] : "";
                                                                                $grades[$number]['grades_grades'] = isset($grades[$number]['grades_grades']) ? $grades[$number]['grades_grades'] : "";
                                                                        ?>  
                                                                            <tr>
                                                                                <td class="td">                                                                                    
                                                                                    <select type="text" name="grades_lable<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                        <option value="">Subject</option>
                                                                                        <?php 
                                                                                            $encoded_subjects = $courses->{0}->{'courses'};
                                                                                            $decoded_subjects = json_decode($encoded_subjects);
                                                                                            $w_s = $decoded_subjects[0]->wassce;
                                                                                            $al_s = $decoded_subjects[0]->a_level;
                                                                                            if($q->study_level == 'WASSCE' || true) {
                                                                                            foreach($w_s as $s) {?>
                                                                                            <option class="ws" <?=$grades[$number]['grades_lable'] == $s ? 'selected' : ''?>><?=$s?></option>
                                                                                        <?php
                                                                                            }
                                                                                        } if($q->study_level == 'A-Level' || true){
                                                                                            foreach($al_s as $als) {
                                                                                        ?>
                                                                                            <option class="als" <?=$grades[$number]['grades_lable'] == $als ? 'selected' : ''?>><?=$als?></option>
                                                                                        <?php
                                                                                            }
                                                                                            }
                                                                                        ?>
                                                                                    </select>
                                                                                    
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" placeholder="Enter grades" value="<?=$grades[$number]['grades_grades']?>" name="grades_grades<?=$number?>" style="width: 100% !important;" class="form-control col-md-6">
                                                                                </td>
                                                                            </tr>
                                                                        
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            <?php } ?>   
                                        </div>
                                        <input data-repeater-create type="button" id="add-form-repeater" value="Add" class="a-rpt"/>
                                        
                                        <div class="form-navigators">
                                            <button type="button" id="previous-button-3"><i class="fa fa-arrow-left myleft-arrow"></i>Previous</button>
                                            <button type="button" id="next-button-4">Save & continue<i class="fa fa-arrow-right myright-arrow"></i></button>
                                        </div>
                                    </fieldset>
                                </div>
                            </form>
                                                            <!--<h1>Apply pin</h1>-->
                            
                                <div class="slidedown-controller">
                                    <h3 class="slidedown-header myheader">Apply pin</h3>
                                    <i class="fa fa-arrow-down myarrow5"></i>
                                </div>
                                <div class="hidden-forms-5 <?=$form_data->active_form?>">
                                                                        
                                <?php
                                    if((int)$form_data->payment_id !== 0){
                                ?>
                                    <div>
                                        <p>Your pin has been applied successfully. Please submit your forms.</p>
                                        <a class="btn btn-primary btn-block" href="<?=site_url('admission/admission/submission')?>" style="color:#ffffff;" onclick="javascript:return confirm('Are you sure you want to submit this Document? \n\n\n There is no going back.')"> <i class="fa fa-diamond"></i> Submit Forms</a>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                        <!--<a class="btn btn-primary btn-block" href="#!" style="color:#ffffff;" onclick="javascript:return alert('Please Save Application Forms Before Applying your payment details \n.')"> <i class="fa fa-diamond"></i> Apply Pin</a>-->
                                    <?php
                                        //} else {
                                    ?>
                                        <!--<a class="btn btn-primary btn-block"  data-toggle="modal" data-target="#apply_pin" style="color:#ffffff;"> <i class="fa fa-diamond" >Enter Pin</i> </a></li>-->
                                        <div class="" id="#apply_pin">
                                            <div class="modal-dialog">
                                                <div class="modal-content animated flipInY" style="box-shadow: 0px 9px 50px  #f1f1f1, 0px -1px 10px rgba(255,255,255, 0.5);">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title"> Apply Payment Details </h4>
                                                        <small class="font-bold"> Please apply details to be able to successfully completely submit Application</small>
                                                    </div>
                                                    <?=form_open_multipart('admission/user/link_payment', 'name="pin-form" id="pin-form"');?>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-6 b-r">
                                                                    <div class="form-group">
                                                                        <label> Serial number </label> 
                                                                        <input type="text" placeholder="Apply Serial number" name="serial_code" class="form-control" required >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>  Pin </label> 
                                                                        <input type="number" placeholder="Apply pin " name="pin" class="form-control" required >
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p class="text-center">
                                                                        <a href="#"><i class="fa fa-credit-card big-icon"></i></a>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-navigators sub-fin" id="sub-fin">
                                                            <button type="button" id="previous-button-4"><i class="fa fa-arrow-left myleft-arrow"></i>Previous</button>
                                                            <button type="button" id="next-button-5">Submit</button>
                                                        </div>
                                                    <?=form_close();?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        }
                                    ?>
                                </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>