<body class="canvas-menu">
<?php $this->load->view('uniform'); ?>

<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <a class="close-canvas-menu"><i class="fa fa-times"></i></a>
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img alt="image" class="img-circle" src="<?=$user_picture?>"  style="width:100%;"/>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"> <?=$user_details->first_name?> </strong>
                         </span> <span class="text-muted text-xs block">Candidate <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?=site_url('admission')?>">Profile</a></li>
                        <li><a href="#!">lock Screen</a></li>
                        <li class="divider"></li>
                        <li><a href="<?=site_url('admission/auth/logout')?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element"> Entrance</div>
            </li>
            <li>
                <a href="<?=site_url('admission')?>"><i class="fa fa-diamond"></i> <span class="nav-label">Home</span></a>
            </li>

            <?php
                /*
                    status == in_progress || failed_to_submit       == 20
                    status == submitted                             == 40    
                    status == qualified || unqualified            == 90
                    status == accepted                              == 100%
                    bg-danger 

                */

                if(isset($form_data)) :
                    switch ($form_data->status) {
                        case 'in_progress':
                            if((int)$form_data->payment_id !== 0){
                                ?>
                                    <li> <a href="<?=site_url('admission/admission/submission')?>" onclick="javascript:return confirm('Are you sure you want to submit this Document? \n\n\n There is no going back.')"> <i class="fa fa-diamond"></i> Submit Forms</a></li>
                                <?php
                            }
                            break;
                        case 'qualified':
                            ?> <li> <a href="<?=site_url('admission/admission/acceptance')?>" onclick="javascript:return confirm('Are you sure you want to accept this Document? ')"> <i class="fa fa-diamond"></i> Accept Admission</a> </li> <?php
                            break;
                        case 'accepted':
                            ?> Congratulation <?php
                            break;
                    }
                endif;
            ?>
        </ul>

    </div>
</nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="#!">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome to Entrance University College of Health Science</span>
                    </li>
                    <!--li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="img/a7.jpg">
                                    </a>
                                    <div class="media-body">
                                        <small class="pull-right">46h ago</small>
                                        <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="img/a4.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy">5h ago</small>
                                        <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="img/profile.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="pull-right">23h ago</small>
                                        <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                        <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="mailbox.html">
                                        <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="mailbox.html">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="profile.html">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="grid_options.html">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="notifications.html">
                                        <strong>See All Alerts</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li-->
                    
                    <!-- User-->
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa"> <img src="<?=$user_picture?>" alt="user" class="img-circle" style="width:28px"> </i>  
                            <span> <?=$user_details->first_name?>  </span>
                        </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#!">Profile</a></li>
                            <li><a href="#!">Lock screen</a></li>
                            <li class="divider"></li>
                            <li><a href="<?=site_url('admission/auth/logout')?>">Logout</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="<?=site_url('admission/auth/logout')?>">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>

                </ul>

            </nav>
        </div>









