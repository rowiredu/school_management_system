<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Entrance University College of Health Science</title>
    <meta content="Entrance Dashboard" name="description" />
    <meta content="Emmanuel Kwabena Dadzie" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/portal/assets/images/favicon.ico">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/jquery-ui.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/user_applicant_portal/plugin/dropify/css/dropify.min.css" rel="stylesheet">
    

    <!-- Toastr style -->
    <link href="<?=base_url()?>assets/user_applicant_portal/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    
    <!--script type='text/javascript'>
        var logout = "<?=base_url("admission/auth/logout")?>";

        window.onbeforeunload = function (e) {
            myWindow = window.open( logout , "myWindow");   // Opens a new 
            myWindow.close();   // Closes the new window

            var message = 'Still want to leave this application.';
            e = e || window.event;
            // For IE and Firefox
            if (e) {
                e.returnValue = message;
            }
        

            // For Safari
            return message;
        };
    </script-->
</head>




