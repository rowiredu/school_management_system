
        <div class="footer">
            <div class="pull-right">
                <strong>Copyright © </strong>  2020 Entrance University College of Health Science . 
            </div>
            <div>
            <small>All rights reserved. Powered by <a target="_blank" href="https://www.zentechgh.com/"> Zentech GH <?=date('Y');?></a></small>
            </div>
        </div>

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/jquery-2.1.1.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/bootstrap.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/jquery-ui.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/inspinia.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    
    <!-- Event handler to handle the subject form inputs-->
    <script type="text/javascript">
        function crt_lvl(sel)
        {
            var value   =  sel.value
            var select  =  sel.name
            
            if(value == "Degree" || value == "Diploma" || value == "HND" || value == "Matured")
            {
                $('select[name="'+select+'"]').parent().siblings('.repeat-separator-bottom').slideUp("fast");
            }
            else
            {
                if(value == "WASSCE" || value == "O-level" || value == "SSSCE") {
                    $('.ws').show();
                    $('.als').hide();
                }else if(value == "A-level"){
                    $('.als').show();
                    $('.ws').hide();
                }
                $('select[name="'+select+'"]').parent().siblings('.repeat-separator-bottom').slideDown();
            }
        }
    </script>

    <script>
        var btn
        
        $(document).ready(function() {
            var speOther_i  = $('#or_info')
            var s_eall = [1 , 2]
            var slength = s_eall.length
            var i = $('.dt-rpt').length
            var addBtns = $('.a-rpt').on('click', function(){
                i++
                for(var x=0; x < i; x++){
                    $($('.dt-rpt')[x]).prop('disabled', false)
                    $($('.repeat-separator-bottom')[x]).css('border-bottom', '5px solid #5d5cbc')
                }
            })

            if(speOther_i.val() == '') {
                $('#hd-at').slideUp()
            }
            else {
                $('#hd-at').slideDown()
            }

            $('#Source_of_information').change(function()
            {
                var v = $(this).val()

                if(v === "Other") 
                {
                    $('#hd-at').slideDown()
                }else {
                    $('#hd-at').slideUp()
                    $('#or_info').val('')
                }
            });
            
            for(d=0; d < slength; d++) {
                if($('#i_e' + d).val() == '') {
                    $('#d_e' + d).slideUp()
                }
                else {
                    $('#d_e' + d).slideDown()
                }
            }
    
            for(de = 0; de < slength; de++ ) {
                $('#s_e' + de).change(function() 
                {
                    if($(this).val() === "Other") 
                    {
                        $(this).parent().next().slideDown()
                    }else {
                        $(this).parent().next().slideUp()
                        $('#i_e' + de).val('')
                    }
                })
            }

            $('.certification').each(function () {
                var select = $(this).val();

                if(select == "Diploma" || select == "Degree" || select == "HND" || select == "Matured") {
                    $(this).parent().siblings('.repeat-separator-bottom').hide();
                }else if(select ==  "WASSCE") {
                    $('.ws').show();
                    $('.als').hide();
                }else if(select == "A-level") {
                    $('.als').show();
                    $('.ws').hide();
                }else if(select  == "") {
                    $('.als').hide();
                    $('.ws').hide();
                }
            });


            $('#form').ajaxForm({
                beforeSubmit: function(formData)
                {
                    var frm     = btn.substr(12)
                    frm_id      = parseInt(frm) + 1

                    formData.push({
                        name: 'frm_id', value: frm_id
                    })
                },
                complete: function(data) 
                { 
                    var sel_btn     = btn.substr(12)
                    var id          = parseInt(sel_btn)
                    var response    = data.responseText.substring(5, 12)
                
                    toastr.options = {
                        "closeButton": true,
                        "debug": true,
                        "progressBar": true,
                        "preventDuplicates": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "400",
                        "hideDuration": "1000000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                    if(btn != 'finish'){
                        if(btn == 'next-button-5'){
                    
                            if ($('form[name="pin-form"]').validate({
                                errorPlacement: function(error, element) {
                                    error.insertBefore(element)
                                }
                            }))
                            {
                                if($('form[name="pin-form"]').valid())
                                {
                                    $('form[name="pin-form"]').submit()
                                }
                                else 
                                {
                                    return false
                                }
                            }
                        }else {
                            $('.hidden-forms-' + id ).slideUp("slow", function()
                            {
                                $('.hidden-forms-' + (id+1)).slideDown()
                                $('i.myarrow' + (id)).removeClass("fa-arrow-up").addClass("fa-arrow-down")
                                $('i.myarrow' + (id+1)).removeClass("fa-arrow-down").addClass("fa-arrow-up")
                                $("#progressbar li").eq(id).addClass("active")
                                $('#next-button-' + (id)).attr('disabled', false).css('cursor', 'default')
                                $('.form-loader-' + id).hide()
                                $('.a' + id +'-0').hide()
                            })
                            if(btn == 'next-button-4') {
                                //window.location.href = '<?php echo base_url('index.php/admission/admission')?>';
                                $(document).scrollTop($(document).height()); 
                            }
                        }
                    }else 
                    {
                        /*window.location.href = '<?php echo base_url('index.php/admission/admission')?>';*/
                    }

                    if(response === "success")
                    {
                        toastr["success"]("Record successfully saved")
                    }else
                    {
                        toastr["error"]("Error occurred. Press previous to resubmit.");
                    }
                }
            }); 

            $('.repeater').repeater({
            defaultValues: {
                'textarea-input': 'foo',
                'text-input': 'bar',
                'select-input': 'B',
                'checkbox-input': ['A', 'B'],
                'radio-input': 'B'
            },
            show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
            ready: function (setIndexes) {

            }
            });

            var _hiddenfrm = [1, 2, 3, 4, 5];
            for(p=1; p <= _hiddenfrm.length; p++) 
            {
                $('.hidden-forms-' + p).hide();
                $('.hidden-forms-' + p + '.' + p).show();
                $('.hidden-forms-' + p).prepend('<div class="form-loader-' + p + ' a' + p + '-0"></div><div class="a' + p + '-0"><div class="activity-indicator"><div class="col-md-3"><div class="loader" id="loader-4"><span></span><span></span><span></span></div></div></div></div>');
                $('.form-loader-' + p).hide();
                $('.a' + p +'-0').hide();
            }

            var active_form = <?php echo $form_data->active_form; ?>;
            if(active_form > 1) 
            {   
                $('i.myarrow1').removeClass("fa-arrow-up").addClass("fa-arrow-down");
                $('i.myarrow'+active_form).removeClass("fa-arrow-down").addClass("fa-arrow-up");
                $('#hrs').removeClass('hrs');
            }

            for(_af = 0; _af < active_form; ++_af)
            {
                $("#progressbar li").eq(_af).addClass("active");
            }

            window.outerRepeater = $('.outer-repeater').repeater({
            isFirstItemUndeletable: true,
            defaultValues: { 'text-input': 'outer-default' },
            show: function () {
                console.log('outer show');
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                console.log('outer delete');
                $(this).slideUp(deleteElement);
            },
            repeaters: [{
                isFirstItemUndeletable: true,
                selector: '.inner-repeater',
                defaultValues: { 'inner-text-input': 'inner-default' },
                show: function () {
                    console.log('inner show');
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    console.log('inner delete');
                    $(this).slideUp(deleteElement);
                }
            }]
            });

            $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 48], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });
        });


        !function () {
            //var nextBtn, prevBtn = nextBtn = [1, 2, 3, 4];
            var nextBtn = [1, 2, 3, 4, 5]
            var prevBtn = [1, 2, 3, 4]

            function formSubmit () 
            {
                if ($('form[name="admission_forms"]').validate({
                    errorPlacement: function(error, element) {
                        if(element.attr('name') == 'user[phone]'){
                            error.insertBefore(element.siblings('span'))
                        }else{
                            error.insertBefore(element)
                        }
                    }
                }))
                {
                    if($('form[name="admission_forms"]').valid())
                    {
                        $('form[name="admission_forms"]').submit()
                        var id = btn.substr(12)
                        var element = $('#next-button-' + id)
                        if(id != '5') {
                            element.attr('disabled', true).css('cursor', 'not-allowed');
                            $('.form-loader-' + id).show()
                            $('.a' + id +'-0').show()
                        }
                        //return true;
                    }
                    else 
                    {
                        return false
                    }
                }
            }

            for(b=1; b <= nextBtn.length; b++){
                b.toString()
                $("#next-button-" + b).click(function(){
                    btn = this.id
                    formSubmit()
                })
            }
            
            /*$("#finish").click(function(){
                btn = this.id;
                formSubmit();
            });*/

            for( q=1; q <= prevBtn.length; q++)
            {
                q.toString()

                $("#previous-button-" + q).click({p : q}, function(e) {
                    var p = e.data.p
                    $('.hidden-forms-' + (+p+1)).slideUp("slow", function()
                    {
                        $('.hidden-forms-' + p).slideDown()
                        $('i.myarrow' + p).removeClass("fa-arrow-down").addClass("fa-arrow-up")
                        $('i.myarrow' + (p+1)).removeClass("fa-arrow-up").addClass("fa-arrow-down")
                        $("#progressbar li").eq(p).removeClass("active")
                    }
                    )
                })
            }
        }();

    </script>

    <!-- END THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/user_applicant_portal/js/plugins/toastr/toastr.min.js" type="text/javascript"></script>
    
    <?php 
        if (isset($notify)) {
            if (is_array($notify)) {
               foreach ($notify as $key => $value) {
                   echo notification($value);
               }
            }
        }

    ?>

    <!-- Steps -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/staps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/validate/jquery.validate.min.js"></script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>



    <script src="<?=base_url()?>assets/user_applicant_portal/plugin/dropify/js/dropify.min.js"></script>
    <script>$('#date_of_birth').datepicker({ dateFormat: 'dd/mm/yy' }).val();</script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>

    <script src="<?=base_url()?>assets/universal_plugin/form_repeater/jquery.repeater.js"></script>
    <script src="<?=base_url()?>assets/universal_plugin/form_submission/jquery.form.js"></script>
</body>
</html>