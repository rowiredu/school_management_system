<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Entrance University College of Health Science </title>

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <style>
        .loginColumns {
            max-width: 500px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
        }

        .logininfo {
            color: navy;
            font-size: 24px;
        }
    </style>
</head>

<body class="gray-bg">
<?php $this->load->view('uniform'); ?>

    <div class="loginColumns animated fadeInDown">
        <!--img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/-->
        
        <div class="row">

            <!--div class="col-md-6">
                <h2 class="font-bold">Welcome to Entrance University College of Health Science </h2>

                <p>
                    Dear Candidate, kindly sign up to register with EUC admissions portal. Please make sure that you enter valid/active email address and mobile number for authentication purpose.
                </p>

                <p>
                    and also manage your your profile and apply online to the best Pharmacy graduates for best Pharmaceutical care of patients and gain a reputable certificate with Kwame Nkrumah University of Science and Technology
                </p>

                <p>
                    Dont have an account? Can click here to register anytime
                </p>

                <p>
                    <small>Forgotten your password? click here to reset...</small>
                </p>

            </div-->
            <div class="col-md-12">
                <div class="ibox-content">
                  <h1><?php echo lang('change_password_heading');?></h1>
                    <div id="infoMessage" class="logininfo"><?php echo $message;?> </div>

                    <?php echo form_open("admission/auth/change_password");?>
                        <div class="form-group">
                            <label for="identity"> <?php echo lang('change_password_old_password_label', 'old_password');?> </label>
                            <?php echo form_input($old_password , '' , 'class="form-control"');?>
                        </div>

                        <div class="form-group">
                              <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                              <?php echo form_input($new_password , '' , 'class="form-control" ');?>
                        </div>

                        <div class="form-group">
                              <label for="new_password_confirm">
                                    <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
                              </label> 
                              <?php echo form_input($new_password_confirm , '' , 'class="form-control" ');?>
                        </div>
                        <?php echo form_input($user_id);?>

                        <div class="form-group row m-t-20">
                            <?php echo form_submit('submit', lang('change_password_submit_btn') , array('class' => 'btn btn-primary block full-width m-b '));?>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <a href="<?=site_url('admission/auth/forgot_password')?>" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                <a class="btn btn-sm btn-white btn-block" href="<?=site_url('admission/auth/register')?>">Create an account</a>
                            </div>
                        </div>
                    <?php echo form_close();?>
                    <p class="m-t">
                        <small>Powered by Zentech</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-10">
                &copy; Copyright Entrance University College of Health Science
            </div>
            <div class="col-md-2 text-right">
               <small>© <?=date('Y')?></small>
            </div>
        </div>
    </div>

</body>


</html>
