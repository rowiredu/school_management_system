                                    
<?php
    if((int)$form_data->payment_id !== 0){
?>
    <div>
        <p>Your pin has been applied successfully. Please submit your forms.</p>
        <a class="btn btn-primary btn-block" href="<?=site_url('admission/admission/submission')?>" style="color:#ffffff;" onclick="javascript:return confirm('Are you sure you want to submit this Document? \n\n\n There is no going back.')"> <i class="fa fa-diamond"></i> Submit Forms</a>
    </div>
    <?php
        } else {
    ?>
        <!--<a class="btn btn-primary btn-block" href="#!" style="color:#ffffff;" onclick="javascript:return alert('Please Save Application Forms Before Applying your payment details \n.')"> <i class="fa fa-diamond"></i> Apply Pin</a>-->
    <?php
        //} else {
    ?>
        <!--<a class="btn btn-primary btn-block"  data-toggle="modal" data-target="#apply_pin" style="color:#ffffff;"> <i class="fa fa-diamond" >Enter Pin</i> </a></li>-->
        <div class="" id="#apply_pin">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY" style="box-shadow: 0px 9px 50px  #f1f1f1, 0px -1px 10px rgba(255,255,255, 0.5);">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title"> Apply Payment Details </h4>
                        <small class="font-bold"> Please apply details to be able to successfully completely submit Application</small>
                    </div>
                    <?=form_open_multipart('admission/user/link_payment/'.$form_data->id, 'name="pin-form" id="pin-form"');?>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6 b-r">
                                    <div class="form-group">
                                        <label> Serial number </label> 
                                        <input type="text" placeholder="Apply Serial number" name="serial_code" class="form-control" required >
                                    </div>
                                    <div class="form-group">
                                        <label>  Pin </label> 
                                        <input type="number" placeholder="Apply pin " name="pin" class="form-control" required >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <p class="text-center">
                                        <a href="#"><i class="fa fa-credit-card big-icon"></i></a>
                                    </p>
                                </div>

                            </div>
                        </div>
                        
                        <div class="form-navigators sub-fin" id="sub-fin">
                            <button type="button" id="previous-button-4"><i class="fa fa-arrow-left myleft-arrow"></i>Previous</button>
                            <button type="button" id="next-button-5">Submit</button>
                        </div>
                    <?=form_close();?>
                </div>
            </div>
        </div>
    <?php
        }
    ?>