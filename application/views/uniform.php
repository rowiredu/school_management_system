<div class="navbar-wrapper" style="height: 80px;">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color: #ffffff;">
            <div class="container">
                <div class="navbar-header page-scroll" style="width:35%; ">
                    <img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll special" href="<?=base_url()?>">Home</a></li>
                        <li><a class="page-scroll special" href="http://euchs.edu.gh">Website</a></li>
                        <li><a class="page-scroll special" href="http://euchs.edu.gh/contact.php">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>