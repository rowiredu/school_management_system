<!DOCTYPE html>
<html lang="en">

<style>
    #container {
        width: 100%;
        height: 100%;
        position: relative;
        /* The image used */
        background-image: url(<?=base_url('assets/bg.jpg')?>);

        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    #navi,
    #infoi {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }

    #navi_, 
    #infoi_ {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }

    #container_ {
        height: 500px;
    }

    #navi, #navi_1 {
        z-index: 10;
    }

    #infoi {
        background-color: white;
        opacity: 0.1;
    }

    #infoi_{
        background-color: #4443a2;
        opacity: 0.519;
        border-radius: 20px;   
    }       

    .portal-header {
        text-align: center;
        color: white;
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%)
    }

    .shift-center {
        flex-direction: column;
        align-items: center;
        display: flex;
    }

    #admission_hr {
        color: rgba(255, 255, 255, 0.9);
    }

    .contact_form {
        margin-left: 20px;
        margin-right: 20px;
        justify-content: center;
        align-items: center;
        border-radius: 20px;
        display: flex;
    }

    .sub {
        border-radius: 20px;
        align-self: center;
    }

</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Entrance University College of Health Science - Landing Page</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="<?=base_url()?>assets/user_applicant_portal/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">

</head>
<body id="container" class="landing-page">
    <div id="infoi"></div>
    <div id="navi">
        <div class="centered col-md-4" id="container_">
            <div id="infoi_"></div>
            <div id="navi_">
                <div class="shift-center">
                    <img src="<?=base_url('assets/logo_part.png')?>">
                    <p id="admission_hr"><br>WELCOME TO ENTRANCE ADMISSION PORTAL</p>
                </div>
                <form id="contact_form" class="contact_form" name="contact_form" action="<?=site_url('admission/auth/register')?>">
                    <input class="submit sub" value="Create Login" type="submit">
                </form>
                <form id="contact_form" class="contact_form" name="contact_form" action="<?=site_url('admission/auth')?>">
                    <input class="submit sub" value="Login" type="submit">
                </form>
            </div>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/jquery-2.1.1.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?=base_url()?>assets/user_applicant_portal/js/Entrance University College of Health Science.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/pace/pace.min.js"></script>
    <script src="<?=base_url()?>assets/user_applicant_portal/js/plugins/wow/wow.min.js"></script>
</body>

</html>
