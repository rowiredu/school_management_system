<hr />
<div class="row">
	<div class="col-md-12">
		<?php echo form_open(site_url('student_portal/admin/publish_results'));?>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label"><?php echo get_phrase('level');?></label>
					<select name="class_id" class="form-control selectboxit" id = 'class_id'>
                        <option value=""><?php echo get_phrase('select_a_level');?></option>
                        <?php 
                        $classes = $this->db->get('class')->result_array();
                        foreach($classes as $row):
                        ?>
                            <option value="<?php echo $row['class_id'];?>"
                            	<?php if ($class_id == $row['class_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('exam');?></label>
					<select name="exam_id" class="form-control selectboxit" id = 'exam_id'>
                        <option value=""><?php echo get_phrase('select_an_exam');?></option>
                        <?php 
                        $exams = $this->db->get_where('exam' , array('semester' => $semester, 'year' => $running_year))->result_array();
                        foreach($exams as $row):
                        ?>
                            <option value="<?php echo $row['exam_id'];?>"
                            	<?php if ($exam_id == $row['exam_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
            <div class="col-md-3">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('assessment');?></label>
					<select name="assessment_id" class="form-control selectboxit" id = 'assessment_id'>
                        <option value=""><?php echo get_phrase('select_an_assessment');?></option>
                        <?php 
                        $assessment = $this->db->get_where('assessment' , array('semester' => $semester, 'year' => $running_year))->result_array();
                        foreach($assessment as $row):
                        ?>
                            <option value="<?php echo $row['assessment_id'];?>"
                            	<?php if ($assessment_id == $row['assessment_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('Department');?></label>
					<select name="department_id" class="form-control selectboxit" id = 'department_id'>
                        <option value=""><?php echo get_phrase('select_a_department');?></option>
                        <?php 
                        $departments = $this->db->get('department')->result_array();
                        foreach($departments as $row):
                        ?>
                            <option value="<?php echo $row['department_id'];?>"
                            	<?php if ($department_id == $row['department_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<input type="hidden" name="operation" value="selection">
			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" id = 'submit' class="btn btn-info"><?php echo get_phrase('review_results');?></button>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<?php if ($class_id != '' && $exam_id != '' && $department_id != '' && $assessment_id != ''):?>

<br>
<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="text-align: center;">
		<div class="tile-stats tile-gray">
		<div class="icon"><i class="entypo-docs"></i></div>
			<h3 style="color: #696969;">
				<?php
                    $exam_name  = $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name; 
                    $assessment_name  = $this->db->get_where('assessment' , array('assessment_id' => $assessment_id))->row()->name; 
					$class_name = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name; 
					echo get_phrase('results');
				?>
			</h3>
            <h4 style="color: #696969;">
                <?php echo '(' . $this->crud_model->get_department_name($department_id) . ') ' . $class_name;?>
            </h4>
			<h4 style="color: #696969;">
				<?php echo $exam_name .' & '. $assessment_name;?>
			</h4>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>


<hr />


<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
				<td style="text-align: center;">
					<?php echo get_phrase('students');?> <i class="entypo-down-thin"></i> | <?php echo get_phrase('courses');?> <i class="entypo-right-thin"></i>
				</td>
				<?php 
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'department_id' => $department_id, 'semester' => $semester))->result_array();
					foreach($subjects as $row):
				?>
					<td style="text-align: center;"><?php echo $row['name'];?></td>
				<?php endforeach;?>
				<td style="text-align: center;"><?php echo get_phrase('total');?></td>
				<td style="text-align: center;"><?php echo get_phrase('average_grade_point');?></td>
				</tr>
			</thead>
			<tbody>
			<?php
				
				$students = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $department_id, 'year' => $running_year))->result_array();
				$fail_mark = $this->db->get_where('settings', array('type' => 'fail_mark'))->row()->description;
				foreach($students as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->first_name . ' ' . $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->last_name;?>
					</td>
				<?php
					$total_marks        = 0;
					$total_grade_point  = 0;
					$total_credit_hours = 0;
					$failed_students    = '';
					$failed_course      = '';

					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php 
							$obtained_mark_query = 	$this->db->get_where('mark' , array(
													'class_id' => $class_id , 
														'exam_id' => $exam_id , 
															'subject_id' => $row2['subject_id'] , 
																'student_id' => $row['student_id'],
																	'year' => $running_year
												));
							/*
                            $obtained_assessment_mark_query = $this->db->get_where('assessment_marks', array(
                                                    'class_id' => $class_id , 
                                                        'assessment_id' => $assessment_id , 
                                                            'subject_id' => $row2['subject_id'] , 
                                                                'student_id' => $row['student_id'],
                                                                    'year' => $running_year
                            ));*/
						
							if ( $obtained_mark_query->num_rows() > 0) {
                                $obtained_exams_marks = $obtained_mark_query->row()->exam_mark;
                                $obtained_assessment_marks = $obtained_mark_query->row()->assessment_mark;
                                $obtained_marks = (($obtained_exams_marks + $obtained_assessment_marks) / 130) * 100;

								echo number_format($obtained_marks, 2);
                                
								if ($obtained_marks >= 0 && $obtained_marks != '') {
									$subject_ins = $this->crud_model->get_subject_info($row2['subject_id']);
									foreach($subject_ins as $subject_in):
										
										$grade = $this->crud_model->get_grade($obtained_marks);
										$total_grade_point += $subject_in['credit_hour'] * $grade['grade_point'];
										$total_credit_hours += $subject_in['credit_hour'];
									endforeach;

								}
								
								if($obtained_marks <= $fail_mark ) {
									$failed_students = $row['student_id'];
									$failed_course   = $row2['subject_id'];
								}
                                
								$total_marks += $obtained_marks;
							}else {
								if(in_array($row2['subject_id'], !empty(json_decode($row['s' . $semester . '_subjects'])) ? json_decode($row['s' . $semester . '_subjects']) : array()))
								echo "<sub>----</sub>";
								else echo "<small style='color: #f1f1f1'>Not registered</small>";
							}
							

						?>
					</td>
				<?php endforeach;?>
				<td style="text-align: center;"><?php echo number_format($total_marks,2);?></td>
				<td style="text-align: center;">
					<?php 
						if($total_credit_hours !== 0 && $total_grade_point !== 0)
							echo number_format($total_grade_point/$total_credit_hours, 1);
						else 
							echo get_phrase('0');
					?>
				</td>
				</tr>
				
			<?php endforeach;?>
			
			</tbody>
		</table>
		<center>
			<?php 

				if(!empty($failed_students) && !empty($failed_course)) {
					$failed_student_data = array(
						'student_id' => $failed_students,
						'failed_course' => $failed_course,
						'class_id' => $class_id,
						'exam_id' => $exam_id,
						'department_id' => $department_id,
						'assessment_id' => $assessment_id,
						'semester' => $semester,
						'year' => $running_year
					);
				}

                $published_results_query = $this->db->get_where('published_results', array('class_id' => $class_id, 'exam_id' => $exam_id, 'department_id' => $department_id, 'assessment_id' => $assessment_id, 'semester' => $semester, 'year' => $running_year))->row();
				
				if(empty($published_results_query)):
            ?>
			<a href="<?php echo site_url('student_portal/admin/publish_results/'.$class_id.'/'.$exam_id.'/'.$department_id.'/'.$assessment_id .'/publish');?>" 
				class="btn btn-success">
				<?php echo get_phrase('publish_results');?>
			</a>
            <?php 
                else: 
            ?>
            <a href="<?php echo site_url('student_portal/admin/publish_results/'.$class_id.'/'.$exam_id.'/'.$department_id.'/'.$assessment_id .'/unpublish');?>" 
				class="btn btn-danger">
				<?php echo get_phrase('unpublish_results');?>
			</a>
            <?php endif;?>
		</center>
	</div>
</div>
<?php endif;?>
<script type="text/javascript">
	var class_id = '';
	var exam_id  = '';
	var department_id = '';
	jQuery(document).ready(function($) {
		//$('#submit').attr('disabled', 'disabled');
	});
	function check_validation(){
		if(class_id !== '' && exam_id !== '' && department_id !== ''){
			//$('#submit').removeAttr('disabled');
		}
		else{
			//$('#submit').attr('disabled', 'disabled');	
		}
	}
	$('#class_id').change(function() {
		class_id = $('#class_id').val();
		check_validation();
	});
	$('#exam_id').change(function() {
		exam_id = $('#exam_id').val();
		check_validation();
	});
	$('#department_id').change(function() {
		department_id = $('#department_id').val();
		check_validation();
	});
</script>