<hr />
<?php //var_dump($department_id);?>
<div class="row">
	<div class="col-md-12">
		<?php echo form_open(site_url('student_portal/admin/tabulation_sheet_assessment'));?>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label"><?php echo get_phrase('Level');?></label>
					<select name="class_id" class="form-control selectboxit" id = 'class_id'>
                        <option value=""><?php echo get_phrase('select_a_level');?></option>
                        <?php 
                        $classes = $this->db->get('class')->result_array();
                        foreach($classes as $row):
                        ?>
                            <option value="<?php echo $row['class_id'];?>"
                            	<?php if ($class_id == $row['class_id']) echo 'selected';?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('assessment');?></label>
					<select name="assessment_id" class="form-control selectboxit" id = 'assessment_id'>
                        <option value=""><?php echo get_phrase('select_an_assessment');?></option>
                        <?php 
            			$semester = $this->db->get_where('sub_admin_settings' , array('type' => 'semester'))->row()->description;
                        $assessments = $this->db->get_where('assessment' , array('semester' => $semester, 'year' => $running_year))->result_array();
                        foreach($assessments as $row):
                        ?>
                            <option value="<?php echo $row['assessment_id'];?>"
                            	<?php if(isset($assessment_id)){if ($assessment_id == $row['assessment_id']) { echo 'selected';}} ?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label"><?php echo get_phrase('department');?></label>
					<select name="department_id" class="form-control selectboxit" id = 'department_id'>
                        <option value=""><?php echo get_phrase('select_a_department');?></option>
                        <?php 
            			
                        $departments = $this->db->get_where('department')->result_array();
                        foreach($departments as $row):
                        ?>
                            <option value="<?php echo $row['department_id'];?>"
                            	<?php if(isset($department_id)){if ($department_id == $row['department_id']) { echo 'selected';}} ?>>
                            		<?php echo $row['name'];?>
                            </option>
                        <?php
                        endforeach;
                        ?>
                    </select>
				</div>
			</div>
			<input type="hidden" name="operation" value="selection">
			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" id = 'submit' class="btn btn-info"><?php echo get_phrase('view_tabulation_sheet');?></button>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<?php if ($class_id != '' && $assessment_id != '' && $department_id != ''):?>
<br>
<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="text-align: center;">
		<div class="tile-stats tile-gray">
		<div class="icon"><i class="entypo-docs"></i></div>
			<h3 style="color: #696969;">
				<?php
					$assessment_name  = $this->db->get_where('assessment' , array('assessment_id' => $assessment_id))->row()->name; 
					$class_name = $this->db->get_where('class' , array('class_id' => $class_id))->row()->name; 
					echo get_phrase('tabulation_sheet');
				?>
			</h3>
			<h4 style="color: #696969;">
				<?php echo $this->crud_model->get_department_name($department_id);?>
			</h4>
			<h4 style="color: #696969;">
				<?php echo $class_name;?> : <?php echo $assessment_name;?>
			</h4>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>


<hr />

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
				<td style="text-align: center;">
					<?php echo get_phrase('students');?> <i class="entypo-down-thin"></i> | <?php echo get_phrase('subjects');?> <i class="entypo-right-thin"></i>
				</td>
				<?php 
					$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'department_id' => $department_id, 'semester' => $semester))->result_array();
					foreach($subjects as $row):
				?>
					<td style="text-align: center;"><?php echo $row['name'];?></td>
				<?php endforeach;?>
				<td style="text-align: center;"><?php echo get_phrase('total');?></td>
				<!--<td style="text-align: center;"><?php echo get_phrase('average_grade_point');?></td>-->
				</tr>
			</thead>
			<tbody>
			<?php
				
				$students = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $department_id, 'year' => $running_year))->result_array();
				foreach($students as $row):
			?>
				<tr>
					<td style="text-align: center;">
						<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->first_name . ' ' . $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->last_name;?>
					</td>
				<?php
					$total_marks = 0;
					$total_grade_point = 0;  
					foreach($subjects as $row2):
				?>
					<td style="text-align: center;">
						<?php 
							$obtained_mark_query = 	$this->db->get_where('mark' , array(
													'class_id' => $class_id , 
														'assessment_id' => $assessment_id , 
															'subject_id' => $row2['subject_id'] , 
																'student_id' => $row['student_id'],
																	'year' => $running_year,
																		'semester' => $semester
												));
							if ( $obtained_mark_query->num_rows() > 0) {
								$obtained_marks = $obtained_mark_query->row()->assessment_mark;
								echo $obtained_marks;
								if ($obtained_marks >= 0 && $obtained_marks != '') {
									$grade = $this->crud_model->get_grade($obtained_marks);
									$total_grade_point += $grade['grade_point'];
								}
								$total_marks += $obtained_marks;
							}else 
							{
								if(in_array($row2['subject_id'], !empty(json_decode($row['s' . $semester . '_subjects'])) ? json_decode($row['s' . $semester . '_subjects']) : array()))
								echo "<sub>----</sub>";
								else 
								echo "<small style='color: #f1f1f1'>Not registered</small>";
							}
							

						?>
					</td>
				<?php endforeach;?>
				<td style="text-align: center;"><?php echo $total_marks;?></td>
				<!--<td style="text-align: center;">
					<?php 
						$course_registered        = json_decode($row['s' . $semester . '_subjects']);
						$number_course_registered = sizeof($course_registered);
						echo $number_course_registered == 0 ? $number_course_registered : ($total_grade_point / $number_course_registered);
					?>
				</td>
						-->
				</tr>

			<?php endforeach;?>

			</tbody>
		</table>
		<center>
			<a href="<?php echo site_url('student_portal/admin/tabulation_sheet_assessment_print_view/'.$class_id.'/'.$assessment_id.'/'.$department_id);?>" 
				class="btn btn-primary" target="_blank">
				<?php echo get_phrase('print_tabulation_sheet');?>
			</a>
		</center>
	</div>
</div>
<?php endif;?>
<script type="text/javascript">
	var class_id = '';
	var assessment_id  = '';
	var department_id  = '';
	jQuery(document).ready(function($) {
		//$('#submit').attr('disabled', 'disabled');
	});
	function check_validation(){
		if(class_id !== '' && assessment_id !== '' && department_id !== ''){
			$('#submit').removeAttr('disabled');
		}
		else{
			//$('#submit').attr('disabled', 'disabled');	
		}
	}
	$('#class_id').change(function() {
		class_id = $('#class_id').val();
		check_validation();
	});
	$('#assessment_id').change(function() {
		assessment_id = $('#assessment_id').val();
		check_validation();
	});
	$('#department_id').change(function() {
		department_id = $('#department_id').val();
		check_validation();
	});
</script>