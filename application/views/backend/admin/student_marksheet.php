<style>
    .exam_chart {
    width           : 100%;
        height      : 265px;
        font-size   : 11px;
}
  
</style>

<?php 
    $student_info  = $this->crud_model->get_student_info($student_id);
    $semester      = $this->db->get_where('sub_admin_settings' , array('type' => 'semester'))->row()->description;
    $running_year  = $this->db->get_where('sub_admin_settings' , array('type' => 'running_year'))->row()->description;
    $exams         = $this->crud_model->get_exams_staff($running_year);
    
    foreach ($exams as $row2):
?>

<?php //var_dump($row2['semester']);?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><?php echo $row2['name'];?></div>
            </div>
            <div class="panel-body">
                
                
               <div class="col-md-12">
                   <table class="table table-bordered">
                       <thead>
                        <tr>
                            <td style="text-align: center;"><?php echo get_phrase('course');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('credit_hours');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('obtained_marks');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('grade');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('comment');?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $total_marks        = 0;
                            $total_grade_point  = 0;
                            $total_credit_hours = 0;

                            $student = $this->db->get_where('enroll' , array(
                                'student_id' => $student_id , 'year' => $running_year
                            ))->row();

                            $student->subjects = json_decode($student->{'s'.$row2['semester'].'_subjects'} , true);
                            $subjects = $student->subjects == null ?  array() : $this->db->query("SELECT * FROM `subject` WHERE `semester` = ".$row2['semester']." AND `department_id` = ".$department_id." AND subject_id IN (".implode(',', array_map('intval', $student->subjects)).")")->result_array();

                            foreach ($subjects as $row3):
                        ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $row3['name'];?></td>
                                <td style="text-align: center;">
                                    <?php

                                    $subject_ins = $this->crud_model->get_subject_info($row3['subject_id']);
                                    foreach($subject_ins as $subject_in):
                                        echo $subject_in['credit_hour'];
                                    endforeach;
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        $obtained_mark_query = $this->db->get_where('mark' , array(
                                                    'subject_id' => $row3['subject_id'],
                                                        'exam_id' => $row2['exam_id'],
                                                            'class_id' => $class_id,
                                                                'student_id' => $student_id , 
                                                                    'year' => $running_year));
                                        if ( $obtained_mark_query->num_rows() > 0) {
                                            $obtained_exam_mark = $obtained_mark_query->row()->exam_mark;
                                            $obtained_assessment_mark = $obtained_mark_query->row()->assessment_mark;
                                            $obtained_marks = (($obtained_exam_mark + $obtained_assessment_mark) / 130) * 100;

                                            echo number_format($obtained_marks, 2);
                                        }else {
                                            echo "<small>--</small>";
                                        }
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        if($obtained_mark_query->num_rows() > 0) {
                                            if ($obtained_marks >= 0 || $obtained_marks != '') {
                                                $grade = $this->crud_model->get_grade($obtained_marks);
                                                echo $grade['name'];
                                                $total_grade_point += $subject_in['credit_hour'] * $grade['grade_point'];
                                                $total_marks += $obtained_marks;
                                                $total_credit_hours += $subject_in['credit_hour'];
                                            }
                                        }
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php if($obtained_mark_query->num_rows() > 0) 
                                            echo $grade['comment'];
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                   </table>

                   <hr />

                   <?php echo get_phrase('total_marks');?> : <?php echo number_format($total_marks, 2);?>
                   <br>
                   <?php echo get_phrase('average_grade_point');?> : 
                        <?php 
                            if($total_credit_hours !== 0 || $total_grade_point !== 0)
                                echo number_format($total_grade_point/$total_credit_hours, 1);
                            else 
                                echo get_phrase('0');
                        ?>

                    <br> <br>
                    <a href="<?php echo site_url('student_portal/admin/student_marksheet_print_view/'.$student_id.'/'.$row2['exam_id'].'/'.$row2['semester']);?>"
                        class="btn btn-primary" target="_blank">
                        <?php echo get_phrase('print_marksheet');?>
                    </a>
               </div>       
            </div>
        </div>  
    </div>
</div>
<?php
    endforeach;
       // endforeach;
?>