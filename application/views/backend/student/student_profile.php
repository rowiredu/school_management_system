<style>
    .profile_details {
        border-radius: 1px;
        border: 1px solid #f1f1f1;
    }

    ._icons {
        font-size: 16px;
        color: #f1f1f1;
        margin-right: 18px;
    }
</style>
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">

			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-user"></i> 
					<?php echo get_phrase('manage_profile');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
    </div>
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane box active" id="list" style="padding: 5px">
                <div class="box-content">
                <?php 
                foreach($student_data as $row):
                ?>
                    <div class="profile-details col-md-4" style="width: 200px !important;">
                        <section id="profile-img">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                <img src="<?php echo $this->crud_model->get_image_url('student' , $row['student_id']);?>" alt="...">
                            </div>
                            <p><?php //var_dump($student_data);?></p>
                        </section>
                    </div>
                    <div class="profile_details col-md-8">
                        <section id="profile-details">
                            <h3 class="hd-rs"><strong>Welcome back, <?=$row['first_name']. ' '. $row['last_name']?>!</strong></h3>
                            <p><i class="fa fa-envelope _icons"></i>Email address :- <?=$row['email']?>  </p>
                            <p><i class="fa fa-calendar _icons"></i>Date of birth :- <?=$row['birthday']?>  </p>
                            <p><i class="fa fa-phone _icons"></i>Mobile number :- <?=$row['phone']?>  </p>
                            <p><i class="fa fa-info-circle _icons"></i>Gender :- <?=$row['sex']?>  </p>
                        </section>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>

    </div>
</div>