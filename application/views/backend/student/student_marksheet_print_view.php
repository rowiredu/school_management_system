<?php
    $class_name         =   $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
    $exam_name          =   $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;
    $system_name        =   $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
    $running_year       =   $year;
?>


<div id="print">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <style type="text/css">
        td {
            padding: 5px;
        }
    </style>

    <center>
        <img src="<?php echo base_url(); ?>uploads/logo.png" style="max-height : 60px;"><br>
        <h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
        <?php echo get_phrase('student_marksheet');?><br>
        <?php echo $this->db->get_where('student' , array('student_id' => $student_id))->row()->first_name . ' ' . $this->db->get_where('student' , array('student_id' => $student_id))->row()->last_name;?><br>
        <?php echo $class_name;?><br>
        <?php echo $exam_name;?>
    </center>

    <table style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;" border="1">
       <thead>
        <tr>
            <td style="text-align: center;">Course</td>
            <td style="text-align: center;">Credit hours</td>
            <td style="text-align: center;">Obtained marks</td>
            <td style="text-align: center;">Grade</td>
            <td style="text-align: center;">Comment</td>
        </tr>
    </thead>
    <tbody>
        <?php 
            $total_marks            = 0;
            $total_grade_point      = 0;
            $total_credit_hours     = 0;
            $student                = $this->db->get_where('enroll' , array(
                                        'student_id' => $student_id , 'year' => $running_year
                                        ))->row();
            $student->subjects      = json_decode($student->{'s'.$semester.'_subjects'} , true);
            $subjects               = $student->subjects == null ?  array() : $this->db->query("SELECT * FROM `subject` WHERE `semester` = ".$semester." AND `department_id` = ".$department_id." AND subject_id IN (".implode(',', array_map('intval', $student->subjects)).")")->result_array();

            foreach ($subjects as $row3):
        ?>
            <tr>
                <td style="text-align: center;"><?php echo $row3['name'];?></td>
                <td style="text-align: center;">
                    <?php
                    $subject_ins = $this->crud_model->get_subject_info($row3['subject_id']);
                        foreach($subject_ins as $subject_in):
                            echo $subject_in['credit_hour'];
                        endforeach;
                    ?>
                </td>
                <td style="text-align: center;">
                    <?php
                        $obtained_mark_query = $this->db->get_where('mark' , array(
                                                    'subject_id' => $row3['subject_id'],
                                                        'exam_id' => $exam_id,
                                                            'class_id' => $class_id,
                                                                'student_id' => $student_id , 
                                                                    'year' => $running_year));

                        if($obtained_mark_query->num_rows() > 0){

                            $obtained_exam_mark = $obtained_mark_query->row()->exam_mark;
                            $obtained_assessment_mark = $obtained_mark_query->row()->assessment_mark;
                            $obtained_marks = (($obtained_exam_mark + $obtained_assessment_mark) / 130) * 100;

                            echo number_format($obtained_marks, 2);
                        }
                    ?>
                </td>
                <td style="text-align: center;">
                    <?php
                        if($obtained_mark_query->num_rows() > 0){
                            if ($obtained_marks >= 0 || $obtained_marks != '') {
                                $grade = $this->crud_model->get_grade($obtained_marks);
                                echo $grade['name'];
                                $total_grade_point += $subject_in['credit_hour'] * $grade['grade_point'];
                                $total_marks += $obtained_marks;
                                $total_credit_hours += $subject_in['credit_hour'];
                            }
                        }
                    ?>
                </td>
                <td style="text-align: center;">
                    <?php if($obtained_mark_query->num_rows() > 0) echo $grade['comment'];?>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
   </table>

<br>

    <center>
       <?php echo get_phrase('total_marks');?> : <?php echo number_format($total_marks, 2);?>
       <br>
       <?php echo get_phrase('average_grade_point');?> : 
            <?php 
                if($total_credit_hours !== 0 || $total_grade_point !== 0)
                    echo number_format($total_grade_point/$total_credit_hours, 1);
                else 
                    echo get_phrase('0');
            ?>
    </center>

</div>


<script type="text/javascript">

    jQuery(document).ready(function($)
    {
        var elem = $('#print');
        PrintElem(elem);
        Popup(data);

    });

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>