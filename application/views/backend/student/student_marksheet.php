<style>
    .exam_chart {
    width           : 100%;
        height      : 265px;
        font-size   : 11px;
    }

    .gpa_container {
        padding: 15px;
        background: #fafafa;
        border-radius: 1px;
        box-shadow: 0px 2px 15px #fafafa;
        max-width: 100px;
        max-height: 100px;
        /*margin-left: auto;
        margin-right: auto;*/
        box-sizing: border-box;
    }
  
</style>

<div class="row">
<div class="col-md-12">
<?php 
    $this->db            = $this->load->database('database2', true);
    $tab                 = '';
    $student_info        = $this->crud_model->get_student_info($student_id);
    $semester            = $this->db->get_where('settings' , array('type' => 'semester'))->row()->description;

    $this->db->order_by('enroll_id', 'DESC');
    $student_enrollments = $this->db->get_where('enroll' , array(
                            'student_id' => $student_id))->result_array();
    $current_level       = sizeof($student_enrollments);
    
    echo '<ul class="nav nav-tabs bordered">';
    foreach($student_enrollments as $tab):
        
        if($tab['class_id'] == $current_level) {
            echo '<li class="active">';
        }else {
            echo '<li class="">';
        }
        
            echo '<a href="#tab'.$tab['class_id'].'" data-toggle="tab" class="btn btn-default" style="width: 200px !important">
                    <span class="visible-xs"><i class="entypo-users"></i></span>
                    <span class="hidden-xs">'.$this->crud_model->get_class_name($tab['class_id']).'</span>
                </a>
            </li>
        ';
    endforeach;
    
    echo '</ul>';


    echo '<div class="tab-content">';
   
    foreach ($student_enrollments as $student_enrollment):
?>

<div class="tab-pane <?php if($student_enrollment['class_id'] == $current_level) echo 'active';?>" id="tab<?php echo $student_enrollment['class_id']?>">
        <?php                     
            $exams = $this->crud_model->get_exams_student($student_enrollment['year']);
            if(!empty($exams)):
            foreach($exams as $exam):
                if($exam['year'] == $student_enrollment['year']):
                
            $published_results_query = $this->db->get_where('published_results', array('class_id' => $student_enrollment['class_id'], 'exam_id' => $exam['exam_id'], 'department_id' => $department_id, 'year' => $exam['year']))->row();
        ?>
        <div class="panel panel-primary panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><?php echo $exam['name'] . ' | '. $exam['year'];?></div>
            </div>
            <?php if(!empty($published_results_query)): ?>
            <div class="panel-body">
                
                
               <div class="col-md-12">
                   <table class="table table-bordered">
                       <thead>
                        <tr>
                            <td style="text-align: center;"><?php echo get_phrase('course');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('credit_hours');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('obtained_marks');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('grade');?></td>
                            <td style="text-align: center;"><?php echo get_phrase('comment');?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $total_marks                = 0;
                            $total_grade_point          = 0;
                            $total_credit_hours         = 0;
                            $student                    = $this->db->get_where('enroll' , array(
                                                            'student_id' => $student_id , 'year' => $exam['year']
                                                                ))->row();
                            $student->subjects          = json_decode($student->{'s'.$exam['semester'].'_subjects'} , true);
                            $subjects                   = $student->subjects == null ?  array() : $this->db->query("SELECT * FROM `subject` WHERE `semester` = ".$exam['semester']." AND `department_id` = ".$department_id." AND subject_id IN (".implode(',', array_map('intval', $student->subjects)).")")->result_array();

                            foreach ($subjects as $row3):
                        ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $row3['name'];?></td>
                                <td style="text-align: center;">
                                    <?php

                                    $subject_ins = $this->crud_model->get_subject_info($row3['subject_id']);
                                    foreach($subject_ins as $subject_in):
                                        echo $subject_in['credit_hour'];
                                    endforeach;
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        $obtained_mark_query = $this->db->get_where('mark' , array(
                                                    'subject_id' => $row3['subject_id'],
                                                        'exam_id' => $exam['exam_id'],
                                                                'student_id' => $student_id , 
                                                                    'year' => $exam['year']));
                                        if ( $obtained_mark_query->num_rows() > 0) {

                                            $obtained_exam_mark = $obtained_mark_query->row()->exam_mark;
                                            $obtained_assessment_mark = $obtained_mark_query->row()->assessment_mark;
                                            $obtained_marks = (($obtained_exam_mark + $obtained_assessment_mark) / 130) * 100;

                                            echo number_format($obtained_marks, 2);
                                        }else {
                                            echo "<small>--</small>";
                                        }
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        if($obtained_mark_query->num_rows() > 0) {
                                            if ($obtained_marks >= 0 || $obtained_marks != '') {
                                                $grade = $this->crud_model->get_grade($obtained_marks);
                                                echo $grade['name'];
                                                $total_grade_point += $subject_in['credit_hour'] * $grade['grade_point'];
                                                $total_marks += $obtained_marks;
                                                $total_credit_hours += $subject_in['credit_hour'];
                                            }
                                        }
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php if($obtained_mark_query->num_rows() > 0) 
                                            echo  $grade['comment'];
                                    ?>
                                </td>
                            </tr>
                                    <?php endforeach;?>
                            <tr>
                                <td style="text-align: center;" colspan="2"><strong><?php echo $total_credit_hours.get_phrase('_total_credit_hours_attempted');?></strong></td>
                                <td style="text-align: center;" colspan="3"><strong><?php echo $total_grade_point.get_phrase('_total_grade_points');?></strong></td>
                            <tr>
                    </tbody>
                   </table>

                   <hr />

                   <?php echo get_phrase('total_marks');?> : <?php echo number_format($total_marks, 2);?>
                   <br>
                   <?php echo get_phrase('average_grade_point');?> : 
                        <?php 
                            if($total_credit_hours !== 0 || $total_grade_point !== 0)
                                echo number_format($total_grade_point/$total_credit_hours, 1);
                            else 
                                echo get_phrase('0');
                        ?>

                    <br> <br>
                    <a href="<?php echo site_url('student_portal/student/student_marksheet_print_view/'.$student_id.'/'.$exam['exam_id'].'/'.$exam['semester'].'/'.$exam['year']);?>"
                        class="btn btn-primary" target="_blank">
                        <?php echo get_phrase('print_marksheet');?>
                    </a>
               </div>
               
            </div>
            <?php else:?>
            <?php echo '<center><div class="text-danger" style="margin: 20px">RESULTS NOT YET PUBLISHED.</div></center>';?>
            <?php endif;?>
        </div>  
    
<?php
    endif;
    endforeach;
    endif;
?>
</div>
<?php
    endforeach; 
?>
<?php echo '</div>';?>
</div>
</div>
<div class="gpa_container">
    <strong>
    <?php echo get_phrase('GPA: ');?>
    <?php 
        $total_gpa = 0;
        $gpas = $this->crud_model->get_gpa($student_id);


        if(!empty($gpas)) {
            foreach($gpas as $gpa):
                $total_gpa += (int)$gpa['average_grade_point'];
            endforeach;
            echo number_format($total_gpa/sizeof($gpas) , 1);
        }else 
        {
            echo '0';
        }
    ?>
    </strong>
</div>