<?php if($this->db->get_where('payment', array('student_id' => $student_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description))->num_rows() > 0):?>
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('course_list');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">            
            <!----TABLE LISTING STARTS-->
			<?php if(empty($registered_subject)) {?>
            <div class="tab-pane box active" id="list">
				<?php echo form_open(site_url('student_portal/student/subject/register') , array('class' => 'form-horizontal form-groups-bordered'));?>
					<table class="table table-bordered datatable" id="table_export">
						<thead>
							<tr>
								<th><div><?php echo get_phrase('level');?></div></th>
								<th><div><?php echo get_phrase('course_name');?></div></th>
								<th><div><?php echo get_phrase('credit_hour');?></div></th>
								<th><div><?php echo get_phrase('lecturer');?></div></th>
								<th><div><?php echo get_phrase('register');?></div></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($failed_subjects as $failed_subject):
								$subject_info = $this->db->get_where('subject', array('subject_id' => $failed_subject['subject_id']))->row();
							?>
							<tr>
								<td><?php echo $this->crud_model->get_type_name_by_id('class',$subject_info->class_id);?></td>
								<td><?php echo $subject_info->name;?></td>
								<td><?php echo $subject_info->credit_hour?></td>
								<td><?php echo $this->crud_model->get_type_name_by_id('teacher',$subject_info->teacher_id);?></td>
								<td><input type="checkbox" value="<?php echo $subject_info->subject_id?>" name=subject[] required/></td>
							</tr>
							<?php endforeach;?>							

							<?php $count = 1;foreach($subjects as $row):?>
							<tr>
								<td><?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?></td>
								<td><?php echo $row['name'];?></td>
								<td><?php echo $row['credit_hour'];?></td>
								<td><?php echo $this->crud_model->get_type_name_by_id('teacher',$row['teacher_id']);?></td>
								<td><input type="checkbox" value="<?php echo $row['subject_id']?>" name=subject[]/></td>
							</tr>

							<?php endforeach;?>
						</tbody>
					</table>

					<?php if($register_button) { ?>
						<button type="submit" class="btn btn-primary" name="register_course" id="register_course">Register course</button>
					<?php }?>
				</form>
			</div>

            <!----TABLE LISTING ENDS-->
            <?php }else {?>
				<div class="tab-pane box active" id="list">
                <table class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('level');?></div></th>
                    		<th><div><?php echo get_phrase('course_name');?></div></th>
							<th><div><?php echo get_phrase('credit_hour');?></div></th>
                    		<th><div><?php echo get_phrase('lecturer');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($registered_subject as $row):?>
                        <tr>
							<td><?php echo $this->crud_model->get_type_name_by_id('class',$row['class_id']);?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $row['credit_hour'];?></td>
							<td><?php echo $this->crud_model->get_type_name_by_id('teacher',$row['teacher_id']);?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
			<?php }?>
            
		</div>
	</div>
</div>
<?php else:?>
<div class="row">
	<center>
		<div class="col-md-12"><h3 class="text-danger"><?php echo get_phrase('please_make_full_fees_payment_and_come_back.');?><h3></div>
	</center>
</div>
<?php endif;?>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>