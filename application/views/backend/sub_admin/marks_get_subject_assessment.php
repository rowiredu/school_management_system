<div class="col-md-3">
	<div class="form-group">
	<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
		<select name="subject_id" id="subject_id" class="form-control selectboxit">
			<?php
				$assessment = $this->db->get_where('assessment', array('assessment_id' => $assessment_id))->row();
				$subjects = $this->db->get_where('subject' , array(
					'department_id' => $department_id,
						'class_id'  => $class_id,
							'semester'  => $assessment->semester,
				))->result_array();
				foreach($subjects as $row):
			?>
			<option value="<?php echo $row['subject_id'];?>"><?php echo $row['name'];?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>

<input type='hidden' value="<?php echo $assessment->semester?>" name="assessment_semester"/>

<div class="col-md-2" style="margin-top: 20px;">
	<center>
		<button type="submit" class="btn btn-info"><?php echo get_phrase('manage_marks');?></button>
	</center>
</div>


<script type="text/javascript">
	$(document).ready(function() {
        if($.isFunction($.fn.selectBoxIt))
		{
			$("select.selectboxit").each(function(i, el)
			{
				var $this = $(el),
					opts = {
						showFirstOption: attrDefault($this, 'first-option', true),
						'native': attrDefault($this, 'native', false),
						defaultText: attrDefault($this, 'text', ''),
					};
					
				$this.addClass('visible');
				$this.selectBoxIt(opts);
			});
		}
    });
	
</script>