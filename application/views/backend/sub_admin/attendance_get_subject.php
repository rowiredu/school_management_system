<div class="col-md-3">
	<div class="form-group">
	<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
		<select name="subject_id" id="subject_id" class="form-control selectboxit">
			<?php
			    $semester = $this->db->get_where('sub_admin_settings' , array('type' => 'semester'))->row()->description;
				$subjects = $this->db->get_where('subject' , array(
					'department_id' => $department_id ,
                        /*'teacher_id' => $this->session->userdata('teacher_id'),*/
                            'class_id' => $class_id,
							    'semester' => $semester
				))->result_array();
				foreach($subjects as $row):
			?>
			<option value="<?php echo $row['subject_id'];?>"><?php echo $row['name'];?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>


<div class="col-md-3" style="margin-top: 20px;">
	<button type="submit" class="btn btn-info"><?php echo get_phrase('manage_attendance');?></button>
</div>



<script type="text/javascript">
	$(document).ready(function() {
        if($.isFunction($.fn.selectBoxIt))
		{
			$("select.selectboxit").each(function(i, el)
			{
				var $this = $(el),
					opts = {
						showFirstOption: attrDefault($this, 'first-option', true),
						'native': attrDefault($this, 'native', false),
						defaultText: attrDefault($this, 'text', ''),
					};

				$this.addClass('visible');
				$this.selectBoxIt(opts);
			});
		}
    });

</script>