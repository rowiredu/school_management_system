<hr />

<?php echo form_open(site_url('student_portal/sub_admin/attendance_selector/'));?>
<div class="row">

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
			<input type="text" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
				value="<?php echo date("d-m-Y");?>"/>
		</div>
	</div>

	<?php
		$class_query = $this->db->get('class');
		if($class_query->num_rows() > 0):
			$classes = $class_query->result_array();
	?>

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('level');?></label>
			<select class="form-control selectboxit" name="class_id" id="class_id">
				<?php foreach($classes as $row):?>
					<option value="<?php echo $row['class_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>
	<?php endif;?>

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('department');?></label>
			<select name="section_id" id="" class="form-control selectboxit" onchange="get_class_subject(this.value)">
				<option value=""><?php echo get_phrase('select_department'); ?></option>
				<?php
				$departments = $this->db->get('department')->result_array();
				foreach($departments as $row):
				?>
					<option value="<?php echo $row['department_id'];?>" 
                    <?php if($row['department_id'] == $section_id) echo 'selected'?>>
							<?php echo $row['name'];?>
					</option>
				<?php
				endforeach;
				?>
			</select>
		</div>
	</div>

		<!--<input type="hidden" name="class_id" value="<?php echo $class_id;?>">-->
		<!--<input type="hidden" name="year" value="<?php echo $running_year;?>">-->
	
    <div id="subject_holder">
		<div class="col-md-3">
			<div class="form-group">
			<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
				<select name="subject_id" id="subject_id" class="form-control selectboxit">
					<?php
						$semester = $this->db->get_where('sub_admin_settings' , array('type' => 'semester'))->row()->description;
						$subjects = $this->db->get_where('subject' , array(
							'department_id' => $section_id ,
								/*'teacher_id' => $this->session->userdata('teacher_id'),*/
									'class_id' => $class_id,
										'semester' => $semester
						))->result_array();
						foreach($subjects as $row):
					?>
					<option value="<?php echo $row['subject_id'];?>"
                    <?php if($row['subject_id'] == $subject_id) echo 'selected';?>><?php echo $row['name'];?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>

		<input type="hidden" name="year" value="<?php echo $running_year;?>">

		<div class="col-md-3" style="margin-top: 20px;">
			<button type="submit" class="btn btn-info"><?php echo get_phrase('manage_attendance');?></button>
		</div>

	</div>




</div>
<?php echo form_close();?>


<hr />
<div class="row" style="text-align: center;">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div class="tile-stats tile-gray">
            <div class="icon"><i class="entypo-chart-area"></i></div>

            <h3 style="color: #696969;"><?php echo get_phrase('attendance_for'); ?> <?php echo $this->db->get_where('class', array('class_id' => $class_id))->row()->name; ?></h3>
            <h4 style="color: #696969;">
                <?php echo $this->db->get_where('department', array('department_id' => $section_id))->row()->name; ?>
            </h4>
            <h4 style="color: #696969;">
                <?php echo get_phrase('course: ');?><?php $subject_query = $this->db->get_where('subject', array('subject_id' => $subject_id)); if(!empty($subject = $subject_query->row())) echo $subject->name; ?>
            </h4>

            <?php if($subject_query->num_rows() > 0): ?>
                <h4 style="color: #696969;">
                    <?php echo date("d M Y", $timestamp); ?>
                </h4>
            <?php endif;?>
            <?php ?>
        </div>
    </div>
    <div class="col-sm-4"></div>
</div>

<center>
    <a class="btn btn-default" onclick="mark_all_present()">
        <i class="entypo-check"></i> <?php echo get_phrase('mark_all_present'); ?>
    </a>
    <a class="btn btn-default"  onclick="mark_all_absent()">
        <i class="entypo-cancel"></i> <?php echo get_phrase('mark_all_absent'); ?>
    </a>
</center>
<br>

<div class="row">

    <div class="col-md-2"></div>
    <div class="col-md-8">

        <?php echo form_open(site_url('student_portal/sub_admin/attendance_update/'. $class_id . '/' . $section_id . '/' . $subject_id. '/' . $timestamp)); ?>
        <div id="attendance_update">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo get_phrase('id'); ?></th>
                        <th><?php echo get_phrase('name'); ?></th>
                        <th><?php echo get_phrase('status'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 1;
                    $select_id = 0;
                    $attendance_of_students = $this->db->get_where('attendance', array(
                                'class_id' => $class_id,
                                'section_id' => $section_id,
                                'subject_id' => $subject_id,
                                'year' => $running_year,
                                'timestamp' => $timestamp
                            ))->result_array();


                    foreach ($attendance_of_students as $row):
                        ?>
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td>
                                <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->student_code; ?>
                            </td>
                            <td>
                                <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->first_name . ' ' . $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->last_name; ?>
                            </td>
                            <td>
                                <input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="0" <?php if ($row['status'] == 0) echo 'checked'; ?>>&nbsp;<?php echo get_phrase('undefined'); ?> &nbsp;
                                <input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="1" <?php if ($row['status'] == 1) echo 'checked'; ?>>&nbsp;<?php echo get_phrase('present'); ?> &nbsp;
                                <input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="2" <?php if ($row['status'] == 2) echo 'checked'; ?>>&nbsp;<?php echo get_phrase('absent'); ?> &nbsp;
                            </td>
                        </tr>
                    <?php
                    $select_id++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>

        <center>
            <button type="submit" class="btn btn-success" id="submit_button">
                <i class="entypo-thumbs-up"></i> <?php echo get_phrase('save_changes'); ?>
            </button>
        </center>
        <?php echo form_close(); ?>

    </div>



</div>


<script type="text/javascript">

var class_selection = "";
jQuery(document).ready(function($) {
    $('#submit').attr('disabled', 'disabled');
});

    function select_section(class_id) {
        if (class_id !== '') {
        $.ajax({
            url: '<?php echo site_url('student_portal/sub_admin/get_section/'); ?>' + class_id,
            success:function (response)
            {
                jQuery('#section_holder').html(response);
            }
        });
    }
}
    function mark_all_present() {
        var count = <?php echo count($attendance_of_students); ?>;
        for(var i = 0; i < count; i++){
            $(":radio[value=1]").prop('checked', true);
        }
    }

    function mark_all_absent() {
        var count = <?php echo count($attendance_of_students); ?>;
        for(var i = 0; i < count; i++)
            $(":radio[value=2]").prop('checked', true);
    }

function check_validation(){
    if(class_selection !== ''){
        $('#submit').removeAttr('disabled')
    }
    else{
        $('#submit').attr('disabled', 'disabled');
    }
}	

    function get_class_subject(department_id) {
		var class_id = $("#class_id").val();
		if (department_id !== '' && class_id !== '') {
		
		$.ajax({
            url: '<?php echo site_url('student_portal/sub_admin/attendance_get_subject/');?>' + class_id  + '/' + department_id,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
	  }
	}

$('#class_selection').change(function(){
    class_selection = $('#class_selection').val();
    check_validation();
});
</script>
