<?php
	$class_name		 	= 	$this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
	$exam_name  	    = 	$this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;
	$department_name  	= 	$this->db->get_where('department' , array('department_id' => $department_id))->row()->name;
	$system_name        =	$this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
	$running_year       =	$this->db->get_where('sub_admin_settings' , array('type'=>'running_year'))->row()->description;
	$semester       	=	$this->db->get_where('sub_admin_settings' , array('type'=>'semester'))->row()->description;
?>
<div id="print">
	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<style type="text/css">
		td {
			padding: 5px;
		}
	</style>

	<center>
		<img src="<?php echo base_url(); ?>uploads/logo.png" style="max-height : 60px;"><br>
		<h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
		<?php echo get_phrase('tabulation_sheet');?><br>
		<?php echo $class_name .' - '. $department_name;?><br>
		<?php echo $exam_name;?>


	</center>


	<table style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;" border="1">
		<thead>
			<tr>
			<td style="text-align: center;">
				<?php echo get_phrase('students');?> <i class="entypo-down-thin"></i> | <?php echo get_phrase('courses');?> <i class="entypo-right-thin"></i>
			</td>
			<?php
				//$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'year' => $running_year))->result_array();
				$subjects = $this->db->get_where('subject' , array('class_id' => $class_id , 'department_id' => $department_id, 'semester' => $semester))->result_array();
				foreach($subjects as $row):
			?>
				<td style="text-align: center;"><?php echo $row['name'];?></td>
			<?php endforeach;?>
			<td style="text-align: center;"><?php echo get_phrase('total');?></td>
			<!--<td style="text-align: center;"><?php echo get_phrase('average_grade_point');?></td>-->
			</tr>
		</thead>
		<tbody>
		<?php

			$students = $this->db->get_where('enroll' , array('class_id' => $class_id , 'section_id' => $department_id, 'year' => $running_year))->result_array();
				foreach($students as $row):
		?>
			<tr>
				<td style="text-align: center;">
					<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->first_name . ' ' . $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->last_name;?>
				</td>
			<?php
				$total_marks = 0;
				$total_grade_point = 0;
				$total_credit_hours = 0;
				foreach($subjects as $row2):
			?>
				<td style="text-align: center;">
					<?php
						$obtained_mark_query = 	$this->db->get_where('mark' , array(
												'class_id' => $class_id ,
													'exam_id' => $exam_id ,
														'subject_id' => $row2['subject_id'] ,
															'student_id' => $row['student_id'],
																'year' => $running_year,
																	'semester' => $semester
											));
						if ( $obtained_mark_query->num_rows() > 0) {
							$obtained_marks = $obtained_mark_query->row()->exam_mark;
							$subject_ins 	= $this->crud_model->get_subject_info($row2['subject_id']);
							echo $obtained_marks;
							
	
							if ($obtained_marks >= 0 && $obtained_marks != '') {
								$grade = $this->crud_model->get_grade($obtained_marks);
								foreach($subject_ins as $subject_in):
									$total_credit_hours += $subject_in['credit_hour'];
									$total_grade_point += $subject_in['credit_hour'] * $grade['grade_point'];
								endforeach;
							}

							$total_marks += $obtained_marks;
						}


					?>
				</td>
			<?php endforeach;?>
			<td style="text-align: center;"><?php echo $total_marks;?></td>
			<!--
			<td style="text-align: center;">
				<?php
					if($total_credit_hours !== 0 || $total_grade_point !== 0)
						echo number_format($total_grade_point/$total_credit_hours, 1);
					else 
						echo get_phrase('0');
				?>
			</td>
		    -->
			</tr>

		<?php endforeach;?>

		</tbody>
	</table>
</div>



<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		var elem = $('#print');
		PrintElem(elem);
		Popup(data);

	});

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
