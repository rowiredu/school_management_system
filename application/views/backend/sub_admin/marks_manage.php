<hr />
<?php echo form_open(site_url('student_portal/sub_admin/marks_selector'));?>
<div class="row">

	<div class="col-md-2">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('exam');?></label>
			<select name="exam_id" id="exam_id" class="form-control selectboxit">
				<?php
					$exams = $this->db->get_where('exam' , array('semester' => $semester , 'year' => $running_year))->result_array();
					foreach($exams as $row):
				?>
				<option value="<?php echo $row['exam_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('assessment');?></label>
			<select name="assessment_id" id="assessment_id" class="form-control selectboxit">
				<?php
					$assessments = $this->db->get_where('assessment' , array('semester' => $semester , 'year' => $running_year))->result_array();
					foreach($assessments as $row):
				?>
				<option value="<?php echo $row['assessment_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('level');?></label>
			<select name="class_id" class="form-control selectboxit" id="class_id" onchange="get_class_subject(this.value, 'level')">
				<option value=""><?php echo get_phrase('select_level');?></option>
				<?php
					$classes = $this->db->get('class')->result_array();
					foreach($classes as $row):
				?>
				<option value="<?php echo $row['class_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>

	<div>
		<div class="col-md-3">
			<div class="form-group">
			<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('department');?></label>
				<select name="department_id" id="department_id" class="form-control selectboxit" onchange="get_class_subject(this.value, 'department')">
					<option value=""><?php echo get_phrase('select_department'); ?></option>
					<?php
					$departments = $this->db->get('department')->result_array();
					foreach($departments as $row):
					?>
						<option value="<?php echo $row['department_id'];?>">
								<?php echo $row['name'];?>
						</option>
					<?php
					endforeach;
					?>
				</select>
			</div>
		</div>
		<div id="subject_holder">
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
					<select name="subject_id" id="subject_id" class="form-control selectboxit" disabled="disabled">
						<option value=""><?php echo get_phrase('select_department_first');?></option>		
					</select>
				</div>
			</div>
			<div class="col-md-2" style="margin-top: 20px;">
				<center>
					<button type="submit" class="btn btn-info" id = "submit"><?php echo get_phrase('manage_marks');?></button>
				</center>
			</div>
		</div>
	</div>

</div>
<?php echo form_close();?>





<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#submit").attr('disabled', 'disabled');
});

	function get_class_subject(selector_id, selector) {
		var exam_id 		= $("#exam_id").val()
		var department_id   = ''
		var class_id		= ''

		switch(selector) {
			case ('department'):
				department_id = selector_id
				class_id      = $("#class_id").val()
			break
			case ('level'):
				department_id = $("#department_id").val()
				class_id = selector_id
			break
			default:
				return
		}
		
		
		if (department_id !== '' && exam_id !== '' && class_id != '') {
		
		$.ajax({
            url: '<?php echo site_url('student_portal/sub_admin/marks_get_subject/');?>' + department_id  + '/' + exam_id + '/' + class_id,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
        $('#submit').removeAttr('disabled');
	  }
	  else{
	  	$('#submit').attr('disabled', 'disabled');
	  }
	}
</script>