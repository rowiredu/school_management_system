<?php
	$this->db   = $this->load->database('database2', true);
	$sub_admin_details = $this->db->get_where('sub_admin', array('sub_admin_id' => $param2))->row_array()
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title">
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('update_sub_admin');?>
            	</div>
            </div>
			<div class="panel-body">

                <?php echo form_open(site_url('student_portal/sub_admin/sub_admin/edit/'.$param2) , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('first_name');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="first_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                            	value="<?php echo $sub_admin_details['first_name']; ?>">
						</div>
					</div>

                    <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('last_name');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="last_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                            	value="<?php echo $sub_admin_details['last_name']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email" value="<?php echo $sub_admin_details['email']; ?>" data-validate="required">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="phone" value="<?php echo $sub_admin_details['phone']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

						<div class="col-sm-5">
							<textarea name="address" class = "form-control" rows="8" cols="80"><?php echo $sub_admin_details['address']; ?></textarea>
						</div>
					</div>

                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default"><?php echo get_phrase('update_sub_admin');?></button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
