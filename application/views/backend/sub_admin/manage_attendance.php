<hr />

<?php echo form_open(site_url('student_portal/sub_admin/attendance_selector/'));?>
<div class="row">

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
			<input type="text" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
				value="<?php echo date("d-m-Y");?>"/>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('level');?></label>
			<select name="class_id" class="form-control selectboxit" id = "class_id">
				<option value=""><?php echo get_phrase('select_level');?></option>
				<?php
					$classes = $this->db->get('class')->result_array();
					foreach($classes as $row):
                                            
				?>
                                
				<option value="<?php echo $row['class_id'];?>"
					><?php echo $row['name'];?></option>
                                
				<?php endforeach;?>
			</select>
		</div>
	</div>

	
	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('department');?></label>
			<select name="section_id" id="" class="form-control selectboxit" onchange="get_class_subject(this.value)">
				<option value=""><?php echo get_phrase('select_department'); ?></option>
				<?php
				$departments = $this->db->get('department')->result_array();
				foreach($departments as $row):
				?>
					<option value="<?php echo $row['department_id'];?>">
							<?php echo $row['name'];?>
					</option>
				<?php
				endforeach;
				?>
			</select>
		</div>
	</div>

	<div id="subject_holder">
		<div class="col-md-3">
			<div class="form-group">
			<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
				<select name="subject_id" id="subject_id" class="form-control selectboxit" disabled="disabled">
					<option value=""><?php echo get_phrase('select_department_first');?></option>		
				</select>
			</div>
		</div>
		<div class="col-md-3" style="margin-top: 20px;">
			<button type="submit" class="btn btn-info" id="submit"><?php echo get_phrase('manage_attendance');?></button>
		</div>
	</div>
	
	<input type="hidden" name="year" value="<?php echo $running_year;?>">

</div>
<?php echo form_close();?>

<script type="text/javascript">
var class_selection = "";
jQuery(document).ready(function($) {
	$('#submit').attr('disabled', 'disabled');
});

/*
function select_section(class_id) {
	if(class_id !== ''){
		$.ajax({
			url: '<?php echo site_url('student_portal/sub_admin/get_section/'); ?>' + class_id,
			success:function (response)
			{

			jQuery('#section_holder').html(response);
			}
		});
	}
}*/

function check_validation(){
	if(class_selection !== ''){
		$('#submit').removeAttr('disabled')
	}
	else{
		$('#submit').attr('disabled', 'disabled');
	}
}
/*
$('#class_selection').change(function(){
	class_selection = $('#class_selection').val();
	check_validation();
});*/
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#submit").attr('disabled', 'disabled');
});

	function get_class_subject(department_id) {
		
		var class_id = $("#class_id").val();
		if (department_id !== '' && class_id !== '') {
		
		$.ajax({
            url: '<?php echo site_url('student_portal/sub_admin/attendance_get_subject/');?>' + class_id  + '/' + department_id,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
        $('#submit').removeAttr('disabled');
	  }
	  else{
	  	$('#submit').attr('disabled', 'disabled');
	  }
	}
</script>