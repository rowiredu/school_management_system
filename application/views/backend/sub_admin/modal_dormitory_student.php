<?php 
    $this->db           = $this->load->database('database2', true);
    $dormitory_students = $this->db->get_where('student' , array('dormitory_id' => $param2))->result_array();
    $running_year       = $this->db->get_where('sub_admin_settings' , array('type' => 'running_year'))->row()->description;
?>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>#</td>
                    <td><?php echo get_phrase('name');?></td>
                    <td><?php echo get_phrase('email').'/'.get_phrase('username');?></td>
                    <td><?php echo get_phrase('phone');?></td>
                    <td><?php echo get_phrase('class');?></td>
                </tr>
            </thead>
            <tbody>
            <?php 
                $count = 1;
                foreach($dormitory_students as $row):
            ?>
                <tr>
                    <?php 
                        $class_query = $this->db->get_where('enroll' , array('student_id' => $row['student_id'], 'year' => $running_year, 'class_id' => '1'))->row();
                        if($class_query != null) {
                            if($class_query->class_id == '1') {
                    ?>
                                <td><?php echo $count++;?></td>
                                <!--<td><?php echo $row['name'];?></td>-->
                                <td><?php echo $row['first_name'] . ' ' . $row['last_name'];?></td>
                                <td><?php echo $row['email'];?></td>
                                <td><?php echo $row['phone'];?></td>
                                <td>
                                    <!--<?php echo $this->db->get_where('class' , array('class_id' => $row['class_id']))->row()->name;?>-->
                                    <?php 
                                        echo $this->crud_model->get_class_name($class_query->class_id);
                                    ?>
                                </td>
                            <?php
                            }
                        }
                        ?>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>