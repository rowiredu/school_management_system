<hr />
<div class="row" style="text-align: center;">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<div class="tile-stats tile-gray">
			<div class="icon"><i class="entypo-chart-bar"></i></div>
			
			<h3 style="color: #696969;"><?php echo get_phrase('marks_for');?> <?php echo $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;?></h3>
			<h4 style="color: #696969;">
				<?php echo $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;?> : 
				<?php echo $this->db->get_where('department' , array('department_id' => $section_id))->row()->name;?> 
			</h4>
			<h4 style="color: #696969;">
				<?php echo get_phrase('course');?> : <?php 
				if($subject_id !== ""){
					echo $this->db->get_where('subject' , array('subject_id' => $subject_id))->row()->name;
				}
				else {
					echo "<span>No course for the selected fields.</span>";
				}
				?>
			</h4>
		</div>
	</div>
	<div class="col-sm-4"></div>
</div>
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">

		<?php echo form_open(site_url('student_portal/teacher/marks_update/'.$exam_id.'/'.$assessment_id.'/'.$class_id.'/'.$section_id.'/'.$subject_id));?>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo get_phrase('id');?></th>
						<th><?php echo get_phrase('name');?></th>
						<th><?php echo get_phrase('level');?></th>
						<th><?php echo get_phrase('exams_marks');?></th>
						<th><?php echo get_phrase('assessment_marks');?></th>
					</tr>
				</thead>
				<tbody>
				<?php
					$count = 1;
					$marks_of_students = $this->db->get_where('mark' , array(
						/*'class_id' => $class_id,*/ 
							'section_id' => $section_id ,
								'year' => $running_year,
									'subject_id' => $subject_id,
										'exam_id' => $exam_id,
										 'assessment_id' => $assessment_id,
					))->result_array();
					
					foreach($marks_of_students as $row):
				?>
					<tr>
						<td><?php echo $count++;?></td>

                        <td><?php echo $this->db->get_where('student',array('student_id'=>$row['student_id']))->row()->student_code;?></td>

						<td>
							<?php echo $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->first_name . ' ' . $this->db->get_where('student' , array('student_id' => $row['student_id']))->row()->last_name;?>
						</td>

						<td>
							<?php echo $this->crud_model->get_class_name($this->db->get_where('enroll', array('student_id' => $row['student_id'], 'year' => $running_year))->row()->class_id);?>
						</td>
						
						<td>
							<input type="text" class="form-control" name="exam_mark_<?php echo $row['mark_id'];?>"
								value="<?php echo $row['exam_mark'];?>">	
						</td>
						<td>
							<input type="text" class="form-control" name="assessment_mark_<?php echo $row['mark_id'];?>"
								value="<?php echo $row['assessment_mark'];?>">
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>

		<center>
			<button type="submit" class="btn btn-success" id="submit_button">
				<i class="entypo-check"></i> <?php echo get_phrase('save_changes');?>
			</button>
		</center>
		<?php echo form_close();?>
		
	</div>
	<div class="col-md-2"></div>
</div>

<?php //var_dump($this->uri->segment(6));?>



<script type="text/javascript">
	

	function get_class_subject(department_id) {
	var exam_id = $("#exam_id").val();
	if (department_id !== '' && exam_id !== '') {
	$.ajax({
            url: '<?php echo site_url('student_portal/teacher/marks_get_subject/');?>' + department_id + '/' + exam_id,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
	  }
	}


</script>