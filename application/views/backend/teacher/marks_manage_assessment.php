<hr />
<div class="row">
<?php
	$semester = $this->db->get_where('teacher_settings' , array('type' => 'semester'))->row()->description;
	$subjects = $this->db->get_where('subject' , array(
		//'department_id' => $department_id ,
			'teacher_id' => $this->session->userdata('teacher_id'),
				'semester' => $semester
	))->result_array();
	//var_dump($subjects);
	foreach($subjects as $subject_row): 
?>
<?php echo form_open(site_url('student_portal/teacher/marks_selector_assessment'));?>


			<select name="assessment_id" id="assessment_id" class="form-control" style="display: none;">
				<?php
					$assessments = $this->db->get_where('assessment' , array('semester' => $semester , 'year' => $running_year))->result_array();
					foreach($assessments as $row):
				?>
				<option value="<?php echo $row['assessment_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>

			<input type="hidden" name="class_id" class="form-control" value="<?php echo $subject_row['class_id']?>">
			<input type="hidden" name="department_id" id="" class="form-control" value="<?php echo $subject_row['department_id']?>">
			<input type="hidden" name="subject_id" id="subject_id" class="form-control" value="<?php echo $subject_row['subject_id']?>">
	
			<button type="submit" class="tile-stats tile-white col-md-2 col-sm-12" id = "submit" style="margin: 20px;">
			<div>
				<!--<div class="icon"><i class="entypo-docs"></i></div>-->
				<h4>Course</h4>
				
				<div><h3><?php echo $subject_row['name'];?></h3></div>
				<p><?php echo $this->crud_model->get_class_name($subject_row['class_id']);?></p>
			</div>
			</button>

<?php echo form_close();?>
<?php endforeach;?>
</div>
