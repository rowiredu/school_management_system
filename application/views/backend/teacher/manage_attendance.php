<hr />

<?php echo form_open(site_url('student_portal/teacher/attendance_selector/'));?>
<div class="row">

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
			<input type="text" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
				value="<?php echo date("d-m-Y");?>"/>
		</div>
	</div>

	<?php
		$class_query = $this->db->get('class');
		if($class_query->num_rows() > 0):
			$classes = $class_query->result_array();
	?>

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('level');?></label>
			<select class="form-control selectboxit" name="class_id" id="class_id">
				<?php foreach($classes as $row):?>
					<option value="<?php echo $row['class_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>
	<?php endif;?>

	<div class="col-md-3">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('department');?></label>
			<select name="section_id" id="" class="form-control selectboxit" onchange="get_class_subject(this.value)">
				<option value=""><?php echo get_phrase('select_department'); ?></option>
				<?php
				$departments = $this->db->get('department')->result_array();
				foreach($departments as $row):
				?>
					<option value="<?php echo $row['department_id'];?>">
							<?php echo $row['name'];?>
					</option>
				<?php
				endforeach;
				?>
			</select>
		</div>
	</div>

		<!--<input type="hidden" name="class_id" value="<?php echo $class_id;?>">-->
		<input type="hidden" name="year" value="<?php echo $running_year;?>">
	
	<div id="subject_holder">
		<div class="col-md-3">
			<div class="form-group">
			<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('course');?></label>
				<select name="subject_id" id="subject_id" class="form-control selectboxit" disabled="disabled">
					<option value=""><?php echo get_phrase('select_department_first');?></option>		
				</select>
			</div>
		</div>
		<div class="col-md-3" style="margin-top: 20px;">
			<button type="submit" class="btn btn-info" id="submit"><?php echo get_phrase('manage_attendance');?></button>
		</div>
	</div>




</div>
<?php echo form_close();?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#submit").attr('disabled', 'disabled');
});

	function get_class_subject(department_id) {
		
		var class_id = $("#class_id").val();
		if (department_id !== '' && class_id !== '') {
		
		$.ajax({
            url: '<?php echo site_url('student_portal/teacher/attendance_get_subject/');?>' + class_id  + '/' + department_id,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
        $('#submit').removeAttr('disabled');
	  }
	  else{
	  	$('#submit').attr('disabled', 'disabled');
	  }
	}
</script>