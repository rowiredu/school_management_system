<style>
	#submit {
		border: 1px solid #696969;
		/*background: #f2f2f2;*/
	}
</style>

<hr />
<!--<div><h3 style="color: #696969"><?php echo get_phrase('courses');?></h3></div>-->
<div class="row animated fadeIn">
<?php
	$semester = $this->db->get_where('teacher_settings' , array('type' => 'semester'))->row()->description;
	$subjects = $this->db->get_where('subject' , array(
		//'department_id' => $department_id ,
			'teacher_id' => $this->session->userdata('teacher_id'),
				'semester' => $semester
	))->result_array();
	//var_dump($subjects);
	foreach($subjects as $subject_row): 
?>
<?php echo form_open(site_url('student_portal/teacher/marks_selector'));?>


			<select name="exam_id" id="exam_id" class="form-control" style="display: none;">
				<?php
					$exams = $this->db->get_where('exam' , array('semester' => $semester , 'year' => $running_year))->result_array();
					foreach($exams as $row):
				?>
				<option value="<?php echo $row['exam_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>

			<select name="assessment_id" id="assessment_id" class="form-control" style="display: none;">
				<?php
					$assessments = $this->db->get_where('assessment' , array('semester' => $semester , 'year' => $running_year))->result_array();
					foreach($assessments as $row):
				?>
				<option value="<?php echo $row['assessment_id'];?>"><?php echo $row['name'];?></option>
				<?php endforeach;?>
			</select>

			<input type="hidden" name="class_id" class="form-control" value="<?php echo $subject_row['class_id']?>">
			<input type="hidden" name="department_id" id="" class="form-control" value="<?php echo $subject_row['department_id']?>">
			<input type="hidden" name="subject_id" id="subject_id" class="form-control" value="<?php echo $subject_row['subject_id']?>">

			<button type="submit" class="tile-stats tile-white col-md-2 col-sm-12 sbt-btn" id = "submit" style="border: 1px solid rgba(0,0,0,0.15); box-shadow: 0px 2px 10px #f6f6f6; border-radius: 20px;min-height: 100px; max-height: 100px; margin: 8px;">
			<!--<div class="icon"><i style="font-size: 58px;" class="entypo-docs"></i></div>-->
			<div>
				<div><h3 style="color: #4a6767; font-size: 16px;"><?php echo $subject_row['name'];?></h3></div>
				<p style="color: #696969;" ><?php echo $this->crud_model->get_class_name($subject_row['class_id']);?></p>
			</div>
			</button>

<?php echo form_close();?>
<?php endforeach;?>
</div>

<!----     MDB CSS     ---->
<link rel="stylesheet" href="<?php echo base_url('assets/css/mdb.min.css');?>">

<!----    MDB JS ----->
<script src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>