<!-- Footer -->
<footer class="main">
	&copy; <?php echo date('yy')?> <strong> <?php echo $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description; ?> | Version 5.6</strong>
    Developed by
	<a href="http://zentechgh.com"
    	target="_blank">Zentech Ghana</a>
</footer>
