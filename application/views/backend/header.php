<?php 
	$this->db     = $this->load->database('database2', true);

		$running_year = $account_type !== 'student' ? $this->db->get_where($account_type.'_settings', array('type' => 'running_year'))->row()->description : $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
		$semester     = $account_type !== 'student' ? $this->db->get_where($account_type.'_settings', array('type' => 'semester'))->row()->description : $this->db->get_where('settings', array('type' => 'semester'))->row()->description;

?>
<div class="row">
	<div class="col-md-12 col-sm-12 clearfix" style="text-align:center;">
		<h2 style="font-weight:200; margin:0px;"><?php echo $system_name;?></h2>
    </div>
	<!-- Raw Links -->
	<div class="col-md-12 col-sm-12 clearfix ">

        <ul class="list-inline links-list pull-left">
        <!-- Language Selector -->
         	<div >
	           <li>
	           		<h4>
	           			<a href="#" style="color: #696969;" id="session_static"
	           				<?php if($account_type == 'admin' || $account_type == 'sub_admin' || $account_type == 'teacher'):?>
								onclick="get_session_changer()"
							<?php endif;?>>
	           					<?php echo get_phrase('running_session');?> : <?php echo $running_year.' ';?><i class="entypo-down-dir"></i>
	           			</a>
	           			<a href="#" style="color: #696969;" id="semester_static"
	           				<?php if($account_type == 'admin' || $account_type == 'sub_admin'|| $account_type == 'teacher'):?>
								onclick="get_semester_changer()"
							<?php endif;?>>
	           					<?php echo get_phrase('Semester');?> : <?php echo $semester.' ';?><i class="entypo-down-dir"></i>
	           			</a>
	           		</h4>
	           </li>
           </div>
        </ul>

		<ul class="list-inline links-list pull-right">

		<li class="dropdown language-selector">
			<a href="<?php echo site_url();?>" target="_blank">
				<i class="entypo-globe"></i> Website
			</a>
			
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                        	<i class="entypo-user"></i>
													<?php
														if($this->session->userdata('login_type') == 'student' || $this->session->userdata('login_type') == 'sub_admin') {
															$first_name = $this->db->get_where($this->session->userdata('login_type'), array($this->session->userdata('login_type').'_id' => $this->session->userdata('login_user_id')))->row()->first_name;
															$last_name = $this->db->get_where($this->session->userdata('login_type'), array($this->session->userdata('login_type').'_id' => $this->session->userdata('login_user_id')))->row()->last_name;

															echo $first_name . ' ' . $last_name;
														
														}else {
															$name = $this->db->get_where($this->session->userdata('login_type'), array($this->session->userdata('login_type').'_id' => $this->session->userdata('login_user_id')))->row()->name;
															echo $name;
														}
													?>
                    </a>

				<?php if ($account_type != 'parent'):?>
				<ul class="dropdown-menu <?php if ($text_align == 'right-to-left') echo 'pull-right'; else echo 'pull-left';?>">
					<li>
						<a href="<?php echo site_url('student_portal/' . $account_type . '/manage_profile');?>">
                        	<i class="entypo-info"></i>
							<span><?php echo get_phrase('edit_profile');?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo site_url('student_portal/' .$account_type . '/manage_profile');?>">
                        	<i class="entypo-key"></i>
							<span><?php echo get_phrase('change_password');?></span>
						</a>
					</li>
				</ul>
				<?php endif;?>
				<?php if ($account_type == 'parent'):?>
				<ul class="dropdown-menu <?php if ($text_align == 'right-to-left') echo 'pull-right'; else echo 'pull-left';?>">
					<li>
						<a href="<?php echo site_url('student_portal/parents/manage_profile');?>">
                        	<i class="entypo-info"></i>
							<span><?php echo get_phrase('edit_profile');?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo site_url('student_portal/parents/manage_profile');?>">
                        	<i class="entypo-key"></i>
							<span><?php echo get_phrase('change_password');?></span>
						</a>
					</li>
				</ul>
				<?php endif;?>
			</li>

			<li>
			<?php if($account_type == 'admin' || $account_type == 'sub_admin' || $account_type == 'teacher'):?>
				<a href="<?php echo site_url('student_portal/staff_login/logout');?>">
					<?php echo get_phrase('log_out'); ?><i class="entypo-logout right"></i>
				</a>
			<?php else: ?>
				<a href="<?php echo site_url('student_portal/login/logout');?>">
					<?php echo get_phrase('log_out'); ?><i class="entypo-logout right"></i>
				</a>
			<?php endif;?>
			</li>
		</ul>
	</div>

</div>

<hr style="margin-top:0px;" />

<script type="text/javascript">
	function get_session_changer()
	{
		$.ajax({
            url: '<?php echo site_url('student_portal/'.$account_type.'/get_session_changer');?>',
            success: function(response)
            {
                jQuery('#session_static').html(response);
            }
        });
	}

	function get_semester_changer()
	{
		$.ajax({
            url: '<?php echo site_url('student_portal/'.$account_type.'/get_semester_changer');?>',
            success: function(response)
            {
                jQuery('#semester_static').html(response);
            }
        });
	}
</script>

<style>
            * {
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }
</style>