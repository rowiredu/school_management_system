
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-meter"></i> Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12 mb-4">
                        <div id="morris-bar-example" class="dash-chart"></div>

                        <!--div class="mt-4 text-center">
                            <button type="button" class="btn btn-outline-light ml-1 waves-effect waves-light">In Progress</button>
                            <button type="button" class="btn btn-outline-info ml-1 waves-effect waves-light"> Submitted </button>
                            <button type="button" class="btn btn-outline-light ml-1 waves-effect waves-light"> Qualified </button>
                        </div-->
                    </div>
                </div>
            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">

                    <?php foreach($stat as $k => $s){ ?>
                        <div class="col-md-6 col-xl-2" style="max-width: 20% !important;">
                            <div class="card text-center m-b-30">
                                <a class="mb-2 card-body text-muted" href="<?=site_url('portal/approval_list/index/'.$k)?>">
                                    <h3 class="text-info"> <?=$s?> </h3>
                                    Total <?=humanize($k)?> 
                                </a>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <!-- end row -->

                <!--div class="row">
                    <div class="col-xl-8">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <h4 class="mt-0 m-b-30 header-title">Latest Transactions</h4>

                                <div class="table-responsive">
                                    <table class="table m-t-20 mb-0 table-vertical">

                                        <tbody>
                                            <?php foreach($transactions as $transaction): ?>
                                                <tr>
                                                    <td>
                                                        <img src="<?=base_url('assets/default.jpg')?>" alt="user-image" class="thumb-sm rounded-circle mr-2"/>
                                                        <?=$transaction->first_name?>
                                                    </td>
                                                    <td>
                                                        <?=$transaction->last_name?>
                                                    </td>
                                                    <td>
                                                        <?=$transaction->email?>
                                                        <p class="m-0 text-muted font-14">email</p>
                                                    </td>
                                                    <td>
                                                        <?=$transaction->phone?>
                                                        <p class="m-0 text-muted font-14">phone</p>
                                                    </td>
                                                    <td>
                                                        <?=$transaction->serial_number?>
                                                        <p class="m-0 text-muted font-14">serial_number</p>
                                                    </td>
                                                    <td>
                                                        <?=$transaction->reference?>
                                                        <p class="m-0 text-muted font-14">reference</p>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <h4 class="mt-0 m-b-15 header-title">Recent Admission Sessions</h4>

                                <ol class="activity-feed mb-0">
                                    <?php if(isset($ad_year)){ ?>
                                        <?php foreach($ad_year as $a){ ?>
                                            <li class="feed-item">
                                                <span class="date"><?=$a->start?> to <?=$a->end?> </span>
                                                <span class="activity-text"> <?=$a->year?> </span>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
