<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Entrance University College of Health Science</title>
        <meta content="Entrance Dashboard" name="description" />
        <meta content="Emmanuel Kwabena Dadzie" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/portal/assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/portal/assets/plugins/morris/morris.css">

        <!-- DataTables -->
        <link href="<?=base_url()?>assets/portal/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="<?=base_url()?>assets/portal/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?=base_url()?>assets/portal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/css/style.css" rel="stylesheet" type="text/css" />


        <!-- Plugins css -->
        <link href="<?=base_url()?>assets/portal/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/portal/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/portal/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/user_applicant_portal/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <script>
            function printElem(divId) {
                var content = document.getElementById(divId).innerHTML;
                var mywindow = window.open('', 'Print', 'height=600,width=800');

                mywindow.document.write('<html><head><title>Print</title>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(content);
                mywindow.document.write('</body></html>');

                mywindow.document.close();
                mywindow.focus()
                mywindow.print();
                mywindow.close();
                return true;
            }
            function resend(str) {
                var xhttp = new XMLHttpRequest();
                var url   = "<?=base_url('index.php/portal/payment/resend_ajax/')?>"+str;
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("resend"+str).innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", url , true);
                xhttp.send();
            }
        </script>
        <!--script type='text/javascript'>
            var logout = "<?=base_url("admission/auth/logout")?>";

            window.onbeforeunload = function (e) {
                myWindow = window.open( logout , "myWindow");   // Opens a new 
                myWindow.close();   // Closes the new window

                var message = 'Still want to leave this application.';
                e = e || window.event;
                // For IE and Firefox
                if (e) {
                    e.returnValue = message;
                }
            

                // For Safari
                return message;
            };
        </script-->

    </head>

