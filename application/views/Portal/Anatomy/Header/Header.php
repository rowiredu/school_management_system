
    <body>


<?php if ($this->ion_auth->is_cashier() || $this->ion_auth->is_banker() ){ ?>
    <?php if (!$this->ion_auth->is_admin() && "payment" != $this->uri->segment(2) ){ redirect('portal/payment'); }?>
<?php } ?>
<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">

                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <a href="<?=site_url()?>" class="logo">
                        Entrance University College of Health Science
                    </a>
                    <!-- Image Logo -->
                    <!-- <a href="index.html" class="logo">
                        <img src="assets/images/logo-sm.png" alt="" height="22" class="logo-small">
                        <img src="assets/images/logo.png" alt="" height="24" class="logo-large">
                    </a> -->

                </div>
                <!-- End Logo container-->


                <div class="menu-extras topbar-custom">

                    <ul class="list-inline float-right mb-0">
                        
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="false" aria-expanded="false">
                                <img src="<?=base_url('assets/default.jpg')?>" alt="user" class="rounded-circle">
                                <span class="ml-1"> <?=$user_details->first_name?> <i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!--a class="dropdown-item" href="#"><i class="dripicons-user text-muted"></i> Profile</a-->
                                <!--a class="dropdown-item" href="#"><i class="dripicons-lock text-muted"></i> Lock screen</a-->
                                <div class="dropdown-divider"></div>
                                
                                <a class="dropdown-item" href="<?=site_url('portal/user/change_password')?>"><i class="dripicons-exit text-muted"></i> Password</a>
                                <a class="dropdown-item" href="<?=site_url('portal/auth/logout')?>"><i class="dripicons-exit text-muted"></i> Logout</a>
                            </div>
                        </li>
                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                    </ul>
                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->

        <!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <!--//is_cashier is_banker -->
                        <?php if ($this->ion_auth->is_admin() ){ ?>
                            <li class="has-submenu">
                                <a href="<?=site_url('portal/dashboard')?>"><i class="dripicons-device-desktop"></i>Dashboard</a>
                            </li>
                        <?php } ?>

                        
                        <?php if ($this->ion_auth->is_cashier() || $this->ion_auth->is_banker() ){ ?>
                            <li>
                                <a href="<?=site_url('portal/payment')?>">  Payments </i></a>
                            </li>
                        <?php } ?>


                        <?php if ($this->ion_auth->is_admin()){ ?>
                            <li>
                                <a href="<?=site_url('portal/academics')?>">  Acadamic Year </i></a>
                            </li>


                                <li class="has-submenu">
                                    <a href="#"><i class="dripicons-trophy"></i>Admissions <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                    <ul class="submenu">

                                        <?php foreach($stat as $k => $s){ ?>
                                            <li><a href="<?=site_url('portal/approval_list/index/'.$k)?> "> <?=humanize($k)?> (<?=$s?>) </a></li>
                                        <?php }?>
                                    </ul>
                                </li>

                            <li>
                                <a href="<?=site_url('portal/user')?>"> User </i></a>
                            </li>
                        <?php } ?>


                    </ul>
                    <!-- End navigation menu -->
                </div> <!-- end #navigation -->
            </div> <!-- end container -->
        </div> <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->
