

        <!-- Footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <small>© 2018 Entrance University College of Health Science . All rights reserved. Powered by <a target="_blank" href="https://www.zentechgh.com/"> Zentech GH</a></small>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/popper.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/modernizr.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/waves.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.scrollTo.min.js"></script>

        <!-- Required datatable js -->
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/buttons.colVis.min.js"></script>

        <!-- Responsive examples -->
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!--Morris Chart-->
        <script src="<?=base_url()?>assets/portal/assets/plugins/morris/morris.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/raphael/raphael-min.js"></script>

        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/portal/assets/pages/datatables.init.js"></script>

        <!-- Charts init js -->
        <script>

            !function ($) {
                "use strict";

                var Dashboard = function () {
                };
                    //creates Bar chart
                    Dashboard.prototype.createBarChart = function (element, data, xkey, ykeys, labels, lineColors) {
                        Morris.Bar({
                            element: element,
                            data: data,
                            xkey: xkey,
                            ykeys: ykeys,
                            labels: labels,
                            gridLineColor: 'rgba(255,255,255,0.1)',
                            gridTextColor: '#98a6ad',
                            barSizeRatio: 0.2,
                            resize: true,
                            hideHover: 'auto',
                            barColors: lineColors
                        });
                    },

                    //creates area chart
                    Dashboard.prototype.createAreaChart = function (element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
                        Morris.Area({
                            element: element,
                            pointSize: 0,
                            lineWidth: 0,
                            data: data,
                            xkey: xkey,
                            ykeys: ykeys,
                            labels: labels,
                            resize: true,
                            gridLineColor: '#eee',
                            hideHover: 'auto',
                            lineColors: lineColors,
                            fillOpacity: .6,
                            behaveLikeLine: true
                        });
                    },

                    //creates Donut chart
                    Dashboard.prototype.createDonutChart = function (element, data, colors) {
                        Morris.Donut({
                            element: element,
                            data: data,
                            resize: true,
                            colors: colors,
                        });
                    },

                    Dashboard.prototype.init = function () {

                        //creating bar chart
                        var $barData = [
                            <?php foreach($stat as $k => $s){ ?>
                                {y: '<?=humanize($k)?>', a: <?=$s?>, b: <?=$s?>},
                            <?php }?>
                        ];
                        this.createBarChart('morris-bar-example', $barData, 'y', ['a', 'b'], ['Series A', 'Series B'], ['#2f8ee0','#4bbbce']);

                        //creating area chart
                        var $areaData = [
                            {y: '2007', a: 0, b: 0, c:0},
                            {y: '2008', a: 150, b: 45, c:15},
                            {y: '2009', a: 60, b: 150, c:195},
                            {y: '2010', a: 180, b: 36, c:21},
                            {y: '2011', a: 90, b: 60, c:360},
                            {y: '2012', a: 75, b: 240, c:120},
                            {y: '2013', a: 30, b: 30, c:30}
                        ];
                        this.createAreaChart('morris-area-example', 0, 0, $areaData, 'y', ['a', 'b', 'c'], ['Series A', 'Series B', 'Series C'], ['#ccc', '#2f8ee0', '#4bbbce']);

                        //creating donut chart
                        var $donutData = [
                            {label: "Marketing", value: 12},
                            {label: "Online", value: 42},
                            {label: "Offline", value: 20}
                        ];
                        this.createDonutChart('morris-donut-example', $donutData, ['#f0f1f4', '#2f8ee0', '#4bbbce']);

                    
                    },
                    //init
                    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
            }(window.jQuery),

            //initializing
                function ($) {
                    "use strict";
                    $.Dashboard.init();
                }(window.jQuery);

    </script>
        <!-- Plugins js -->
        <script src="<?=base_url()?>assets/portal/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/portal/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/js/toastr.js" type="text/javascript"></script>
        <!-- Plugins Init js -->
        <script src="<?=base_url()?>assets/portal/assets/pages/form-advanced.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/portal/assets/js/app.js"></script>
        <?=$js_show ? $js_show : ""?>
               <!-- SHOW TOASTR NOTIFIVATION -->
               <?php if ($this->session->flashdata('flash_message') != ""):?>

        <script type="text/javascript">
            toastr.success('<?php echo $this->session->flashdata("flash_message");?>');
        </script>

        <?php endif;?>

        <?php if ($this->session->flashdata('error_message') != ""):?>

        <script type="text/javascript">
            toastr.error('<?php echo $this->session->flashdata("error_message");?>');
        </script>

        <?php endif;?>
    </body>

</html>