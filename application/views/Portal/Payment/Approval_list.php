
            <style>
                .modal-dialog {
                    max-width: 75%;
                    margin: 1.75rem auto;
                }
                .center{
                    margin-left:42%; 
                    margin-right:42%;
                }
            </style>
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i>  <?=$status == null ? "Application List" : $status ?> </h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title"> <?=$status == null ? "Submitted List" : $status ?></h4>
                                <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                      
                                <table id="datatables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <?php  //if(trim($status) != humanize("in_progress") ){   ?>
                                            <?php  if(trim($status)){   ?>
                                                <th>Details</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                        <?php foreach($data as $single){ ?>
                                            <?php $record = $this->students_m->get($single->applicant_id);?>
                                            <?php if($single->year_id == $last_admission){   ?>
                                                <tr>
                                                    <td><?=$record->first_name?></td>
                                                    <td><?=$record->last_name?></td>
                                                    <td><?=$record->email?></td>
                                                    <td><?=$record->phone?></td>
                                                    <?php //if(trim($status) != humanize("in_progress") ){ ?>
                                                    <?php if(trim($status)){ ?>
                                                        <td><i class='fa fa-eye ' data-toggle="modal" data-target="#records_<?=$record->id?>"></i></td>
                                                        
                                                        <!-- sample modal content -->
                                                        <div id="records_<?=$record->id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title mt-0" id="myModalLabel"> Students Details  <a href="<?=site_url('portal/approval_list/printer_out/'.$single->id)?>" class="fa fa-print"> </a></h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    </div>

                                                                    <div class="modal-body">
                                                                        <?php
                                                                            $stu_forms = json_decode($single->json_values);
                                                                            $user_picture = $record->picture == '' ? base_url('assets/default.jpg') :  base_url($record->picture) ;
                                                                            echo "<img src='".$user_picture."' class='row ' style='width:50%; float:left; padding:50px 0 0 100px;'>";
                                                                            //helper self repeat
                                                                            div_clean_render($stu_forms);

                                                                        ?>
                                                                    <div class="modal-footer">
                                                                        <a href="<?=site_url('portal/approval_list/reject/'.$single->id)?>" class="col-md-4 btn btn-danger btn-rounded btn-custom btn-block waves-effect waves-light" onclick="javascript:return confirm('Are you sure you want to reject this Document? \n\n\n There is no going back.')"> Not Admitted </a>
                                                                        <a href="<?=site_url('portal/approval_list/reverse/'.$single->id)?>" class="col-md-4 btn btn-secondary btn-rounded btn-custom btn-block waves-effect waves-light" onclick="javascript:return confirm('Are you sure you want to Reverse this Document? \n\n\n There is no going back.')"> Reverse </a>
                                                                        <a href="<?=site_url('portal/approval_list/enroll/'.$single->id)?>" class="col-md-4 btn btn-success btn-rounded btn-custom btn-block waves-effect waves-light" onclick="javascript:return confirm('Are you sure you want to approve this Document? \n\n\n There is no going back.')"> Enroll </a>
                                                                        <!--<a href="<?=site_url('portal/approval_list/approve/'.$single->id)?>" class="col-md-4 btn btn-success btn-rounded btn-custom btn-block waves-effect waves-light" onclick="javascript:return confirm('Are you sure you want to approve this Document? \n\n\n There is no going back.')"> Admitted </a>-->
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                    <?php } ?>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
