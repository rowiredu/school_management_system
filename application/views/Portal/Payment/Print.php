<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Entrance University College of Health Science (S)</title>
    <meta content="Entrance Dashboard" name="description" />
    <meta content="Emmanuel Kwabena Dadzie" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/portal/assets/images/favicon.ico">

    <link href="<?=base_url()?>assets/user_applicant_portal/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/user_applicant_portal/css/plugins/iCheck/custom.css" rel="stylesheet">
    <style>
        .footer {
            background: none repeat scroll 0 0 white;
            border-top: 1px solid #e7eaec;
            bottom: 0;
            left: 0;
            padding: 10px 20px;
            position: relative;
            margin:auto;
            right: 0;
        }

        .indent  {
            text-indent: 50px;
        }

        body {
            font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
            background-color: #ffffff;
            font-size: 13px;
            color: #676a6c;
            overflow-x: hidden;
        }

        .left{
            width:50%;
            float:left;
        }

        .right{
            width:50%;
            float:right;
        }

        .picture{
            width:20%;
            margin:auto;
        }

        .picture img{
            width:100%;
            margin:auto;
        }

        .profile{
            padding-top:20px;
            width:72%;
        }

        .pagebreak { page-break-before: always; }
        #space td, #space th{ width: 25%; }
    </style>
    <script type="text/javascript">
        window.onload=function(){
            window.open();
            window.print();
            window.close();
        }
    </script>
</head>
<!--body onmouseover="history.go(-1)" -->
<body >
    <div class="row">
        <div class="left">
            <p> <img src="<?=base_url('assets/logo.png')?>" style="width:100%; float:left;"/></p>
            <p class="indent">Online Application Forms</p>
        </div>
        <div class="right">
            <span style="float:right;">
                <p style="width:100px; font-size:9px;"> 
                    Private mail Bag <br />
                    University Post Office<br />
                    Spintex-Accra<br />
                    Ghana
                </p>

                <p> <?=date("l dS F,Y")?></p>

            </span>
        </div>
    </div>


    <div class="row" style="text-align:center;">
        <p><h3 style="font-weight:700; "> PREVIEW </h3></p>
        <p>Keep this copy on you when attending the interview</p>
        <p>Also attach your original documents of certificates for confirmation</p>
    </div>

    <?php 
        $_pd = json_decode($form_data->json_values); 
        if($_pd->user->gender == "male" ){
            $sal = "Mr. ";
        } else {
            if($_pd->user->marital_status == "single" ){
                $sal = "Miss. ";
            } else {
                $sal = "Mrs. ";
            }
        }
    ?>
    <?php 
        foreach($_pd->others->qualification as $key => $values){
            $insert_inject = "<div class='row'>";
            foreach($_pd->others->qualification[$key]->grades as $key_two => $values_two){
                if($values_two->grades_lable !== ""){
                    $insert_inject .= "<div class='row'  style='padding:10px 0px 0px 10px;'><div style='width:180px;float:left;'>".$values_two->grades_lable."</div> - <div style='width:20px;float:right;'>".$values_two->grades_grades."</div></div> <br/>"; 
                }
                
            }
            $insert_inject .= "</div>";
            $_pd->others->qualification[$key]->grades = $insert_inject;
        }
    ?>
    <table style="border-collapse:separate; border-spacing:0.5em;">
        <?=repeat_self($user_picture , $_pd  , "tr")?>
    <table>


    <?=img_only($_pd)?>

    <div class="pagebreak"> </div>
    <h4 style="font-weight:700; "> Please note that EXAMINATION DESCRIPTION (ie. index no, year of exam, etc.) will be used to pick result directly from the west African examination council(WAEC). CHECK for it Validity </h4>

    <p>IMPORTANT</p>
    <p>AN APPLICANT WHO MAKES A FALSE STATEMNET OR WHITHHOLD RELEVANT INFORMATION MAY BE REFUSED ADMISSION.IF HE HAS ALREADY COME INTO THE UNIVERSITY, HE WILL BE WITHDRAWN.</p>

    <h5>DECLARATION</h5>

    <p>I, <b><?=$sal?> <?=$_pd->user->first_name." ".$_pd->user->middle_name." ".$_pd->user->last_name?></b> certify that the information provided above is valid and will be held personally responsible for its authenticity and will bear any consequences for any invalid information provided </p>

    <table id="space">
        <tr>
            <th>Date of print</th>
            <th>Application ID</th>
            <th> Envelope number </th>
            <th>Signature of Applicant </th>
        </tr>
        <tr>
            <td colspan="3" style="height:20px;"></td>
        </tr>
        <tr>
            <td><?=date("l dS F,Y")?> </td>
            <td><?=$form_data->id?></td>
            <td> LAND/0308 </td>
            <td> ………………………………… </td>
        </tr>
    </table>                                

    <h5 style="font-weight:700; "> Please note that until we have received all the required items your application shall not be processed. </h5>
    <p>PLEASE READ THE INSTRUCTION CAREFULLY BEFORE YOU ENDORSE THIS FORM </p>
    <ul>
        <li>This declaration should be signed by a person of high integrity and honor who must also endorse at least one of the candidate’s passport-size photographs on the reverse side and also satisfy himself/herself that examination Description and grades indicated on the form by the applicant are genuine.</li>
        <li>The application will not be valid if the declaration below is not signed.</li>
        <li>If the declaration proves to be false, the application shall be rejected; if the falsity is detected after admission, the student shall be dismissed..</li>
    </ul>
    <p>
        I hereby declare that the photograph endorsed by me is the true likeness of the applicant <b><?=$sal?> <?=$_pd->user->first_name." ".$_pd->user->middle_name." ".$_pd->user->last_name?></b>  who is personally known to me. I have inspected his/her certificates and EXAMINATION DESCRIPTION against the result indicated on the form and I am satisfied that they are genuine and the same that appears on them is EXACTLY the same as that by which he/she is officially/personally known to me.
    </p>

    <p>SIGNATURE………………………………………………………………………………………………………………………</p>
    <p>DATE …………………………………………………………………………………………………………………………………</p>
    <p>NAME (BLOCK LETTERS) ……………………………………………………………………………………………………</p>
    <p>POSITION……………………………………………………………………………………………………………………………</p>
    <p>ADDRESS AND OFFICIAL STAMP…………………………………………………………………………………………</p>


    <div class="footer">
        <div class="pull-right">
            <strong>Copyright © </strong>  2018 Entrance University College of Health Science . 
        </div>
        <div>
            <small>All rights reserved. Powered by <a target="_blank" href="https://www.zentechgh.com/"> Zentech GH <?=date('Y');?></a></small>
        </div>
    </div>

</body>

</html>