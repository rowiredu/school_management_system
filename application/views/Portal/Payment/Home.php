    <style>
        ul .nav-item .active, .tab-content .active{
            background-color:#1967a93b;
        }
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #495057;
            background-color:#1967a93b;
            border-color: #fff0 #fff0 #fff0;
        }

    </style>
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i>  Fees </h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>

        <?php 
            $appropirate_payment_fld[0] = $appropirate_payment_fld[12];
            unset($appropirate_payment_fld[12]);
            sort($appropirate_payment_fld);
        ?>
        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title"> Admission Fees </h4>
                                <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                
                                <p class="text-muted m-b-30 font-14">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#record_applicant"> Record applicant</button>
                                </p>

                                <!-- sample modal content -->
                                <div id="record_applicant" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel"> Aquiring Forms </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>

                                            <?php if($within_admission): ?>


                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#gh_stu" role="tab">Ghanaian Student</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#fr_stu" role="tab">Foreigh Student</a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active p-3" id="gh_stu" role="tabpanel">
                                                        <?php echo form_open("portal/payment/records");?>
                                                            <div class="modal-body">
                                                                <h2> Making Payment of <?=$within_admission->gh_currency?>  <?=$within_admission->gh_amount?> </h2>
                                                                <div class="form-group">
                                                                    <label class="control-label"> Admission Year *</label>
                                                                    <select class="form-control select2" required>
                                                                        <optgroup label="Description Year">
                                                                            <?php foreach($ad_year as $year_des){ ?>
                                                                                <option value="<?=$year_des->id?> "> <?=$year_des->year?> </option>
                                                                            <?php } ?>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                                <?php if($this->ion_auth->is_banker()){ ?>
                                                                    <div class="form-group">
                                                                        <label>Reference code</label>
                                                                        <input type="text" name="bank_ref_code" value=""  class="form-control" required/>
                                                                    </div>
                                                                <?php } ?>
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="text" name="first_name" value=""  class="form-control" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Last Name  *</label>
                                                                    <input type="text" name="last_name" value=""  class="form-control" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>E-mail *</label>
                                                                    <input type="text" name="email" value=""  class="form-control" required/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Phone *eg 233209137654</label>
                                                                    <input type="text" name="phone" value="233"  class="form-control" required/>
                                                                </div>

                                                                <?=form_hidden('reference', 'ghanaian_student')?>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                                <?php echo form_submit('submit', 'record' , 'class="btn btn-primary"');?>
                                                            </div>
                                                        <?php echo form_close();?>
                                                    </div>
                                                    <div class="tab-pane p-3" id="fr_stu" role="tabpanel">
                                                        <?php echo form_open("portal/payment/records");?>
                                                            <div class="modal-body">
                                                                <h2> Making Payment of <?=$within_admission->fr_currency?>  <?=$within_admission->fr_amount?> </h2>
                                                                <div class="form-group">
                                                                    <label class="control-label"> Admission Year *</label>
                                                                    <select class="form-control select2" required>
                                                                        <optgroup label="Description Year">
                                                                            <?php foreach($ad_year as $year_des){ ?>
                                                                                <option value="<?=$year_des->id?> "> <?=$year_des->year?> </option>
                                                                            <?php } ?>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                                <?php if($this->ion_auth->is_banker()){ ?>
                                                                    <div class="form-group">
                                                                        <label>Reference code</label>
                                                                        <input type="text" name="bank_ref_code" value=""  class="form-control" required/>
                                                                    </div>
                                                                <?php } ?>
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="text" name="first_name" value=""  class="form-control" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Last Name  *</label>
                                                                    <input type="text" name="last_name" value=""  class="form-control" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>E-mail *</label>
                                                                    <input type="text" name="email" value=""  class="form-control" required/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Phone *</label>
                                                                    <input type="text" name="phone" placeholder="Country Code before phone number 44214578492"  class="form-control" required/>
                                                                </div>
                                                                <?=form_hidden('reference', 'foreign_student')?>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                                <?php echo form_submit('submit', 'record' , 'class="btn btn-primary"');?>
                                                            </div>
                                                        <?php echo form_close();?>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <span class="text-muted m-b-30 font-14" style="text-align:center; padding-top:34px;">
                                                    Sorry, Admission is not in session
                                                </span>
                                            <?php endif ?>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                                <table id="datatables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <?php foreach ($appropirate_payment_fld as $field) { ?>
                                                <th><?=humanize($field)?></th>
                                            <?php } ?>
                                            <th><?='Operations'?></th>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                        <?php foreach ($data as $user):?>
                                            <tr>
                                                <?php foreach ($appropirate_payment_fld as $field) { ?>
                                                    <td><?php echo humanize(htmlspecialchars($user->$field,ENT_QUOTES,'UTF-8'));?></td>
                                                <?php } ?>
                                                <!-- sample modal content -->
                                                <td>
                                                    <i class="fa fa-edit waves-effect waves-light" data-toggle="modal" data-target="#<?='edit'.$user->id?>">  </i>
                                                    <i class="fa fa-print waves-effect waves-light" data-toggle="modal" data-target="#<?='receipt'.$user->id?>">  </i>
                                                    <i id="<?='resend'.$user->id?>" class="fa fa-comment waves-effect waves-light"  onclick="resend(<?=$user->id?>)"> </i>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                                <!-- print modal -->
                                <?php foreach ($data as $user):?>
                                    <!-- sample modal content -->
                                    <div id="<?='receipt'.$user->id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myModalLabel"> 
                                                        Recept 
                                                        <i class="fa fa-print" onclick="printElem('<?='receipt'.$user->id?>')"> </i>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                
                                                <div id="invoice-POS">
                                                    
                                                    <center id="top">
                                                        <div clas8s="logo"></div>
                                                        <div class="info"> 
                                                            <img src="<?=base_url('assets/logo.png')?>" style="width:90%; margin:auto;"/>
                                                        </div><!--End Info-->
                                                    </center><!--End InvoiceTop-->
                                                    <style>
                                                        #pos>td{
                                                            width:90%; 
                                                            margin:auto;
                                                        }
                                                    </style>
                                                    <div id="mid">
                                                        <div class="info">
                                                            <center> 
						                                        <table style="width:100%;">
                                                                    <?php foreach ($appropirate_payment_fld as $field) { ?>
                                                                        <tr>
                                                                            <td class="col-3"> 
                                                                                <?=humanize($field)?><br/>
                                                                            </td>
                                                                            <td class="col-1"></td>
                                                                            <td class="col-8">
                                                                                <?=htmlspecialchars(humanize($user->$field),ENT_QUOTES,'UTF-8');?><br/>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>

                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>

                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>

                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>

                                                                    <tr >
                                                                        <td class="col-4"><h4><?=humanize("Serial")?><br/></h4></td>
                                                                        <td class="col-4"><h4><?=humanize("Pin ")?></h4></td>
                                                                        <td class="col-4"><h4>Sub Total</h4></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="col-4"> 
                                                                            <?=htmlspecialchars($user->serial_number,ENT_QUOTES,'UTF-8');?><br/>
                                                                        </td>
                                                                        <td class="col-4">
                                                                            <?=htmlspecialchars($user->password,ENT_QUOTES,'UTF-8');?><br/>
                                                                        </td>
                                                                        <td class="col-4">

                                                                            <?=$user->reference == "foreign_student" ? $within_admission->fr_currency : $within_admission->gh_currency?>  
                                                                            <?=$user->reference == "foreign_student" ? $within_admission->fr_amount : $within_admission->gh_amount?>.00  
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td class="item"><h2></h2></td>
                                                                        <td class="Hours"><h2></h2></td>
                                                                        <td class="Rate"><h2></h2></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="col-4"></td>
                                                                        <td class="col-4">tax</td>
                                                                        <td class="col-4"><?=$user->reference == "foreign_student" ? $within_admission->fr_currency : $within_admission->gh_currency?>  0.00</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="col-4"></td>
                                                                        <td class="col-4">Total</td>
                                                                        <td class="col-4">
                                                                            <?=$user->reference == "foreign_student" ? $within_admission->fr_currency : $within_admission->gh_currency?>  
                                                                            <?=$user->reference == "foreign_student" ? $within_admission->fr_amount : $within_admission->gh_amount?>.00  
                                                                    </tr>
                                                                </table>

                                                            </center>
                                                        </div>
                                                    </div><!--End Invoice Mid-->

                                                    <div id="legalcopy" style="background:#8c8c8c !important; padding:20px; text-align:center;">
                                                        <p class="legal"><strong>Thank you for your business!</strong>  Payment is only for this academic session. 
                                                        </p>
                                                    </div>
                                                </div><!--End Invoice-->
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                    <div id="<?='edit'.$user->id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myModalLabel"> 
                                                        Edit record for <?=$user->first_name." ".$user->last_name?>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                
                                                <?php echo form_open("portal/payment/records/".$user->id);?>
                                                    <div class="modal-body">
                                                        <?php if($this->ion_auth->is_banker()){ ?>
                                                            <div class="form-group">
                                                                <label>Reference code</label>
                                                                <input type="text" name="bank_ref_code" value="<?=$user->bank_ref_code?>"  class="form-control" required/>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="form-group">
                                                            <label>E-mail *</label>
                                                            <input type="text" name="email" value="<?=$user->email?>"  class="form-control" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Phone *eg 233209137654</label>
                                                            <input type="text" name="phone" value="<?=$user->phone?>"  class="form-control" required/>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        <?php echo form_submit('submit', 'Save and resend info' , 'class="btn btn-primary"');?>
                                                    </div>
                                                <?php echo form_close();?>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                <?php endforeach;?><!-- /.print modal -->


                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
