
            <style>
                .modal-dialog {
                    max-width: 75%;
                    margin: 1.75rem auto;
                }
                .center{
                    margin-left:42%; 
                    margin-right:42%;
                }
                .enroll-container {
                    margin-top: 70px;
                }

                blockquote.blockquote-blue, blockquote.blockquote-info{
                    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                    line-height: 1.42857143;
                    font-family: 'Open Sans', sans-serif !important;
                    box-sizing: border-box;
                    font-size: 15px;
                    margin: 0 0 17px;
                    background: #c5e8f7;
                    border: 1px solid #b6d6e4;
                    border-left-width: 5px;
                    padding: 15px;
                    border-radius: 3px;
                    background-clip: padding-box;
                    color: #0c3c50;
                }
            </style>
            <?php
                if(empty($id)) {
                    $id                 = '';
                    $first_name         = '';
                    $last_name          = '';
                    $student_code       = '';
                    $birthday           = '';
                    $sex                = '';
                    $address            = '';
                    $phone              = '';
                    $email              = '';
                    $password           = '';
                }
            ?>
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i>  Enroll Student</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>

        <?php //var_dump($password);?>
        

        <?php echo form_open(site_url('portal/approval_list/approve/'. $id) , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
        <div class="wrapper enroll-container">
            <div class="container-fluid">
            <!--<center><h4><?php echo get_phrase('Enroll_form');?></h4></center>-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-primary" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title" >
                                    <i class="entypo-plus-circled"></i>
                                    
                                </div>
                            </div>
                            <div class="panel-body">
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-12 control-label"><?php echo get_phrase('first_name');?></label>

                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="first_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?=$first_name?>" required>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('Level');?></label>

                                        <div class="col-sm-12">
                                            <select required name="class_id" class="form-control" data-validate="required" id="class_id"
                                                data-message-required="<?php echo get_phrase('value_required');?>"
                                                onchange="return get_student_code(this.value, 'level')" >
                                            <option value=""><?php echo get_phrase('select');?></option>
                                            <?php
                                                $classes = $this->db->get('class')->result_array();
                                                foreach($classes as $row):
                                                    ?>
                                                    <option value="<?php echo $row['class_id'];?>">
                                                            <?php echo $row['name'];?>
                                                            </option>
                                                <?php
                                                endforeach;
                                            ?>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-3" class="col-sm-3 control-label"><?php echo get_phrase('department');?></label>

                                        <div class="col-sm-12">
                                            <select required name="section_id" class="form-control" data-validate="required" id="department_id"
                                                data-message-required="<?php echo get_phrase('value_required');?>"
                                                    onchange="return get_student_code(this.value, 'department')">
                                            <option value=""><?php echo get_phrase('select department');?></option>
                                            <?php
                                                $classes = $this->db->get('department')->result_array();
                                                foreach($classes as $row):
                                                    ?>
                                                    <option value="<?php echo $row['department_id'];?>">
                                                            <?php echo $row['name'];?>
                                                            </option>
                                                <?php
                                                endforeach;
                                            ?>
                                        </select>
                                        </div>
                                    </div>

                                    <div id="student_code">
                                        <div class="form-group">
                                            <label for="field-4" class="col-sm-3 control-label"><?php echo get_phrase('id_no');?></label>

                                            <div class="col-sm-12">
                                                <!--<input type="text" class="form-control" name="student_code" value="<?=$student_code?>" data-validate="required" id="class_id"
                                                    data-message-required="<?php echo get_phrase('value_required');?>">-->
                                                    <input type="text" class="form-control" name="student_code" value="" data-validate="required" id="student_code"
                                                    placeholder="Select department first" data-message-required="<?php echo get_phrase('value_required');?>" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>

                                        <div class="col-sm-12">
                                            <input type="text" class="form-control datepicker" name="birthday" value="<?=$birthday?>" data-start-view="2">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('gender');?></label>

                                        <div class="col-sm-12">
                                            <select name="sex" class="form-control selectboxit">
                                            <option value=""><?php echo get_phrase('select');?></option>
                                            <option <?=$sex == "male" ? 'selected': ''?>><?php echo get_phrase('male');?></option>
                                            <option <?=$sex == "female" ? 'selected': ''?>><?php echo get_phrase('female');?></option>
                                        </select>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-info"><?php echo get_phrase('add_student');?></button>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-12 control-label"><?php echo get_phrase('last_name');?></label>

                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="last_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?=$last_name?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="address" value="<?=$address?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>

                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="phone" value="<?=$phone?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="email" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?=$email?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('password');?></label>

                            <div class="col-sm-12">
                                <input type="password" class="form-control" name="password" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?=$password?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-2" class="col-sm-12 control-label"><?php echo get_phrase('Hall of Residence');?></label>

                            <div class="col-sm-12">
                                <select name="dormitory_id" class="form-control selectboxit">
                                <option value=""><?php echo get_phrase('select');?></option>
                                    <?php
                                        $dormitories = $this->db->get('dormitory')->result_array();
                                        foreach($dormitories as $row):
                                    ?>
                                        <option value="<?php echo $row['dormitory_id'];?>"><?php echo $row['name'];?></option>
                                    <?php endforeach;?>
                            </select>
                            </div>
                        </div>

                    </div>
                    
                    <div class="col-md-2">
                        <blockquote class="blockquote-blue">
                            <p>
                                <strong>Student Admission Notes</strong>
                            </p>
                            <p>
                                Admitting new students will automatically create an enrollment to the selected level in the running session.
                                Please check and recheck the informations you have inserted because once you admit new student, you won't be able
                                to edit his/her level, department without promoting to the next session.
                            </p>
                        </blockquote>
                    </div>
                </div>

            </div> <!-- end container -->
        </div>
        <?php echo form_close();?>
        <!-- end wrapper -->

        <script type="text/javascript">

            function get_student_code(selector_id, selector) {
                var year = "<?php echo $running_year;?>";
                switch(selector) {
                    case ('department'):
                        department_id = selector_id
                        class_id      = $("#class_id").val()
                    break
                    case ('level'):
                        department_id = $("#department_id").val()
                        class_id      = selector_id
                    break
                    default:
                        return
                }
                
                if(department_id != '' && class_id != '' && year != '') {
                    $.ajax({
                        url: '<?php echo site_url('portal/approval_list/student_code/'); ?>'+ department_id + '/' + class_id + '/' + year,
                        success: function (response)
                        {
                            jQuery('#student_code').html(response);
                        }
                    });
                }
            }

        </script>
