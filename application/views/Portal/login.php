<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Entrance University College of Health Science</title>
        <meta content="Entrance Dashboard" name="description" />
        <meta content="Entrance" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/logo_part.png">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

        <!-- App css -->
        <link href="<?=base_url()?>assets/portal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/portal/assets/css/style.css" rel="stylesheet" type="text/css" />
        <style>
            .wrapper-page {
                margin: 0% auto;
                padding: 10% 34% 15% 34%;
                max-width: 2000px;
                position: relative;
            }
        </style>
    </head>


    <body>

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div class="accountbg" style="background-size: 100%;"></div>
        <div class="wrapper-page" style="background-color: hsla(0, 0%, 0%, 0.75);">

            <div class="card" style="background-color: hsla(0, 0%, 100%, 0.75);">
                <div class="card-body">

                    <h3 class="text-center m-0" style="font-family: 'Titillium Web', sans-serif; font-size:30px; width:100%; color: rgb(67, 63, 153);">
                        <img src="<?=base_url('assets/logo.png')?>" style="width:100%;"/>
                        <!--Entrance University College of Health Science-->
                    </h3>

                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                        <p class="text-muted text-center">Sign in to continue.</p>

                        <?php echo form_open("portal/auth/login");?>


                            <div class="form-group">
                                <label for="identity"> <?php echo _l('E-mail');?> </label>
                                <?php echo form_input('identity' , '' , 'class="form-control"');?>
                            </div>

                            <div class="form-group">
                                <label for="pin"><?php echo lang('login_password_label', 'password');?></label>
                                <?php echo form_input($password , '' , 'class="form-control" ');?>
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <div class="custom-control custom-checkbox">
                                        <?php echo form_checkbox('remember', '1', FALSE, 'class="custom-control-input" id="customControlInline"');?>
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <?php echo form_submit('submit', lang('login_submit_btn') , array('class' => 'btn btn-primary '));?>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p><a href="<?=base_url()?>"><?php echo _l('Go back');?></a></p>
                <p><a href="#!"><?php echo lang('login_forgot_password');?></a></p>
            </div>

        </div>


        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/popper.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/modernizr.min.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/waves.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>assets/portal/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/portal/assets/js/app.js"></script>

    </body>

</html>