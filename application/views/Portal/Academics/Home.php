            <style>
                .select2-container--default .select2-selection--single {
                    background-color: #fff;
                    border: 1px solid #aaa;
                    border-radius: 0px 0px 0px 20px;
                }
                .select2-container .select2-selection--single {
                    box-sizing: border-box;
                    cursor: pointer;
                    display: block;
                    height: 36px;
                    user-select: none;
                    -webkit-user-select: none;
                }
            </style>
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i>  Acadamic Year </h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">  </h4>
                                <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                <?=validation_errors('<span class="error">', '</span>'); ?>
                                
                                <p class="text-muted m-b-30 font-14">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#record_applicant"> 
                                        create admission year
                                    </button>
                                </p>

  

                                <!-- sample modal content -->
                                <div id="record_applicant" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel"> Admission </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <?php if(!$within_admission): ?>
                                                <?php echo form_open("portal/Academics/records");?>
                                                    <div class="modal-body">
                                                        <div class="form-group"> 
                                                            <?php $year = date('Y/')?>
                                                            <?php $year.= date('Y',strtotime(date("Y", time()) . " + 365 day"));?>
                                                            <label> Year Description</label>
                                                            <input type="text" name="year" value="<?=$year?>"  class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Admission Date Range</label>
                                                            <div>
                                                                <div class="input-daterange input-group" id="date-range">
                                                                    <input type="text" class="form-control" name="start" />
                                                                    <input type="text" class="form-control" name="end" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <b class="control-label"> Ghanaian Student Payment</b>
                                                        </div>
                                                        <div class="form-group col-3" style="float:left; padding-right: 0px; padding-left: 0px;">
                                                            <label class="control-label"> Currency *</label>
                                                            <select class="form-control select2" name="gh_currency" required>
                                                                <optgroup label="Currency">
                                                                    <option value="GHC"> GHC </option>
                                                                    <option value="USD"> USD </option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-9" style="float:left; padding-right: 0px; padding-left: 0px;">
                                                            <label>Amount</label>
                                                            <input type="number" name="gh_amount" value=""  class="form-control" style=" border-radius: 0px 20px 0px 0px;"/>
                                                        </div>


                                                        <div class="form-group">
                                                            <b class="control-label"> Foreign Student Payment</b>
                                                        </div>
                                                        <div class="form-group col-3" style="float:left; padding-right: 0px; padding-left: 0px;">
                                                            <label class="control-label"> Currency *</label>
                                                            <select class="form-control select2" name="fr_currency" required>
                                                                <optgroup label="Currency">
                                                                    <option value="USD"> USD </option>
                                                                    <option value="GHC"> GHC </option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-9" style="float:left; padding-right: 0px; padding-left: 0px;">
                                                            <label>Amount</label>
                                                            <input type="number" name="fr_amount" value=""  class="form-control" style=" border-radius: 0px 20px 0px 0px;"/>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        <?php echo form_submit('submit', 'record' , 'class="btn btn-primary"');?>
                                                    </div>
                                                <?php echo form_close();?>
                                            <?php else: ?>
                                                <span class="text-muted m-b-30 font-14" style="text-align:center; padding-top:34px;">Sorry, Admission in session or a future date hasnt started yet...</span>
                                            <?php endif ?>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                                <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <?php foreach ($appropirate_academics_fld as $field) { ?>
                                                <th><?=humanize($field)?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                        <?php foreach ($data as $user):?>
                                            <tr>
                                                <?php foreach ($appropirate_academics_fld as $field) { ?>
                                                    <td><?php echo htmlspecialchars($user->$field,ENT_QUOTES,'UTF-8');?></td>
                                                <?php } ?>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
