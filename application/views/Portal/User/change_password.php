            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i> <?php echo lang('change_password_heading');?></h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                              <div class="card-body">
                                    <h4 class="mt-0 header-title"><?php echo lang('edit_user_heading');?></h4>
                                    <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                    <p><?php echo lang('edit_user_subheading');?></p>
                                    <?php echo form_open(uri_string());?>

                                        <div id="infoMessage"><?php echo $message;?></div>

                                          <div class="form-group">
                                                <label><?php echo lang('change_password_old_password_label', 'old_password');?></label>
                                                <?php echo form_input($old_password ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                                                <?php echo form_input($new_password ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label> <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> </label>
                                                <?php echo form_input($new_password_confirm ,'','class="form-control"');?>
                                          </div>
                                            <?php echo form_input($user_id);?>
                                          
                                          <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

                                    <?php echo form_close();?>
                              </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
