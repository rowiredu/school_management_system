

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i> Users</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">System Users</h4>
                                <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                
                                <p class="text-muted m-b-30 font-14">

                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#create_user"> Create Users</button>
                                </p>

  
                                <!-- sample modal content -->
                                <div id="create_user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="myModalLabel"> Create User </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <?=form_open("portal/user/create_user");?>
                                                <div class="modal-body">
                                                    <div id="infoMessage"><?php echo $message;?></div>

                                                    <div class="form-group">
                                                        <label><?php echo lang('create_user_fname_label', 'first_name');?></label>
                                                        <?php echo form_input($first_name ,'','class="form-control"');?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> <?php echo lang('create_user_lname_label', 'last_name');?>  </label>
                                                        <?php echo form_input($last_name ,'','class="form-control"');?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?php
                                                        if($identity_column!=='email') {
                                                            echo '<p>';
                                                            echo lang('create_user_identity_label', 'identity');
                                                            echo '<br />';
                                                            echo form_error('identity');
                                                            echo form_input($identity);
                                                            echo '</p>';
                                                        }
                                                        ?>

                                                        <?php if ($this->ion_auth->is_admin()): ?>
                                                        <label> <?php echo lang('edit_user_groups_heading');?>  </label>
                                                        <?php foreach ($groups as $group):?>
                                                            <p class="checkbox">
                                                            <?php
                                                                $gID=$group['id'];
                                                                $checked = null;
                                                                $item = null;
                                                                foreach($currentGroups as $grp) {
                                                                    if ($gID == $grp->id) {
                                                                        $checked;
                                                                    break;
                                                                    }
                                                                }
                                                            ?>
                                                            <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                                                            <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                                                            </p>
                                                        <?php endforeach?>

                                                        <?php endif ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> <?php echo lang('create_user_email_label', 'email');?>  </label>
                                                        <?php echo form_input($email ,'','class="form-control"');?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> <?php echo lang('create_user_phone_label', 'phone');?>  </label>
                                                        <?php echo form_input($phone ,'','class="form-control"');?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> <?php echo lang('create_user_password_label', 'password');?>  </label>
                                                        <?php echo form_input($password ,'','class="form-control"');?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> <?php echo lang('create_user_password_confirm_label', 'password_confirm');?>  </label>
                                                        <?php echo form_input($password_confirm ,'','class="form-control"');?>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <?php echo form_submit('submit', lang('create_user_submit_btn') , 'class="btn btn-primary"');?>
                                                </div>
                                            <?php echo form_close();?>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang('index_fname_th');?></th>
                                            <th><?php echo lang('index_lname_th');?></th>
                                            <th><?php echo lang('index_email_th');?></th>
                                            <th><?php echo lang('index_groups_th');?></th>
                                            <th><?php echo lang('index_status_th');?></th>
                                            <th><?php echo lang('index_action_th');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                        <?php foreach ($users as $user):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                                                <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                                <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                                                <td>
                                                    <?php foreach ($user->groups as $group):?>
                                                        <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?> - 
                                                    <?php endforeach?>
                                                </td>
                                                <td><?php echo ($user->active) ? anchor("portal/user/deactivate/".$user->id, lang('index_active_link')) : anchor("portal/user/activate/". $user->id, lang('index_inactive_link'));?></td>
                                                <td><?php echo anchor("portal/user/edit_user/".$user->id, 'Edit') ;?></td>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
