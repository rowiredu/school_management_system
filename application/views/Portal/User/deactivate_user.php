


            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i> Users</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                              <div class="card-body">
                                    <h4 class="mt-0 header-title"><?php echo lang('deactivate_heading');?></h4>
                                    <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                    <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
                                    <?php echo form_open("portal/auth/deactivate/".$user->id);?>
                                      <div class="form-group">
                                            <label> <?php echo lang('deactivate_confirm_y_label', 'confirm');?>  </label>
                                            <input type="radio" name="confirm" value="yes" checked="checked" />
                                            <label> <?php echo lang('deactivate_confirm_n_label', 'confirm');?>  </label>
                                            <input type="radio" name="confirm" value="no" />
                                      </div>
                                      <?php echo form_hidden($csrf); ?>
                                      <?php echo form_hidden(array('id'=>$user->id)); ?>
                                      <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>

                                    <?php echo form_close();?>
                              </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
