

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <form class="float-right app-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                            <h4 class="page-title"> <i class="dripicons-blog"></i> Users</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                              <div class="card-body">
                                    <h4 class="mt-0 header-title"><?php echo lang('edit_user_heading');?></h4>
                                    <div id="infoMessage" style='color:red;'><?php echo $message;?></div>
                                    <p><?php echo lang('edit_user_subheading');?></p>
                                    <?php echo form_open(uri_string());?>
                                          <div class="form-group">
                                                <label><?php echo lang('edit_user_fname_label', 'first_name');?></label>
                                                <?php echo form_input($first_name ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label> <?php echo lang('edit_user_lname_label', 'last_name');?>  </label>
                                                <?php echo form_input($last_name ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label> <?php echo lang('edit_user_phone_label', 'phone');?>  </label>
                                                <?php echo form_input($phone ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label> <?php echo lang('edit_user_password_label', 'password');?>  </label>
                                                <?php echo form_input($password ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <label> <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>  </label>
                                                <?php echo form_input($password_confirm ,'','class="form-control"');?>
                                          </div>
                                          <div class="form-group">
                                                <?php if ($this->ion_auth->is_admin()): ?>
                                                      <label><?php echo lang('edit_user_groups_heading');?></label>
                                                      <?php foreach ($groups as $group):?>
                                                      <label class="checkbox">
                                                            <?php
                                                                  $gID=$group['id'];
                                                                  $checked = null;
                                                                  $item = null;
                                                                  foreach($currentGroups as $grp) {
                                                                        if ($gID == $grp->id) {
                                                                        $checked= ' checked="checked"';
                                                                        break;
                                                                        }
                                                                  }
                                                            ?>
                                                            <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                                                            <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                                                      </label>
                                                      <?php endforeach?>
                                                <?php endif ?>
                                          </div>
                                          <?php echo form_hidden('id', $user->id);?>
                                          <?php echo form_hidden($csrf); ?>

                                          <p><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></p>

                                    <?php echo form_close();?>
                              </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
