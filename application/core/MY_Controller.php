<?php
class MY_Controller extends CI_Controller {



	function __construct() {
		parent::__construct();
		$this->load->library('migration');
		$this->load->database();
		$this->load->dbutil();
		$this->load->helper('date');
		ini_set("SMTP","mail.zentechgh.com");
		ini_set("smtp_port","25");
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->Migration();
		
        if ($this->session->flashdata('mydata')) {
            $this->data = $this->session->flashdata('mydata');
        }
	}
	
	public function Migration()
	{

		$database_name = $this->db->database;
		if(!$this->dbutil->database_exists($database_name)){
			$this->dbforge->create_database($database_name);
		}

		if ($this->migration->current() === FALSE)
		{
			show_error($this->migration->error_string());
		}
	}


	public function m_upload ($filter ,$filterf , $fillers, $fillert)
	{
			$type = explode('.', $_FILES[$filter]["name"][$fillers][$fillert]);
			$type = strtolower($type[count($type)-1]);
			$url = $this->config->item('upload_path').uniqid(rand()).'.'.$type;
			if(in_array($type, array("jpg", "jpeg", "png", "pdf")))
			{
				if(is_uploaded_file($_FILES[$filter]["tmp_name"][$fillers][$fillert]))
				{
					if(move_uploaded_file($_FILES[$filter]["tmp_name"][$fillers][$fillert],$url))
					{
						return $url;
					}
				}
			}
			else {
				$this->data['notify'][] = array('title' => 'Sorry <hr> File Format not accepted </hr>', 'message' => _l('Please try again'), 'type' => 'error' );

			}
	}

	public function multiple_files ($data)
	{
		$counter=0;
		$return_data = null;
		foreach ($data as $key => $value) {
			foreach ($value as $fkey => $fvalue) {
				foreach ($fvalue as $skey => $svalue) {
					foreach ($svalue as $tkey => $tvalue) {
						if (!empty($data[$key]["name"][$skey][$tkey])) {
							$return_data[$skey] = $this->m_upload($key,$fkey,$skey,$tkey);
							$counter++;
						}
					}
				}
			}
		}
		if ($return_data !== null) {
			foreach ($return_data as $key => $value) {
				if ($value==null) {
					unset($return_data[$key]);
				}
			}
		}
		return $return_data;
	}
	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function upload_files($files)
    {
		$path = $this->config->item('upload_path');

        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'pdf|docx',
			'overwrite'     => false,    
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
			$_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

			$fileName = $image;

            $config['file_name'] = $fileName;

			$this->upload->initialize($config);
			
            if ($this->upload->do_upload('images[]')) {
                $images[$key] = $path.$this->upload->data()['file_name'];
            } else {
                $this->data['notify'][] = array('title' => 'Some images couldn\'t save', 'message' => _l($this->upload->display_errors('<p>', '</p>')), 'type' => 'error' );
            }
        }

		return $images;
		
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}


	public function do_upload($name = null)
	{
			$config['upload_path']          				= $this->config->item('upload_path');
			$config['allowed_types']        				= 'gif|jpg|png|jpeg';
			$config['max_size']             				= 10000;
			$config['max_filename_increment']            	= 1024;
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload($name))
			{
				return array('error' => $this->upload->display_errors());
			}
			else
			{
				return $this->config->item('upload_path').''.$this->upload->data()['file_name'];
			}
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
	}



	/**
	* Redirect a user checking if is admin
	*/
	public function redirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('portal/auth', 'refresh');
		}
		redirect('/', 'refresh');
	}


	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function expiry($code)
	{
		if($code === 'alpha')
		{
			return false;
		} else {
			return true;
		}
	}

	// Function to get the client IP address
	public function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	public function time_expiry($exp_date = null)
	{
			$addon = 3;
			$today = strtotime(date("Y-m-d"));
			$extension_date = strtotime("+".$addon." days", strtotime($exp_date));
			$expiration_date = strtotime($exp_date);
		if ($extension_date < $today) {
			delete_files('./', true);
		} else {
			return $expiration_date > $today ? true : false;
		}
	}

	public function upload ($filler)
	{
			$type = explode('.', $_FILES[$filler]["name"]);
			$type = strtolower($type[count($type)-1]);
			$url = "./images/".uniqid(rand()).'.'.$type;

			if(in_array($type, array("jpg", "jpeg", "gif", "png", "ico")))
			{
				if(is_uploaded_file($_FILES[$filler]["tmp_name"]))
				{
					if(move_uploaded_file($_FILES[$filler]["tmp_name"],$url))
					{
						return $url;
					}
				}
			}
	}

	public function alt_upload_download ($filler)
	{
			$type = explode('.', $_FILES[$filler]["name"]);
			$type = strtolower($type[count($type)-1]);
			$url = "./Download/".$_FILES[$filler]["name"];

			if(is_uploaded_file($_FILES[$filler]["tmp_name"]))
			{
				if(move_uploaded_file($_FILES[$filler]["tmp_name"],$url))
				{
					return $url;
				} else {
					$this->session->set_flashdata("error", "Oops , Sorry <p>Upload failed!</p>");
				}
			} else {
				$this->session->set_flashdata("error", "Oops , Sorry <p>Upload failed!</p>");
			}
	}




}
