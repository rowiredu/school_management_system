<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Students_m extends MY_Model
{

	public $rules = array(
		'first_name' => array(
			'field' => 'first_name', 
			'label' => 'First_name', 
			'rules' => 'trim|required'
		), 
		'last_name' => array(
			'field' => 'last_name', 
			'label' => 'last_name', 
			'rules' => 'trim|required'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'email', 
			'rules' => 'trim|required'
		), 
		'phone' => array(
			'field' => 'phone', 
			'label' => 'Phone', 
			'rules' => 'trim|required'
		)
    );
    
	public function __construct()
	{
        $this->table = "user_applicant";
		$this->primary_key = 'id';
		$this->_order_by = 'created desc';
		parent::__construct();
	}
	
	
	

}