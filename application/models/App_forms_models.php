<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class App_forms_models extends MY_Model
{

	public $rules = array(
		'first_name' => array(
			'field' => 'first_name', 
			'label' => 'First_name', 
			'rules' => 'trim|required'
		), 
		'last_name' => array(
			'field' => 'last_name', 
			'label' => 'last_name', 
			'rules' => 'trim|required'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'email', 
			'rules' => 'trim|required'
		), 
		'phone' => array(
			'field' => 'phone', 
			'label' => 'Phone', 
			'rules' => 'trim|required'
		)
    );
    
	public function __construct()
	{
        $this->table = 'app_forms';
        $this->primary_key = 'id';
		parent::__construct();
	}
	
	
	

}