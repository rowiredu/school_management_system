<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Academics_model extends MY_Model
{

	public $rules = array(
		'start' => array(
			'field' => 'start', 
			'label' => 'Admission date range', 
			'rules' => 'trim|required'
		), 
		'end' => array(
			'field' => 'end', 
			'label' => 'End date', 
			'rules' => 'trim|required'
		), 
		'currency' => array(
			'field' => 'currency', 
			'label' => 'Currency', 
			'rules' => 'trim|required'
		), 
		'year' => array(
			'field' => 'year', 
			'label' => 'Year', 
			'rules' => 'trim|required|is_unique[academic_year_with_fee.year]'
		), 
		'amount' => array(
			'field' => 'amount', 
			'label' => 'Amount', 
			'rules' => 'trim|required'
		)
    );
	protected $_order_by = 'id desc';
    
	public function __construct()
	{
        $this->table = 'academic_year_with_fee';
        $this->primary_key = 'id';
		parent::__construct();
	}
	
	
	

}