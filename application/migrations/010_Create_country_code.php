<?php
class Migration_Create_country_code extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'iso' => array(
				'type' => 'VARCHAR',
				'constraint' => 2,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 80,
			),
			'nicename' => array(
				'type' => 'VARCHAR',
				'constraint' => 80,
				'default' => null,
			),
			'numcode' => array(
				'type' => 'INT',
				'constraint' => 8,
				'default' => null,
			),
			'phonecode' => array(
				'type' => 'VARCHAR',
				'constraint' => 5,
				'default' => null,
			),
			'iso3' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('country');
	}

	public function down()
	{
		$this->dbforge->drop_table('country');
	}
}

