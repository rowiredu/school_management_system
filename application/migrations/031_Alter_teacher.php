<?php
class Migration_Alter_teacher extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
        $this->dbforge=$this->load->dbforge($this->db, TRUE);
        
        /*
        $this->dbforge->drop_column('teacher', 'first_name');
		$this->dbforge->drop_column('teacher', 'last_name');
		*/
        
		
		$fields = array(
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
            ),
            'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
		);
		$this->dbforge->add_column('teacher', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge = $this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('teacher', 'first_name');
        $this->dbforge->drop_column('teacher', 'last_name');
        $this->dbforge->drop_column('teacher', 'name');
	}
}