<?php
class Migration_Alter_attendance extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$fields = array(
			'subject_id' => array(
				'type' => 'INT',
				'constraint' => '8',
			),
		);
		$this->dbforge->add_column('attendance', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
		$this->dbforge->drop_column('attendance', 'subject_id');
	}
}