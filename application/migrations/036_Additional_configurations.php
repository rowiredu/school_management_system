<?php
class Migration_Additional_configurations extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$data = array(
			array(
				'type' 			=> 'fail_mark',
				'description' 		=> '40',
            ),
        );

		$this->db->insert_batch( 'settings', $data);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->db->where('type', 'fail_mark')->delete('settings');
	}
}