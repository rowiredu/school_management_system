<?php
class Migration_Alter_class extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		

		$fields = array(
			'class_divisions_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
		);
		$this->dbforge->add_column('class', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_column('class', 'class_divisions_id');
		$this->dbforge->drop_table('department');
	}
}
