<?php
class Migration_Set_Super_root_user extends CI_Migration {

	public function up()
	{
		$groups = array(
			array(
		        'name' 			=> 'admin',
		        'description' 		=> 'Administrator',
			),
			array(
		        'name' 			=> 'cashier',
		        'description' 		=> 'Accountant',
			),
			array(
		        'name' 			=> 'teller',
		        'description' 		=> 'Banks',
			)
		);

		$users = array(
		        'ip_address' 			=> '127.0.0.1',
		        'username' 			=> 'administrator',
		        'password' 			=> '$2y$08$0sLliIfjrGtLp41tNkrHDuSakVD/3U7ZX.hoSN.0U2F.ICKYw35D6',
		        'salt' 				=> '',
		        'email' 			=> 'emmanuel@zentechgh.com',
		        'activation_code' 		=> '',
		        'forgotten_password_code'	=> '',
		        'created' 			=> '1268889823',
		        'last_login' 			=> '1268889823',
		        'active' 			=> '1',
		        'first_name' 			=> 'Emmanuel',
		        'last_name' 			=> 'Dadzie',
		        'phone' 			=> '0209137654',
		);

		$users_groups = array(
			array(
		        'user_id' 					=> 1,
		        'group_id' 					=> 1,
			),
			array(
		        'user_id' 					=> 1,
		        'group_id' 					=> 2,
			)
		);

		$this->db->insert_batch( 'groups', $groups);
		$this->db->insert( 'users', $users);
		$this->db->insert_batch( 'users_groups', $users_groups);
	}

	public function down()
	{
		$this->db->empty_table( 'groups' );
		$this->db->empty_table( 'users' );
		$this->db->empty_table( 'users_groups' );
	}
}


