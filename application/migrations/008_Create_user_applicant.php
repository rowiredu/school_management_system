<?php
class Migration_Create_user_applicant extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'email' => array(
				'type'       => 'VARCHAR',
				'constraint' => '254'
			),
			'first_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '50',
				'null'       => TRUE
			),
			'last_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '50',
				'null'       => TRUE
			),
			'date_of_birth' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'gender' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'active' => array(
				'type'       => 'TINYINT',
				'constraint' => '1',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'salt' => array(
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => TRUE
			),
			'picture' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'phone' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE,
			),
			'activation_code' => array(
				'type'       => 'VARCHAR',
				'constraint' => '40',
				'null'       => TRUE
			),
			'forgotten_password_code' => array(
				'type'       => 'VARCHAR',
				'constraint' => '40',
				'null'       => TRUE
			),
			'forgotten_password_time' => array(
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'ip_address' => array(
				'type'       => 'VARCHAR',
				'constraint' => '45'
			),
			'remember_code' => array(
				'type'       => 'VARCHAR',
				'constraint' => '40',
				'null'       => TRUE
			),
			'bio' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'json_encoded_prefield' => array(
				'type' 		=> 'TEXT',
				'null' 		=> TRUE,
			),
			'last_login' => array(
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'last_logout' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_on' => array(
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'modified' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('user_applicant');
	}

	public function down()
	{
		$this->dbforge->drop_table('user_applicant' , $fields );
	}
}

