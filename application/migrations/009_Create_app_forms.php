<?php
class Migration_Create_app_forms extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'applicant_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'year_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'payment_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'status' => array(
				'type' => 'TEXT',
			),
			'json_values' => array(
				'type' => 'TEXT',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'modified' => array(
				'type' => 'DATETIME',
			),
			'active_form' => array(
				'type' => 'VARCHAR',
				'constraint' => 11,
				'default' => '1'
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('app_forms');
	}

	public function down()
	{
		$this->dbforge->drop_table('app_forms');
	}
}

