<?php
class Migration_Alter_enroll extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_column('enroll', 's1_subjects');
		$this->dbforge->drop_column('enroll', 's2_subjects');

		$fields = array(
			's1_subjects' => array(
				'type' => 'VARCHAR',
				'constraint' => '1024',
            ),
            's2_subjects' => array(
				'type' => 'VARCHAR',
				'constraint' => '1024',
			),
		);
		$this->dbforge->add_column('enroll', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('s1_subjects', 'enroll');
        $this->dbforge->drop_column('s2_subjects', 'enroll');
	}
}