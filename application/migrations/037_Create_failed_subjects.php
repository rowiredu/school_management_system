<?php
class Migration_Create_failed_subjects extends CI_Migration {
	
	public function up()
	{	
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->add_field (  array(
			'failed_subject_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
            ),
            'student_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
            'subject_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
			'class_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
			'department_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
			),
			'exam_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
			),
			'assessment_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
			'semester' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '11',
            ),
            'year' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '11',
            ),
            'status' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '24',
            ),
            
		));
		$this->dbforge->add_key('failed_subject_id', TRUE);
		$this->dbforge->create_table('failed_subjects');
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_table('failed_subjects');
	}
}