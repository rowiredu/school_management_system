<?php
class Migration_Alter_student extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_column('student', 'first_name');
        $this->dbforge->drop_column('student', 'last_name');
        $this->dbforge->drop_column('student', 'name');
		
		$fields = array(
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '1024',
            ),
            'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '1024',
			),
		);
		$this->dbforge->add_column('student', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge = $this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('student', 'first_name');
        $this->dbforge->drop_column('student', 'last_name');
        $this->dbforge->drop_column('student', 'name');
	}
}