<?php
class Migration_Alter_accountant extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
        $this->dbforge=$this->load->dbforge($this->db, TRUE);
        
        /*
        $this->dbforge->drop_column('accountant', 'first_name');
		$this->dbforge->drop_column('accountant', 'last_name');
		*/
        
		
		$fields = array(
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
            ),
            'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
		);
		$this->dbforge->add_column('accountant', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge = $this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('accountant', 'first_name');
        $this->dbforge->drop_column('accountant', 'last_name');
        $this->dbforge->drop_column('accountant', 'name');
	}
}