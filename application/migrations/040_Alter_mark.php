<?php
class Migration_Alter_mark extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$fields = array(
			'assessment_id' => array(
				'type' => 'INT',
				'constraint' => '8',
            ),
            'exam_mark' => array(
				'type' => 'INT',
				'constraint' => '11',
            ),
            'assessment_mark' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
		);
		$this->dbforge->add_column('mark', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('mark', 'assessment_id');
        $this->dbforge->drop_column('mark', 'exam_mark');
        $this->dbforge->drop_column('mark', 'assessment_mark');
        
	}
}