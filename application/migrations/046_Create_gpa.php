<?php
class Migration_Create_gpa extends CI_Migration {

	public function up()
	{
        $this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$this->dbforge->add_field (  array(
			'gpa_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'student_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),
            'class_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),		
            'section_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),			
            'exam_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),
            'assessment_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),
            'semester' => array(
                'type' => 'LONGTEXT',
            ),
            'year' => array(
                'type' => 'LONGTEXT',
            ),
            'total_marks' => array(
                'type' => 'LONGTEXT',
            ),
            'average_grade_point' => array(
                'type' => 'LONGTEXT',
            ),
            'published_results_id' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),
		));
		$this->dbforge->add_key('gpa_id', TRUE);
		$this->dbforge->create_table('gpa');
	}

	public function down()
	{
        $this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$this->dbforge->drop_table('gpa');
	}
}