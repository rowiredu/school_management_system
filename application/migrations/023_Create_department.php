<?php
class Migration_Create_department extends CI_Migration {
	
	public function up()
	{	
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		

		$this->dbforge->add_field (  array(
			'department_id' => array(
                'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '64',
            ),
            'head_of_department' => array(
                'type'           => 'VARCHAR',
                'constraint'     => '256',
            ),
        ));
        $this->dbforge->add_key('department_id', TRUE);
		$this->dbforge->create_table('department');
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_table('department');
	}
}