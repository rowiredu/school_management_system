<?php
class Migration_Create_courses extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'courses' => array(
				'type' => 'VARCHAR',
                'constraint' => 1024,
                'default' => '[{"wassce":["English","Core Mathematics","Integrated Science","Social Studies","Further Mathematics","Biology","Physics","Chemistry","Geography","ICT","French","Music"],"a_level":["Mathematics","English","Science","Social Studies","Chemistry","Physics","Elective Mathematics"]}]'
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('courses');
	}

	public function down()
	{
		$this->dbforge->drop_table('courses');
	}
}

