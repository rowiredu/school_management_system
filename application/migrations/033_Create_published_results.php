<?php
class Migration_Create_published_results extends CI_Migration {
	
	public function up()
	{	
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->add_field (  array(
			'published_result_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'class_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
			'department_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
			),
			'exam_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
			),
			'assessment_id' => array(
				'type'           => 'INT',
				'constraint'     => '8',
            ),
			'semester' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '11',
            ),
            'year' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '11',
            ),
            'status' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '24',
            ),
            
		));
		$this->dbforge->add_key('published_result_id', TRUE);
		$this->dbforge->create_table('published_results');
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_table('published_results');
	}
}