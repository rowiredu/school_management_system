<?php
class Migration_Alter_failed_subjects extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$fields = array(
			'published_results_id' => array(
				'type' => 'INT',
				'constraint' => '8',
			),
		);
		$this->dbforge->add_column('failed_subjects', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
		$this->dbforge->drop_column('failed_subjects', 'published_results_id');
	}
}