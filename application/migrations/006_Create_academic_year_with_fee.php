<?php
class Migration_Create_academic_year_with_fee extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'year' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'start' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'end' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'gh_currency' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'gh_amount' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'fr_currency' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'fr_amount' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'modified' => array(
				'type' => 'DATETIME',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('academic_year_with_fee');
	}

	public function down()
	{
		$this->dbforge->drop_table('academic_year_with_fee');
	}
}

