<?php
class Migration_Add_initial_data extends CI_Migration {

	public function up()
	{
        $this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

        $teacher_settings = array(
            array(
                'type' => 'running_year',
                'description' => '2018/2019'
            ),
            array(
                'type' => 'semester',
                'description' => '1'
            ),
        );

        $admin_settings = array(
            array(
                'type' => 'running_year',
                'description' => '2018/2019'
                ),
            array(
                'type' => 'semester',
                'description' => '1'
            ),
        );

        $sub_admin_settings = array(
            array(
                'type' => 'running_year',
                'description' => '2018/2019'
            ),
            array(
                'type' => 'semester',
                'description' => '1'
            ),
        );

        $this->db->insert_batch('teacher_settings', $teacher_settings);
        $this->db->insert_batch('admin_settings', $admin_settings);
        $this->db->insert_batch('sub_admin_settings', $sub_admin_settings);
	}

	public function down()
	{
        $this->db = $this->load->database('database2',true);
        $this->dbforge=$this->load->dbforge($this->db, TRUE);
        
        $this->db->empty_table( 'teacher_settings' );
        $this->db->empty_table( 'admin_settings' );
        $this->db->empty_table( 'sub_admin_settings' );

	}
}