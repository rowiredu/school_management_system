<?php
class Migration_Create_sub_admin extends CI_Migration {
	
	public function up()
	{	
		$this->db = $this->load->database('database2',true);
        $this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->add_field (  array(
			'sub_admin_id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'first_name' => array(
				'type'           => 'LONGTEXT',
            ),
            'last_name' => array(
				'type'           => 'LONGTEXT',
            ),
            'email' => array(
				'type'           => 'LONGTEXT',
            ),
            'password' => array(
				'type'           => 'LONGTEXT',
            ),
            'level' => array(
				'type'           => 'LONGTEXT',
            ),
            'authentication_key' => array(
				'type'           => 'LONGTEXT',
            ),
            'phone' => array(
				'type'           => 'LONGTEXT',
            ),
            'address' => array(
				'type'           => 'LONGTEXT',
            ),
            'department_id' => array(
				'type'           => 'INT',
                'constraint'     => '8',
                'null'           => true
			),
            
		));
		$this->dbforge->add_key('sub_admin_id', TRUE);
		$this->dbforge->create_table('sub_admin');
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		$this->dbforge->drop_table('sub_admin');
	}
}