<?php
class Migration_Alter_question_paper extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
        $this->dbforge=$this->load->dbforge($this->db, TRUE);
        
        /*
        $this->dbforge->drop_column('question_paper', 'department_id');
		*/
        
		
		$fields = array(
			'department_id' => array(
				'type' => 'int',
				'constraint' => '11',
            ),
		);
		$this->dbforge->add_column('question_paper', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge = $this->load->dbforge($this->db, TRUE);
		
        $this->dbforge->drop_column('question_paper', 'department_id');
	}
}