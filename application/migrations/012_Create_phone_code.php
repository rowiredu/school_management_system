<?php
class Migration_Create_phone_code extends CI_Migration {

	public function up()
	{
		$fields = array(
			'phone_code' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => "233",
			),
		);
		$this->dbforge->add_column('user_applicant', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('user_applicant', 'read');
	}
}

