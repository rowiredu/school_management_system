<?php
class Migration_Create_teacher_settings extends CI_Migration {

	public function up()
	{
        $this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		$this->dbforge->add_field (  array(
			'settings_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'type' => array(
				'type' => 'LONGTEXT',
            ),
            'description' => array(
				'type' => 'LONGTEXT',
            ),
            
		));
		$this->dbforge->add_key('settings_id', TRUE);
		$this->dbforge->create_table('teacher_settings');
	}

	public function down()
	{
        $this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);

		//$this->dbforge->drop_table('teacher_settings');
	}
}