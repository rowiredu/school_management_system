<?php
class Migration_Alter_subject2 extends CI_Migration {
	
	public function up()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
		$fields = array(
			'semester' => array(
				'type' => 'VARCHAR',
				'constraint' => '1024',
			),
		);
		$this->dbforge->add_column('subject', $fields);
	}

	public function down()
	{
		$this->db = $this->load->database('database2',true);
		$this->dbforge=$this->load->dbforge($this->db, TRUE);
		
		$this->dbforge->drop_column('subject', 'semester');
	}
}