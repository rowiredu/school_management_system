<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('academics_model');
		$this->load->model('payment_models');
		$this->load->model('app_forms_models');
    }
    
	public function index()
	{
		$this->data['transactions'] = $this->payment_models->get_all() == false ? array() :  $this->payment_models->limit(7)->get_all();
        $this->data['ad_year'] 		= $this->academics_model->get_all()  == array() ? null : $this->academics_model->get_all();

        $this->data['content'] = 'Dashboard';
        $this->load->view('Portal/Anatomy/Anatomy' , $this->data );
    }
    
}
