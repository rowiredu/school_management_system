<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * 
 */
class Academics extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('academics_model');
		$this->within_admission();
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['appropirate_academics_fld'] = $this->academics_model->list_fields();
		$unset = array();
		$unset[] = array_search('id', $this->data['appropirate_academics_fld']);
		$unset[] = array_search('user_id', $this->data['appropirate_academics_fld']);
		foreach($unset as $remove){
			unset($this->data['appropirate_academics_fld'][$remove]);
		}
		
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
        $this->data['content'] 		= 'Academics/Home';
		$this->load->view('Portal/Anatomy/Anatomy' , $this->data );
	}


	/**
	 * 
	 */
	public function within_admission()
	{
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
		$this->data['within_admission'] = false;
		foreach($this->data['data'] as $single){
			$current = date('m/d/Y');
			if(( $current >= $single->start ) && ( $current <= $single->end )){
				$this->data['within_admission'] = $single->year;
				break;
			} else if( $current < $single->end ){
				$this->data['within_admission'] = $single->year;
				break;
			}
		}
	}
	/**
	 * 
	 */
	public function records()
	{
		
		//Setting rules for forms validation

        // Set up the form
		$rules = $this->academics_model->rules;
		$this->form_validation->set_rules($rules);

		// Set up the form
		$form = $this->input->post();
		if ($form) {
			if ($this->form_validation->run() !== false) {
				unset($form['submit']);
				$form['user_id'] = $this->session->userdata('user_id');
				
				if($this->academics_model->insert($form)) {
					$this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
				} else {
					$this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
				}
			} else {
				$this->errors[] = "coundn't create data";
				$this->session->set_flashdata('mydata',  $this->errors );
			}
			$this->session->set_flashdata('mydata', $this->data);
			redirect('portal/academics');
		}
	}


}
