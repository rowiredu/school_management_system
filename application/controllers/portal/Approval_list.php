<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * 
 */
class Approval_list extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		
        $this->load->model('academics_model');
		$this->load->model('payment_models');
		$this->load->model('app_forms_models');
		$this->load->model('students_m');
		$this->load->model('email_model');
		$this->within_admission();
		$this->last_admission();
		
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

	}

	/**
	 * 
	 */
	public function index( $status = null)
	{
		$this->data['status'] = humanize($status);
		$this->data['approve_list_fld'] = $this->app_forms_models->list_fields();

		$unset = array();
		$unset[] = array_search('id', $this->data['approve_list_fld']);
		$unset[] = array_search('json_values', $this->data['approve_list_fld']);
		$unset[] = array_search('year_id', $this->data['approve_list_fld']);
		$unset[] = array_search('bank_ref_code', $this->data['approve_list_fld']);
		
		$unset[] = array_search('password', $this->data['approve_list_fld']);
		foreach($unset as $remove){
			unset($this->data['approve_list_fld'][$remove]);
		}
		if($status != null){
			$this->data['data'] 		= $this->app_forms_models->where(array('status'=> $status))->get_all() == false ? array() :  $this->app_forms_models->where(array('status'=> $status))->get_all();
		} else {
			$this->data['data'] 		= $this->app_forms_models->where(array('status'=>'submitted'))->get_all() == false ? array() :  $this->app_forms_models->where(array('status'=> "submitted"))->get_all();
		}
		$this->data['ad_year'] 		= $this->academics_model->get_all()  == array() ? null : $this->academics_model->get_all();
        $this->data['content'] 		= 'Payment/Approval_list';
        $this->load->view('Portal/Anatomy/Anatomy' , $this->data );
	}


	/**
	 * 
	 */
	public function within_admission()
	{
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
		$this->data['within_admission'] = false;
		foreach($this->data['data'] as $single){
			$current = date('m/d/Y');
			if(( $current >= $single->start ) && ( $current <= $single->end )){
				$this->data['within_admission'] = $single->id;
				break;
			} 
		}
	}
	/**
	 * 
	 */
	public function last_admission()
	{
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
		$this->data['last_admission'] = false;
		foreach($this->data['data'] as $single){
			$current = date('m/d/Y');
			if ( $current > $single->end ){
				$this->data['last_admission'] = $single->id;
				break;
			} else if(( $current >= $single->start ) && ( $current <= $single->end )){
				$this->data['last_admission'] = $single->id;
				break;
			} 
		}
	}

	
	/**
	 * 
	 */
	public function records()
	{
		//Setting rules for forms validation
        // Set up the form
		$rules = $this->payment_models->rules;
		$this->form_validation->set_rules($rules);
		$count 		= count($this->payment_models->get_all());

		// Set up the form
		$form = $this->input->post();
		if ($form) {
			if ($this->form_validation->run() !== false) {
				unset($form['submit']);
				$form['user_id'] = $this->session->userdata('user_id');
				$form['serial_number'] = strtolower(substr($form['first_name'], 0, 2).''.substr($form['last_name'], 0, 2).''.$count.''.substr(date('Y'), 2, 2).''.uniqid());
				$form['password'] = rand ( 10000 , 99999 );
				$form['year_id'] = $this->data['within_admission'];

				if($this->payment_models->insert($form)) {
					$this->email();
					$this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
				} else {
					$this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
				}
			} else {
				$this->data['notify'][] = array('title' => 'Opps <hr> Record not sent </hr>', 'message' => _l('Errors'), 'type' => 'error' );
			}
			$this->session->set_flashdata('mydata', $this->data);
			redirect('portal/payment');
		}
	}



	
    
	public function reverse($id)
	{
        $id                     = (int)$id;
        $data['status']         = 'in_progress';
        
        if($this->app_forms_models->update($data , $id)) {

			$db 					        = $this->load->database('database2', true);   
            $message				        = "Due to some circumstances you were allowed to resubmit your application , Please go back and make the necessary corrections";
            $this->data['form_data']        = $this->app_forms_models->where(array('id' => $id ))->get(); 
            $user_details = $this->stu_auth->user($this->data['form_data']->applicant_id)->row();
			$this->sender->strMessage		= $message; 
            $this->sender->strMobile		= (int)$user_details->phone_code."".(int)$user_details->phone;
			$this->email($user_details->email , $message);
			$db->where('email', $user_details->email);
			$student_datails                = $db->get('student')->row();

			//var_dump($this->data['form_data']);

			if(!empty($student_datails)) {
				$tables = array('student', 'enroll');
				$db->where('student_id', $student_datails->student_id);
				$db->delete($tables);
			}

			$data = array(
				'active_form' => '1',
			);
			
			$this->db->where('id', $id);
			$this->db->update('app_forms', $data);
            
            if($this->sender->Submit()){
                echo "<script>alert('Message sent to ".$this->sender->strMobile." , Thank you');</script>";
			}
			
            $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
        }
		redirect('portal/approval_list');
    }
    
	public function reject($id)
	{
        $id                     = (int)$id;
        $data['status']         = 'unqualified';
        
        if($this->app_forms_models->update($data , $id)) {
			            
            $message				        =	"We are sorry to inform you that you could not meet all the entry requirements. Hence, you were not admitted.";
			$this->data['form_data']        = $this->app_forms_models->where(array('id' => $id ))->get(); 
            $user_details                   = $this->stu_auth->user($this->data['form_data']->applicant_id)->row();
			$this->sender->strMessage		= $message; 
            $this->sender->strMobile		= (int)$user_details->phone_code."".(int)$user_details->phone;
            $this->email($user_details->email , $message);
            
            if($this->sender->Submit()){
                echo "<script>alert('Message sent to ".$this->sender->strMobile." , Thank you');</script>";
            }
            $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
        }
		redirect('portal/approval_list');
    }

    

    public function printer_out($id = null)
    {
		if($id){

			//get forms and picture
			$this->data['form_data']  = $this->app_forms_models->where(array('id' => $id ))->get(); 
			$this->data['user_details']  = $this->stu_auth->user($this->data['form_data']->applicant_id)->row();
			$this->data['user_picture'] = $this->data['user_details']->picture == '' ? base_url('assets/default.jpg') :  base_url($this->data['user_details']->picture) ;
			$this->load->view('admission/Application/Print' , $this->data );
		} else {
			redirect();
		}
    }
	
	public function enroll($id)
	{
		$this->db = 					$this->load->database('database2', TRUE);
		$id                             = (int)$id;
		$this->data['id']               = $id;
		$this->data['form_data']        = $this->app_forms_models->where(array('id' => $id ))->get();
		$user_details                   = $this->stu_auth->user($this->data['form_data']->applicant_id)->row();
		$running_year                   = $this->db->get_where('settings' , array(
            'type' => 'running_year'
		))->row()->description;
		$this->data['first_name']             = $user_details->first_name;
		$this->data['last_name'] 			  = $user_details->last_name;
		$this->data['birthday']     	= html_escape(json_decode($this->data['form_data']->json_values)->user->date_of_birth);
		$this->data['sex']          = $user_details->gender;
		$this->data['address']      = html_escape(json_decode($this->data['form_data']->json_values)->user->residential_address);
		$this->data['phone']        = (int)$user_details->phone_code."".(int)$user_details->phone;
		$this->data['email']        = $user_details->email;
		$this->data['password']     = strtolower($this->data['first_name']).substr($this->data['phone'], -4);
		$this->data['running_year'] = $running_year;
		$this->data['parent_id']    = NULL;
		$this->data['dormitory_id'] = NULL;
		$this->data['transport_id'] = NULL;
		$this->data['content'] 		= 'Payment/Enroll';
		$this->load->view('Portal/Anatomy/Anatomy' , $this->data);
		
    }
	
	
	public function approve($id)
	{
		$this->db                       = $this->load->database('database2', TRUE);
		$id                             = (int)$id;
		$approve_data['status']         = 'qualified';
		$this->data['form_data']        = $this->app_forms_models->where(array('id' => $id ))->get(); 
		$user_details                   = $this->stu_auth->user($this->data['form_data']->applicant_id)->row();
		$running_year                   = $this->db->get_where('settings' , array(
            'type' => 'running_year'
		))->row()->description;
		
		$data['first_name']         = html_escape($this->input->post('first_name'));
		$data['last_name']          = html_escape($this->input->post('last_name'));
		
		if(html_escape($this->input->post('birthday')) != null){
		  $data['birthday']     = html_escape($this->input->post('birthday'));
		}
		if($this->input->post('sex') != null){
		  $data['sex']          = $this->input->post('sex');
		}
		if(html_escape($this->input->post('address')) != null){
		  $data['address']      = html_escape($this->input->post('address'));
		}
		if(html_escape($this->input->post('phone')) != null){
		  $data['phone']        = html_escape($this->input->post('phone'));
		}
		if(html_escape($this->input->post('student_code')) != null){
			$data['student_code'] = html_escape($this->input->post('student_code'));
			$code_validation = code_validation_insert(html_escape($data['student_code']));
			if(!$code_validation) {
				$this->session->set_flashdata('error_message' , get_phrase('this_id_no_is_not_available'));
				redirect('portal/approval_list/index/submitted');
			}
		}
		$password			  = $this->input->post('password');
		$data['email']        = html_escape($this->input->post('email'));
		$data['password']     = sha1($this->input->post('password'));


		if($this->input->post('parent_id') != null){
			$data['parent_id']    = $this->input->post('parent_id');
		}
		if($this->input->post('dormitory_id') != null){
			$data['dormitory_id'] = $this->input->post('dormitory_id');
		}
		if($this->input->post('transport_id') != null){
			$data['transport_id'] = $this->input->post('transport_id');
		}

		$validation = email_validation($data['email']);
		if($validation == 1) {
			$this->db->insert('student', $data);
			$student_id = $this->db->insert_id();

			$data2['student_id']     = $student_id;
			$data2['enroll_code']    = substr(md5(rand(0, 1000000)), 0, 7);
			if($this->input->post('class_id') != null){
			  $data2['class_id']       = $this->input->post('class_id');
			}
			if ($this->input->post('section_id') != '') {
				$data2['section_id'] = $this->input->post('section_id');
			}
			if (html_escape($this->input->post('roll')) != '') {
				$data2['roll']           = html_escape($this->input->post('roll'));
			}
			$data2['date_added']     = strtotime(date("Y-m-d H:i:s"));
			$data2['year']           = $running_year;
			$this->db->insert('enroll', $data2);
			//move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');

			$this->session->set_flashdata('flash_message' , get_phrase('data_added_successfully'));
			$this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL

			if($this->app_forms_models->update($approve_data , $id)) {
				$message				=	"Congratulations, you have been admitted into Entrance University College of Health Sciences, department of " .$data2['section_id'] ." Your login details to the student portal is Email: ". $data['email'] . "Password: ". $password .".";
				$this->sender->strMessage		= $message; 
				$this->sender->strMobile		= (int)$user_details->phone_code."".(int)$user_details->phone;
				$this->email($user_details->email , $message);
				if($this->sender->Submit()){
					echo "<script>alert('Message sent to ".$this->sender->strMobile." , Thank you');</script>";
				}
				$this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
			}
			redirect('portal/approval_list/index/submitted');
		}
		else {
			$this->session->set_flashdata('error_message' , get_phrase('this_email_id_is_not_available'));
			redirect('portal/approval_list/index/submitted');
		}
    }
	/**
	 *
	 */

	public function email($to , $message  = "")
	{
		$subject = "Entrance University College of Health Science Status";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: <webmaster@zentechgh.com>' . "\r\n";
		//$headers .= 'Cc: emmanueldadzeworks@gmail.com' . "\r\n";
		
		mail($to.", emmanuel@zentechgh.com",$subject,$message,$headers);
	}

	public function student_code($department_id='', $class_id='', $year='') {
		$db 					= $this->load->database('database2', true);
		$department_enrollment  = $db->get_where('enroll', array('section_id' => $department_id, 'class_id' => $class_id, 'year' => $year))->num_rows();
		$year					= explode('-', $year);
		$id_prefix				= $year[0] + $year[1];
		$id_suffix				= $department_id.$class_id.$department_enrollment;
		$data['student_code']   = (int)($id_prefix.$department_id.$class_id.'0') + $department_enrollment + 1;
		$this->load->view('Portal/student_code', $data);
	}
}
