<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * 
 */
class Payment extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model('academics_model');
		$this->load->model('payment_models');
		$this->within_admission();
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

	}

	/**
	 * 
	 */
	public function index($receipt = null)
	{
		if($receipt){
			$this->data['js_show'] 		= "<script type='text/javascript'>$(window).on('load',function(){ $('#receipt".$receipt."').modal('show'); }); printElem('receipt".$receipt."'); </script>";
		}

		$this->data['appropirate_payment_fld'] = $this->payment_models->list_fields();
		$unset = array();
		$unset[] = array_search('id', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('user_id', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('year_id', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('bank_ref_code', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('salt', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('serial_number', $this->data['appropirate_payment_fld']);
		$unset[] = array_search('modified', $this->data['appropirate_payment_fld']);
		
		$unset[] = array_search('password', $this->data['appropirate_payment_fld']);
		foreach($unset as $remove){
			unset($this->data['appropirate_payment_fld'][$remove]);
		}
		
		if($this->ion_auth->is_admin() ){
			$total_records 				= $this->payment_models->where("created >= (SELECT COALESCE(MAX(created), 0) FROM `academic_year_with_fee`)", NULL, NULL, FALSE, FALSE, TRUE)->get_all();
			$this->data['data'] 		= $total_records == false ? array() :  $total_records;
		} else {
			$own_records = $this->payment_models->where(array('user_id'=> $this->session->userdata('user_id') ))->get_all();
			$this->data['data'] 		= $own_records == false ? array() :  $own_records;
		}

		$this->data['ad_year'] 		= $this->academics_model->get_all()  == array() ? null : $this->academics_model->get_all();
		$this->data['content'] 		= 'Payment/Home';
		$this->load->view('Portal/Anatomy/Anatomy' , $this->data );
	}


	/**
	 * 
	 */
	public function within_admission()
	{
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
		$this->data['within_admission'] = false;
		foreach($this->data['data'] as $single){
			$current = date('m/d/Y');
			if(( $current >= $single->start ) && ( $current <= $single->end )){
				$this->data['within_admission'] = $single;
				break;
			}
		}
	}
	/**
	 * 
	 */
	public function records($id = null)
	{
		
		//Setting rules for forms validation
        // Set up the form
		$rules = $this->payment_models->rules;
		$this->form_validation->set_rules($rules);
		$count 		= count($this->payment_models->get_all());

		// Set up the form
		$form = $this->input->post();
		unset($form['submit']);
		if($id !== null){
			$id 			= (int)$id;
			$override 		= (array)$this->payment_models->where(array('id'=> $id ))->get();
			unset($override['id']);
			unset($override['created']);
			unset($override['modified']);
			$form 		= array_merge($override, $form);
		}

		if ($form) {
			$response = null;
			$form['user_id'] = $this->session->userdata('user_id');
			$form['serial_number'] = strtolower(substr($form['first_name'], 0, 2).''.substr($form['last_name'], 0, 2).''.$count.''.substr(date('Y'), 2, 2).''.uniqid());
			$form['password'] = rand ( 10000 , 99999 );
			$form['year_id'] = $this->data['within_admission']->id;

			if($id !== null){
				$response = $this->payment_models->update($form , array('id'=> $id ) );
			} else if ($this->form_validation->run() !== false) {
				$response = $this->payment_models->insert($form);
			}
			if($response !== null) {
				$this->email($form);
				$this->sms($form);
				$this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
			} else {
				$this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
			}

			$this->session->set_flashdata('mydata', $this->data);
			if($id == null){
				redirect('portal/payment/index/'.$response);
			} else{
				redirect('portal/payment/index/'.$id);
			}
		}
	}



	/**
	 *
	 */
	public function resend_ajax($id = null)
	{
		if($id !== null){
			//get data
			$data = (array)$this->payment_models->where(array('id'=> $id ))->get();
			$email_data = $this->email($data);
			$sms_data = $this->sms($data);
			if($email_data){
				if(startsWith($sms_data, "1701") )
				echo "sent";
			}
		}
	}
	/**
	 *
	 */
	public function sms( $data = null)
	{
		//Call the sms libiary
		if($data !== null)
		{
			$message				=	"hello ".$data['first_name'].", \n Thanks for purchasing your application forms as a ".$data['reference']." with Entrance University College of Health Science. Below are your serial number and pin to complete the admission process. \n\n";
			$message			   .=	"serial code : '".$data['serial_number']."' \n";
			$message			   .=	"and this code '".$data['password']."' \n";

			$this->sender->strMessage		= $message; 
			$this->sender->strMobile		= (int)$data['phone'];
			$response = $this->sender->Submit();
			if($response){
				return $response;
			}
		}
	}


	/**
	 *
	 */
	public function email( $data = null)
	{

		$to = $data['email'].", emmanuel@zentechgh.com";
		$subject = "Entrance University College of Health Science Admission login details";
		
		$message = "
		<html>
		<head>
		<title> Entrance University College of Health Science Admission login details </title>
		</head>
		<body>
		<p>Hi ".$data['first_name']." ".$data['last_name'].",This email contains your login details for online application for current year admission only </p>
		<table>
			<tr>
				<th>Serial Number</th>
				<th>".$data['serial_number']."</th>
			</tr>
			<tr>
				<td>Pin</td>
				<td>".$data['password']."</td>
			</tr>
		</table>
		</body>
		</html>
		";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: <info@euchs.com>' . "\r\n";
		//$headers .= 'Cc: emmanueldadzeworks@gmail.com' . "\r\n";
		
		return mail($to,$subject,$message,$headers);
	}


}
