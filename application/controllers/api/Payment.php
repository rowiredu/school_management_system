<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Aplikcation 
 * 
 */
class Payment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->data[''] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->load->model('academics_model');
		$this->load->model('payment_models');
		$this->load->library('sender');
	}

	/**
	 * 
	 */
	public function index()
	{
        $data = $_POST;
		if(empty($data)){
			$rapi = json_encode(array('error' => 'Sorry, No Data transmitted'));
		} else if($this->within_admission() === false ) {
			$rapi = json_encode(array('error' => 'Sorry, Admission not in progress'));
        } else {
            $auth = json_decode($this->login());
            if($auth->auth == true){
                //perform by Action
                if($data['action'] == 'payment'){

					$national 		= array('ghanaian_student' , 'foreign_student');
                    $bank_ref_code 	= $this->polish($data['reference_code']);
                    $first_name   	= $this->polish($data['first_name']);
                    $last_name     	= $this->polish($data['last_name']);
                    $phone         	= $this->polish((int)$data['phone']);
                    $email         	= $this->polish($data['email']);
                    $reference     	= $this->polish($national[(int)$data['student_national']]);
                    $error      	= null;

					if (preg_match("/^[a-zA-Z ]*$/", $first_name )) {
					  $error = "Only letters and white space allowed in first name"; 
					} else if (preg_match("/^[a-zA-Z ]*$/", $last_name )) {
					  $error = "Only letters and white space allowed in last name"; 
					} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					    $error =  "Email address '$email' is considered invalid.\n";
					}



					if ($error !== null) {
						$rapi = json_encode(array('error' => $error));
					} else {
	                    $Payment = array(
	                        "bank_ref_code"         => $bank_ref_code,
	                        "first_name"            => $first_name,
	                        "last_name"             => $last_name,
	                        "phone"                 => $phone,
	                        "email"                 => $email,
	                        "reference"             => $reference,
	                    );
	                    $rapi = json_encode($this->save_api($Payment));
					}
                } else {
			        $rapi = json_encode(array('error' => 'Sorry, No Action transmitted'));
                }
            } else {
            	$rapi = json_encode(array('error' => $auth));
            }

        }
        echo trim($rapi);

	}



	/**
	 * Log the user in
	 */
	public function polish($data = null) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	/**
	 * Log the user in
	 */
	public function login()
	{
		$this->data['title'] = $this->lang->line('login_heading');

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = false;

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				return json_encode(array('auth' => true , 'message' => $this->ion_auth->messages()));
			}
			else
			{
				// if the login was un-successful
				return json_encode(array('auth' => false , 'message' => $this->ion_auth->errors()));
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$return_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$return_data['auth'] = false;

			return json_encode($return_data);
		}
	}

	/**
	 * 
	 */
	public function within_admission()
	{
		$this->data['data'] 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
		$this->data['within_admission'] = false;
		foreach($this->data['data'] as $single){
			$current = date('m/d/Y');
			if(( $current >= $single->start ) && ( $current <= $single->end )){
				return $single;
			}
        }
        return false;
	}
	/**
	 * 
	 */
	public function save_api($data = null)
	{
		if ($data) {
            $count 		= count($this->payment_models->get_all());
            $save_data = null;

			$data['user_id'] = $this->session->userdata('user_id');
			$data['serial_number'] = strtolower(substr($data['first_name'], 0, 2).''.substr($data['last_name'], 0, 2).''.$count.''.substr(date('Y'), 2, 2).''.uniqid());
			$data['password'] = rand ( 10000 , 99999 );
            $data['year_id'] = $this->within_admission()->id;
            
            $save_data = $this->payment_models->insert($data);

			if($save_data !== null) {
				$email 	= $this->email($data);
				$sms 	= startsWith($this->sms($data), '1701') ;
				
				return array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' , 'responce' => array('sms' => $sms , 'email' => $email , )); //, 'id' => $save_data 
			} else {
				return array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
			}
		} else {
            return array('title' => 'Oops', 'message' => _l('this Should follow Protocols'), 'type' => 'error' );
        }
	}


	/**
	 *
	 */
	public function sms( $data = null)
	{
		//Call the sms libiary
		if($data !== null)
		{
			$message				=	"hello ".$data['first_name'].", \n Thanks for purchasing your application forms as a ".$data['reference']." with Entrance University College of Health Science. Below are your serial number and pin to complete the admission process. \n\n";
			$message			   .=	"serial code : '".$data['serial_number']."' \n";
			$message			   .=	"and this code '".$data['password']."' \n";

			$this->sender->strMessage		= $message; 
			$this->sender->strMobile		= (int)$data['phone'];
			$response = $this->sender->Submit();
			if($response){
				return $response;
			}
		}
	}


	/**
	 *
	 */
	public function email( $data = null)
	{

		$to = $data['email'].", emmanuel@zentechgh.com";
		$subject = "Entrance University College of Health Science Admission login details";
		
		$message = "
            <html>
				<head>
					<title> Entrance University College of Health Science Admission login details </title>
				</head>
            <body>
				<p>Hi ".$data['first_name']." ".$data['last_name'].",This email contains your login details for online application for current year admission only </p>
				<table> 
					<tr> <th>Serial Number</th> <td>".$data['serial_number']."</td></tr>
					<tr> <th>Pin</th> <td>".$data['password']."</td> </tr>
				</table>
				</body>
            </html>
		";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: <info@euchs.com>' . "\r\n";
		$headers .= 'Cc: emmanueldadzeworks@gmail.com' . "\r\n";
		
		return mail($to,$subject,$message,$headers);
	}


}
