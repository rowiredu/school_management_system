<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admission_Controller {
    
    function __construct()
    {
        parent::__construct();
        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $this->load->model('app_forms_models');
        $this->load->model('academics_model');
    }
    
	public function index()
	{
        //dump_exit($this->data['data']);

        /*status == in_progress
        status == submitted
        status == qualified
        status == accepted
        */

		$form = $this->input->post();
		if ($form) {
            $data['applicant_id']   = $this->session->userdata('user_id');
            $data['status']         = 'in_progress';
            $data['json_values']    = json_encode($form);
            
            if($this->app_forms_models->insert($data)) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
        $this->data['content'] = 'Dashboard';
        $this->load->view('admission/Anatomy/Anatomy' , $this->data );
    }
    


    
}
