<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class auth/
 * @property stu_auth|stu_auth_model $stu_auth        The ION auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Auth extends Admission_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->stu_auth->errors() ? $this->stu_auth->errors() : $this->session->flashdata('message')));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'stu_auth'), $this->config->item('error_end_delimiter', 'stu_auth'));
		//$this->login();
		//$this->register();
	}

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	public function index()
	{
		$this->login();
	}

	/**
	 * Log the user in
	 */
	public function login()
	{
		$this->data['title'] = $this->lang->line('login_heading');

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', _l('email')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', _l('password')), 'required');

		
		if ($this->form_validation->run() === TRUE)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool)$this->input->post('remember');
			if ($this->stu_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->stu_auth->messages());
				redirect('admission/dashboard', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->stu_auth->errors());
				redirect('admission/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'email',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			$this->_render_page('admission' . DIRECTORY_SEPARATOR . 'login', $this->data);
		}
	}

	/**
	 * Create a new user
	 */
	public function register()
	{

		$tables = $this->config->item('tables', 'stu_auth');
		$identity_column = $this->config->item('identity', 'stu_auth');
		$this->data['identity_column'] = $identity_column;


		$this->data['first_name'] = array(
			'name' => 'first_name',
			'id' => 'first_name',
			'type' => 'text',
			'value' => $this->form_validation->set_value('first_name'),
		);
		$this->data['username'] = array(
			'name' => 'username',
			'id' => 'username',
			'type' => 'text',
			'value' => $this->form_validation->set_value('username'),
		);
		$this->data['last_name'] = array(
			'name' => 'last_name',
			'id' => 'last_name',
			'type' => 'text',
			'value' => $this->form_validation->set_value('last_name'),
		);
		$this->data['date_of_birth'] = array(
			'name' => 'date_of_birth',
			'id' => 'date_of_birth',
			'type' => 'date',
			'value' => $this->form_validation->set_value('date_of_birth'),
		);
		$this->data['gender'] = array(
			'name' => 'gender',
			'id' => 'gender',
			'type' => 'text',
			'value' => $this->form_validation->set_value('gender'),
		);
		$this->data['email'] = array(
			'name' => 'email',
			'id' => 'email',
			'type' => 'email',
			'value' => $this->form_validation->set_value('email'),
		);
		$this->data['phone'] = array(
			'name' => 'phone',
			'id' => 'phone',
			'type' => 'number',
			'value' => $this->form_validation->set_value('phone'),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id' => 'password',
			'type' => 'password',
			'value' => $this->form_validation->set_value('password'),
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id' => 'password_confirm',
			'type' => 'password',
			'value' => $this->form_validation->set_value('password_confirm'),
		);


		// validate form input

		if($_POST) { 
			foreach($_POST as $key => $value){
				$this->data[$key]['value'] = $value;
			}
			if($_POST['submit'] == 'Create User') { 
				$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
				$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
				if ($identity_column !== 'email')
				{
					$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
					$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
					$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim|required|is_unique[' . $tables['users'] . '.phone]');
				}
				else
				{
					$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
					$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim|required|is_unique[' . $tables['users'] . '.phone]');
				}
				//$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
				$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'stu_auth') . ']|max_length[' . $this->config->item('max_password_length', 'stu_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

				if ($this->form_validation->run() === TRUE)
				{
					$email = strtolower($this->input->post('email'));
					$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
					$password = $this->input->post('password');

					$additional_data = array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						/*'date_of_birth' => $this->input->post('date_of_birth'),*/
						'date_of_birth' => '',
						/*'username' => $this->input->post('username'),*/
						'username' => '',
						'gender' => $this->input->post('gender'),
						'phone_code' => $this->input->post('phone_code'),
						'phone' => $this->input->post('phone'),
						'json_encoded_prefield' 	=> '{"qualification":[],"father":{"first_name":"","last_name":"","occupation":"","phone":"","address":""},"mother":{"first_name":"","last_name":"","occupation":"","phone":"","address":""},"guardian":{"relation":"","first_name":"","last_name":"","email":"","occupation":"","phone":""},"emergency":[{"first_name":"","last_name":"","relation":"","email":"","occupation":"","phone":"","address":""},{"first_name":"","last_name":"","relation":"","email":"","occupation":"","phone":"","address":""}],"reference":[{"first_name":"","last_name":"","relation":"","email":"","occupation":"","phone":"","address":""},{"first_name":"","last_name":"","relation":"","email":"","occupation":"","phone":"","address":""}]}',

					);
				}

				if($this->form_validation->run() === False ){
					// if the login was un-successful
					// redirect them back to the login page
					$this->session->set_flashdata('message', $this->stu_auth->messages());
				}
				if ($this->form_validation->run() === TRUE && $this->stu_auth->register($identity, $password, $email, $additional_data))
				{
					// check to see if we are creating the user
					// redirect them back to the admin page
					$this->session->set_flashdata('message', $this->stu_auth->messages());
					redirect("admission/auth/login", 'refresh');
				}


			}
		}
		$this->_render_page('admission' . DIRECTORY_SEPARATOR . 'register', $this->data);
    }
	
	


	/**
	 * Forgot password
	 */
	public function forgot_password()
	{
		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'stu_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'stu_auth');
			// setup the input
			$this->data['identity'] = array(
				'name' => 'identity',
				'id' => 'identity',
			);

			if ($this->config->item('identity', 'stu_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('admission' . DIRECTORY_SEPARATOR . 'Forgot', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'stu_auth');
			$identity = $this->stu_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'stu_auth') != 'email')
				{
					$this->stu_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->stu_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->stu_auth->errors());
				redirect("admission/auth/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->stu_auth->forgotten_password($identity->{$this->config->item('identity', 'stu_auth')});
			
			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->stu_auth->messages());
				redirect("admission/auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->stu_auth->errors());
				redirect("admission/auth/forgot_password", 'refresh');
			}
		}
	}


	/**
	 * Reset password - final step for forgotten password
	 *
	 * @param string|null $code The reset code
	 */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->stu_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'stu_auth') . ']|max_length[' . $this->config->item('max_password_length', 'stu_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'stu_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('admission' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->stu_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'stu_auth')};

					$change = $this->stu_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->stu_auth->messages());
						redirect("admission/auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->stu_auth->errors());
						redirect('admission/auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->stu_auth->errors());
			redirect("admission/auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Activate the user
	 *
	 * @param int         $id   The user ID
	 * @param string|bool $code The activation code
	 */
	public function activate($id, $code = FALSE)
	{
		if ($code !== FALSE)
		{
			$activation = $this->stu_auth->activate($id, $code);
		}
		else if ($this->stu_auth->is_admin())
		{
			$activation = $this->stu_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->stu_auth->messages());
			redirect("portal/auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->stu_auth->errors());
			redirect("portal/auth/forgot_password", 'refresh');
		}
	}
	/**
	 * Log the user out
	 */
	public function logout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->stu_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->stu_auth->messages());
		redirect('admission/auth/login', 'refresh');
	}

}
