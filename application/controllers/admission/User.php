<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admission_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->load->model('app_forms_models');
        $this->load->model('academics_model');
        $this->load->model('payment_models');
        
        $this->ind_prop = json_decode($this->data['user_details']->json_encoded_prefield , true);
    }
    


	public function index()
	{
        //redirect

    }
    public function profile()
    {
        $picture = $this->do_upload('profile');
		$form = $_FILES['profile'];
		if ($form) {
            if(!isset( $picture['error'] )){
                $data['picture']         = $picture;
                //var_dump($picture);
                if($this->applicant_model->update($data , $this->session->userdata('user_id') )) {
                    $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
                } else {
                    $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
                }
                $this->session->set_flashdata('mydata', $this->data);
                
            } else {
                //dump errors on screen
                $this->session->set_flashdata('error', $this->data);

            }
        }
        redirect('admission/dashboard');
    }
    

    public function profile_details()
    {
		$form = $this->input->post();
		if ($form) {
            $data                   = $form;
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			
        }
        redirect('admission/dashboard');
    }

    
    public function rm_qualification($key = null)
    {
		if ($key !== null) {
            unset($this->ind_prop['qualification'][$key]);

            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }

    
    public function qualification()
    {
		$form = $this->input->post();
		if ($form) {
            $form['qualification']['picture'] = $this->do_upload('picture');
            $this->ind_prop['qualification'][] = $form['qualification'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }

    
    public function father()
    {
		$form = $this->input->post();
		if ($form) {
            $this->ind_prop['father'] = $form['father'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }
    
    public function mother()
    {
		$form = $this->input->post();
		if ($form) {
            $this->ind_prop['mother'] = $form['mother'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }
    
    public function guardian()
    {
		$form = $this->input->post();
		if ($form) {
            $this->ind_prop['guardian'] = $form['guardian'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }
    
    public function emergency( $key = null)
    {
		$form = $this->input->post();
		if ($form) {
            $this->ind_prop['emergency'][$key] = $form['emergency'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }
    
    public function reference( $key = null)
    {
		$form = $this->input->post();
		if ($form) {
            $this->ind_prop['reference'][$key] = $form['reference'];
            $data['json_encoded_prefield']   = json_encode($this->ind_prop);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )){
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }
    
    public function link_payment()
    {
        $key = $form_data->id;
        //var_dump($key);
		$form = $this->input->post();
		if ($form) {
            $extract = $this->payment_models->where(array('serial_number' =>  $form['serial_code'] , 'password' => $form['pin']  ))->get(); 
            //dump_exit($extract);
            if($extract ){
                $used = $this->app_forms_models->where(array('payment_id' =>  $extract->id  ))->get(); 
                if($used->year_id == $this->data['within_admission']->year_id){
                    if(!$used){
                        $data['payment_id']   = $extract->id;
                        if($this->app_forms_models->update($data , $key )) {
                            $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent. Please submit your forms below.'), 'type' => 'success' );
                        } else {
                            $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Incorrect Serial number and Pin'), 'type' => 'error' );
                        }
                    } else {
                        $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Sorry Serial number already used'), 'type' => 'error' );
                    }
                } else {
                    $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Sorry Serial number was not bought this academic year'), 'type' => 'error' );
                }
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Sorry Serial number is incorrect'), 'type' => 'error' );
            }

            
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/admission');
		}
    }
    
    public function save_forms()
    {
		$form = $this->input->post();
		if ($form) {
            $data['applicant_id']   = $this->session->userdata('user_id');
            $data['status']         = 'in_progress';
            $data['json_values']    = json_encode($form);
            
            if($this->applicant_model->update($data , $this->session->userdata('user_id') )) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
			$this->session->set_flashdata('mydata', $this->data);
			redirect('admission/dashboard');
		}
    }


}
