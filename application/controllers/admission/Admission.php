<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission extends Admission_Controller {
    
    function __construct()
    {
        parent::__construct();
        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $this->load->model('academics_model');
        $this->load->model('applicant_model');
		$this->data['within_admission'] == false ? redirect('admission/dashboard')  : '' ;

    }
    


	public function index()
	{
        //dump($this->data['form_data']);
        //$this->data['form_data'] = ($this->data['form_data'] == false) ? new stdClass() : $this->data['form_data'] ;
        //dump_exit($this->data['form_data']);

        /*
        status == in_progress
        status == submitted
        status == qualified
        status == accepted
        */

        if(isset($this->data['form_data'])) :
            switch ($this->data['form_data']->status) {
                case 'in_progress':
                    $this->data['content'] = 'Application/Forms';
                    break;
                
                case 'submitted':
                    $this->data['content'] = 'Application/Status';
                    break;
                
                case 'qualified':
                    $this->data['content'] = 'Application/Status';
                    break;
                
                case 'unqualified':
                    $this->data['content'] = 'Application/Status';
                    break;
                
                case 'accepted':
                    $this->data['content'] = 'Application/Status';
                    break;
                
                default:
                    $this->data['content'] = 'Application/Forms';
                    break;
            }
        endif;
        $this->load->view('admission/Anatomy/Anatomy' , $this->data);
    }

    public function save_forms()
    {
        $userdata  = array();
        if(isset($_FILES['qualification']))
        {
            unset($_FILES['profile']);
            
            foreach($_FILES['qualification'] as $img_key => $img_values ){
                foreach($img_values as $img_key_two => $img_values_two ){
                    $_FILES['qualification'][$img_key][$img_key_two] = $img_values_two['qualification'];
                }
            }

            $path = $this->upload_files($_FILES['qualification']);
            foreach($path as $kq => $fq){
                $_POST['qualification'][$kq]['picture'] = $fq;
            }
        }

        if(isset($_FILES['profile']))
        {
            $picture = $this->do_upload('profile');
            $data['picture']         = $picture;
            //var_dump($picture);
            if($this->applicant_model->update($data , $this->session->userdata('user_id'))) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
                unset($data['picture']);
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => _l('Error occurred'), 'type' => 'error' );
            }
        }

        $counter = 1;

        foreach($_POST['qualification'] as $key => $value){
            foreach($value as $key_two => $value_two ){
                $counter++;
                if(strpos($key_two, 'grades')  !== false ){
                    preg_match_all('!\d+!', $key_two, $matches);
                    $array_number = $matches[0][0];
                    $name = str_replace($array_number, "" , $key_two);//explode('lable' , $key_two)[0];
                    $_POST['qualification'][$key]['grades'][$array_number][$name] = $value_two;
                    unset($_POST['qualification'][$key][$key_two]);
                }
            }
        } 

        $_POST['others']['qualification'] = $_POST['qualification'];
        unset($_POST['qualification']);
        
        $form = $this->input->post();
        
		if ($form) {
            $data['applicant_id']   = $this->session->userdata('user_id');
            $data['status']         = 'in_progress';
            $data['year_id']        = $this->data['within_admission']->id;

            $this->db->trans_start();
            $frm_id              = $this->input->post('frm_id');
            $data['active_form'] = $frm_id == "6" ? "5" : $frm_id;
            $data['json_values'] = json_encode($form);

            if($this->data['form_data']->status == '')
            {
                $return = $this->app_forms_models->insert($data);
            } else 
            {
                $return = $this->app_forms_models->update($data ,  (int)$this->data['form_data']->id );
            }
            $this->db->trans_complete();

            if($return) {
                echo "success";
            } else {
                echo "error";
            }
		}
    }
    

    public function submission()
    {
        $id                     = (int)$this->session->userdata('user_id');
        $data['status']         = 'submitted';
        
        if($this->app_forms_models->update($data , array('applicant_id' => $id))) {
            
            $message				=	"Thanks for completing the admission the process. We will update you on your admission status.";
            
			$this->sender->strMessage		= $message; 
            $this->sender->strMobile		= $this->data['user_details']->phone_code."".(int)$this->data['user_details']->phone;
            
            if($this->sender->Submit()){
                echo "<script>alert('Message sent to ".$this->sender->strMobile." , Thank you');</script>";
            }
            $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
        }
        redirect('admission/dashboard');
        
    }

    public function printing($id = null)
    {
        $this->load->view('admission/Application/Print' , $this->data );
    }
    
    public function acceptance()
    {
        $id                     = (int)$this->session->userdata('user_id');
        $data['status']         = 'accepted';
        
        if($this->app_forms_models->update($data , array('applicant_id' => $id))) {
            $this->data['notify'][] = array('title' => 'Success', 'message' => _l('Record successfully sent'), 'type' => 'success' );
        }
        redirect('admission/dashboard');
        
    }
}
