<?php
class Admin_Controller extends MY_Controller
{

	function __construct ()
	{
		parent::__construct();
		$this->lang->load('auth');
        $this->load->model('academics_model');
        $this->load->model('app_forms_models');
		
		// Login check


		$uris_loout = array(
			'portal/auth/logout', 
		);
		$uris_login = array(
			'portal/auth/login', 
			'portal/auth/reset', 
			'browser', 
			'portal/auth', 
			'portal', 
		);
		$uris_lock = array(
			'portal/auth/lock/account', 
			'portal/auth/lock', 
		);
        $this->data['js_show'] 		= false;

		//User Agent Properties
		$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
		$user_agent_one = explode("(",$user_agent[0]);
		$user_agent_two = explode(";",$user_agent_one[1]);


		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
			$this->data['user_agent']['browser'] = 'Internet explorer';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
			$v_count = strlen("MSIE/");
			redirect('welcome/browser');
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
			$this->data['user_agent']['browser'] = 'Mozilla Firefox';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
			$v_count = strlen("Firefox/");
			$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
			$user_agent_one = explode("(",$user_agent[0]);
			$user_agent_two = explode(";",$user_agent_one[1]);
	
	
			if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
				$this->data['user_agent']['browser'] = 'Internet explorer';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
				$v_count = strlen("MSIE/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
				$this->data['user_agent']['browser'] = 'Mozilla Firefox';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
				$v_count = strlen("Firefox/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
				$this->data['user_agent']['browser'] = 'Google Chrome';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
				$v_count = strlen("Chrome/");
			} else {
				$this->data['user_agent']['browser'] = 'Something else';
			}
			$this->data['user_agent']['browser_version'] 	= substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
			$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
			$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);
	
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
			$this->data['user_agent']['browser'] = 'Google Chrome';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
			$v_count = strlen("Chrome/");
		} else {
			$this->data['user_agent']['browser'] = 'Something else';
			redirect('welcome/browser');
		}
		$this->data['user_agent']['browser_version'] 	= (int)substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
		$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
		$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);



		//in login
		if (in_array(uri_string(), $uris_login) == TRUE) 
		{
			if ($this->ion_auth->logged_in() == TRUE) {
				//redirect("portal/dashboard");
			}
		}


		//in dashboard
		if (in_array(uri_string(), $uris_login) == FALSE) 
		{
			if ($this->ion_auth->logged_in() == FALSE) {
				redirect('portal/auth/login');
			} else {
				//load data
				$this->data['within_admission'] 	= false;
				$this->data['user_details']  		= $this->ion_auth->user($this->session->userdata('user_id'))->row();
				$data 								= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
				
				foreach($data as $single){
					$current = date('m/d/Y');
					if(( $current >= $single->start ) && ( $current <= $single->end )){
						$this->data['within_admission'] = $single;
						break;
					} else if( $current < $single->end ){
						$this->data['within_admission'] = $single;
						break;
					}
				}

				if($this->data['within_admission']){
					$this->data['stat']['in_progress']      = $this->app_forms_models->where(array('status' => "in_progress" , 'year_id' => $this->data['within_admission']->id ))->get_all() == false ? 0 : count((array)$this->app_forms_models->where(array('status' => "in_progress" , 'year_id' => $this->data['within_admission']->id ))->get_all()); 
					$this->data['stat']['submitted']        = $this->app_forms_models->where(array('status' => "submitted" , 'year_id' => $this->data['within_admission']->id ))->get_all() == false ? 0 : count((array)$this->app_forms_models->where(array('status' => "submitted" , 'year_id' => $this->data['within_admission']->id ))->get_all()); 
					$this->data['stat']['qualified']        = $this->app_forms_models->where(array('status' => "qualified" , 'year_id' => $this->data['within_admission']->id ))->get_all() == false ? 0 : count((array)$this->app_forms_models->where(array('status' => "qualified" , 'year_id' => $this->data['within_admission']->id ))->get_all()); 
					$this->data['stat']['unqualified']      = $this->app_forms_models->where(array('status' => "unqualified" , 'year_id' => $this->data['within_admission']->id ))->get_all() == false ? 0 : count((array)$this->app_forms_models->where(array('status' => "unqualified" , 'year_id' => $this->data['within_admission']->id ))->get_all());
					$this->data['stat']['accepted']         = $this->app_forms_models->where(array('status' => "accepted" , 'year_id' => $this->data['within_admission']->id ))->get_all() == false ? 0 : count((array)$this->app_forms_models->where(array('status' => "accepted" , 'year_id' => $this->data['within_admission']->id ))->get_all());
				} else {
					$this->data['stat']['in_progress']      = 0;//$this->app_forms_models->where(array('status' => "in_progress" , 'year_id' => $this->data['within_admission']->id ))->get() == false ? 0 : count($this->app_forms_models->where(array('status' => "in_progress" , 'year_id' => $this->data['within_admission']->id ))->get()); 
					$this->data['stat']['submitted']        = 0;//$this->app_forms_models->where(array('status' => "submitted" , 'year_id' => $this->data['within_admission']->id ))->get() == false ? 0 : count($this->app_forms_models->where(array('status' => "submitted" , 'year_id' => $this->data['within_admission']->id ))->get()); 
					$this->data['stat']['qualified']        = 0;//$this->app_forms_models->where(array('status' => "qualified" , 'year_id' => $this->data['within_admission']->id ))->get() == false ? 0 : count($this->app_forms_models->where(array('status' => "qualified" , 'year_id' => $this->data['within_admission']->id ))->get()); 
					$this->data['stat']['unqualified']      = 0;//$this->app_forms_models->where(array('status' => "unqualified" , 'year_id' => $this->data['within_admission']->id ))->get() == false ? 0 : count($this->app_forms_models->where(array('status' => "unqualified" , 'year_id' => $this->data['within_admission']->id ))->get());
					$this->data['stat']['accepted']         = 0;//$this->app_forms_models->where(array('status' => "accepted" , 'year_id' => $this->data['within_admission']->id ))->get() == false ? 0 : count($this->app_forms_models->where(array('status' => "accepted" , 'year_id' => $this->data['within_admission']->id ))->get());
				}

	
			}
		} 
		
		//in lock 
		if (in_array(uri_string(), $uris_lock) == FALSE) 
		{
			if ($this->ion_auth->lockin() == TRUE) {
				redirect('portal/auth/lock/account');
			}			
		}


		//setcookie("test_cookie", "test", time() + 3600, '/');
		if(count($_COOKIE) < 0) {
		    echo "<script>alert('Cookies are disabled. \n Pls enable for full funtionality')</script>";
		} else {
			//Rabbani Script
		}

	}
}