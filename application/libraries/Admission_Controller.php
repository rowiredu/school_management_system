<?php
class Admission_Controller extends MY_Controller
{

	function __construct ()
	{
		parent::__construct();
		$this->lang->load('auth');
		$this->load->model('country_models');
		$this->load->model('course_models');
        $this->load->model('app_forms_models');
        $this->load->model('academics_model');
		
		$this->data['countries']  = $this->country_models->get_all();
		$this->data['courses']	  = $this->course_models->get_all();

		// Login check
		$uris_logout = array(
			'admission/auth/logout', 
			'admission/auth/reset_password', 
		);
		$uris_login = array(
			'admission/auth/login', 
			'admission/auth/register',
			'admission/auth/reset', 
			'admission/auth/forgot_password', 
			'admission/auth/reset_password', 
			'browser', 
			'admission/auth', 
			'admission', 
		);
		$uris_lock = array(
			'admission/auth/lock/account', 
			'admission/auth/lock', 
			'admission/auth/reset_password', 
		);
		//User Agent Properties
		$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
		$user_agent_one = explode("(",$user_agent[0]);
		$user_agent_two = explode(";",$user_agent_one[1]);


		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
			$this->data['user_agent']['browser'] = 'Internet explorer';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
			$v_count = strlen("MSIE/");
			redirect('welcome/browser');
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
			$this->data['user_agent']['browser'] = 'Mozilla Firefox';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
			$v_count = strlen("Firefox/");
			$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
			$user_agent_one = explode("(",$user_agent[0]);
			$user_agent_two = explode(";",$user_agent_one[1]);
	
	
			if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
				$this->data['user_agent']['browser'] = 'Internet explorer';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
				$v_count = strlen("MSIE/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
				$this->data['user_agent']['browser'] = 'Mozilla Firefox';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
				$v_count = strlen("Firefox/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
				$this->data['user_agent']['browser'] = 'Google Chrome';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
				$v_count = strlen("Chrome/");
			} else {
				$this->data['user_agent']['browser'] = 'Something else';
			}
			$this->data['user_agent']['browser_version'] 	= substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
			$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
			$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);
	
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
			$this->data['user_agent']['browser'] = 'Google Chrome';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
			$v_count = strlen("Chrome/");
		} else {
			$this->data['user_agent']['browser'] = 'Something else';
			redirect('welcome/browser');
		}
		$this->data['user_agent']['browser_version'] 	= (int)substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
		$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
		$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);



		//in login
		if (in_array(uri_string(), $uris_login) == TRUE) 
		{
			if ($this->stu_auth->logged_in() == TRUE) {
				redirect("admission/dashboard");
			}
		}


		//in dashboard
		if (in_array(uri_string(), $uris_login) == FALSE) 
		{
			if ($this->stu_auth->logged_in() == FALSE) {
				redirect('admission/auth/login');
			} else {
    			
				//load data $this->ion_auth->user($id)->row();
				$this->data['user_details']  = $this->stu_auth->user($this->session->userdata('user_id'))->row();
				$this->data['ind_prop']  = json_decode($this->data['user_details']->json_encoded_prefield);
				$this->data['user_picture'] = $this->data['user_details']->picture == '' ? base_url('assets/default.jpg') :  base_url($this->data['user_details']->picture) ;


				$data 		= $this->academics_model->get_all() == false ? array() :  $this->academics_model->get_all();
				$this->data['within_admission'] = false;
				foreach($data as $single){
					$current = date('m/d/Y');
					if(( $current >= $single->start ) && ( $current <= $single->end )){
						$this->data['within_admission'] = $single;
						break;
					} else if( $current < $single->end ){
						$this->data['within_admission'] = $single;
						break;
					}
				}
				//Load appropirate Sample
				if($this->data['within_admission'] !== false){
					$this->data['form_data'] = $this->app_forms_models->where(array('applicant_id' => $this->session->userdata('user_id') , 'year_id' => $this->data['within_admission']->id ))->get(); 
					if($this->data['form_data'] == false){
						$this->data['form_data'] = new stdClass();
						$this->data['form_data']->status = '';
						$this->data['form_data']->payment_id = '';
					}

				}



        
			}
		} 
		
		//in lock 
		if (in_array(uri_string(), $uris_lock) == FALSE)
		{
			if ($this->stu_auth->lockin() == TRUE) {
				redirect('admission/auth/lock/account');
			}			
		}


		//setcookie("test_cookie", "test", time() + 3600, '/');
		if(count($_COOKIE) < 0) {
		    echo "<script>alert('Cookies are disabled. \n Pls enable for full funtionality')</script>";
		}

	}
}