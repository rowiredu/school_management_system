<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function add_meta_title ($string)
{
	$CI =& get_instance();
	$CI->data['meta_title'] = e($string) . ' - ' . $CI->data['meta_title'];
}

function un_btn ($uri , $info = null)
{
	return anchor($uri, '<i title="'.$info.'" class="material-icons"> '.$info.' </i>' , 'class="btn-flat btn-small waves-effect"');
}


function un_btn_activate ($status , $uri , $info = null)
{
	if ($status == 'Active') {
		$hover_status = 'Published';
		$color= '#ea454b !important';
	}
	else
	{
		$hover_status = 'Unpublished';
		$color= '#c3b1e0 !important';
	}

	return anchor($uri, '<i title="Status:'.$hover_status.'" style="color:'.$color.';" class="material-icons" > '.$info.' </i>' , 'class="btn-flat btn-small waves-effect"');
}


function un_btn_delete ($uri , $info = null)
{
	return anchor($uri, '<i  title="Delete" class="material-icons"> delete </i>', array(
		'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');",
		'class' => "btn-flat btn-small waves-effect"
	));
}

function btn_edit ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Edit" class="fa fa-pencil-square-o"> '.$info.' </i>');
}

function flashdata ()
{
	if (isset($_SESSION['error'])) {
		?>
			<script type="text/javascript">
				// Materialize.toast(message, displayLength, className, completeCallback);
				Materialize.toast('<?php echo $_SESSION["error"]; ?>', 4000) // 4000 is the duration of the toast
			</script>
		<?php
	}
}

function btn_order ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Sort" class="fa fa-sort"> '.$info.' </i>');
}
function btn_medea ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Gallery" class="fa fa-picture-o"> '.$info.' </i>');
}


function btn_bullets ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Bulletings forms" class="fa fa-th-large"> '.$info.' </i>');
}

function btn_bullets_home ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Bulletings forms" class="fa fa-cogs"> '.$info.' </i>');
}

function btn_activate ($status , $uri , $info = null)
{
	if ($status == 'Active') {
		$hover_status = 'Published';
		$color= '#fd7b12';
	}
	else
	{
		$hover_status = 'Unpublished';
		$color= '#adad88';
	}

	return anchor($uri, '<i style="padding-left:10px;padding-right:10px; color:'.$color.';" class="fa fa-star" title="Status:'.$hover_status.'"> '.$info.' </i>');
}


function btn_add ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Add" class="fa fa-plus"> '.$info.' </i>');
}

function btn_view ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="view" class="fa fa-eye"> '.$info.' </i>');
}

function btn_delete ($uri , $info = null)
{
	return anchor($uri, '<i style="padding-left:10px;padding-right:10px;" title="Delete" class="fa fa-trash-o"> '.$info.' </i>', array(
		'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
	));
}

function article_link($article){
	return 'article/' . intval($article->id) . '/' . e($article->slug);
}
function article_links($articles){
	$string = '<ul>';
	foreach ($articles as $article) {
		$url = article_link($article);
		$string .= '<li>';
		$string .= '<h3>' . anchor($url, e($article->title)) .  ' ›</h3>';
		$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
		$string .= '</li>';
	}
	$string .= '</ul>';
	return $string;
}

function get_excerpt($article, $numwords = 50){
	$string = '';
	$url = article_link($article);
	$string .= '<h2>' . anchor($url, e($article->title)) .  '</h2>';
	$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
	$string .= '<p>' . e(limit_to_numwords(strip_tags($article->body), $numwords)) . '</p>';
	$string .= '<p>' . anchor($url, 'Read more ›', array('title' => e($article->title))) . '</p>';
	return $string;
}

function limit_to_numwords($string, $numwords){
	$excerpt = explode(' ', $string, $numwords + 1);
	if (count($excerpt) >= $numwords) {
		array_pop($excerpt);
	}
	$excerpt = implode(' ', $excerpt);
	return $excerpt;
}

function e($string){
	return htmlentities($string);
}

function get_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';
	
	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;
		
		foreach ($array as $item) {
			
			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">' : '<li class="dropdown">';
				$str .= '<a  class="dropdown-toggle" data-toggle="dropdown" href="' . site_url(e($item['slug'])) . '">' . e($item['title']);
				$str .= '<b class="caret"></b></a>' . PHP_EOL;
				$str .= get_menu($item['children'], TRUE);
			}
			else {
				$str .= $active ? '<li class="active">' : '<li>';
				$str .= '<a href="' . site_url($item['slug']) . '">' . e($item['title']) . '</a>';
			}
			$str .= '</li>' . PHP_EOL;
		}
		
		$str .= '</ul>' . PHP_EOL;
	}
	
	return $str;
}

/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        
        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
        
        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}


if (!function_exists('dump_exit')) {
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}