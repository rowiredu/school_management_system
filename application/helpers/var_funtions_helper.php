<?php
function self_humanize ($string)
{
	$string = str_replace('_', ' ', $string);
	return ucfirst($string);
}

function obj_converter ($array)
{
	$obj = new stdClass();
		foreach ($array as $key => $value) {
			$obj->$key = $value;
		}
	return $obj;
}

function array_converter ($array)
{
	$obj = array();
		foreach ($array as $key => $value) {
			$obj[$key] = $value;
		}
	return $obj;
}

function obj_cleaner ($array)
{
	foreach( $array as $key => $value )
	{
	    if( empty($value) || $value === "")
	    {
	       unset( $array->$key );
	    }
	}
	return $array;
}

function array_cleaner($array)
{
	foreach($array as $key => $value)
	{
	    if( empty($value) || $value == "")
	    {
	       unset($array[$key]);
	    }
	}
	return $array;
}

function empty_array($array = null)
{
	$counter	=	0;
	if ($array != null) {
		foreach($array as $key => $value)
		{
		    $value == "" ? "" : $counter++ ;
		}
	}
	return $counter > 0 ? true : false ;
}



function is_multi_array($values) {

     if (is_array($values)) 
     {
	    foreach ($values as $value) {
	        if (is_array($value)) {
	        	return true;
	        }
	    }
	}
    return false;
}


function rmc($data) {

    return str_replace(",","",$data); 
}


function unique_code_gen() {
	$CI =& get_instance();

	$code = $CI->session->userdata('staff_number');
	$t = microtime(true);
	$micro = sprintf("%06d",($t - floor($t)) * 1000000);
	$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	return clean($code)."".$d->format("YmdHisu"); // note at point on "u"
}

function clean($string) {
	$string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}