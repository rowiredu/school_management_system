<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function status_presenter($status) {
    switch ($status) {
        case NULL:
        case 'Pending':
            echo '<span class="label label-purple">Work in Progress</span>';
            break;
        case 'Approve':
        case 'Accept':
            echo '<span class="label label-success">Approved</span>';
            break;
        case 'Reject':
        case 'Rejected':
            echo '<span class="label label-danger">Rejected by Administrator </span>';
            break;
        case 'hold':
            echo '<span class="label label-warning"> On hold </span>';
            break;
        case 'declined':
            echo '<span class="label label-purple">Rejected by Administrator </span>';
            break;
        case 'Verify':
        case 'Verified':
            echo '<span class="label label-warning">Verified for Processing</span>';
            break;        
        default:
            echo '<span class="label label-warning">' .$status.'</span>';
            break;
    }
}
function dash_json_presenter($data) {
    //dump($data);  

    $count_img=1;
    foreach (json_decode($data) as $json_key => $json_value) { 
        if (!strpos($json_key, '_image')) { 
            if (!strpos($json_key, '_picture')) {
                if ($count_img > 2) {
                    break; 
                } 
                ?> <span class="name"><?=humanize($json_key)?> :: <?=humanize($json_value)?></span><br /> <?php
                $count_img++;
            } else {
                echo '<span class="name">Picture Data</span>';
            }
        }
    } 
}
function dash_img_presenter($data) {
    $count_img=1;
    foreach (json_decode($data) as $json_key => $json_value) { 
        if (strpos($json_key, '_image') || strpos($json_key, '_picture')) { 
            if ($count_img > 2) {
                ?><div class="more_background right" style="background: url(<?=base_url($json_value)?>);" >
                    <div class="more_transbox">
                    <p>
                        <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="color_white" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">view more....</a>
                    </p>
                    </div>
                </div><?php
                break; 
            } 
            ?><a href="<?=base_url($json_value)?>" class="image-popup user-list-item" title="Proof">
                <img src="<?=base_url($json_value)?>" class=" sm_image img-circle" alt="profile-image">
            </a><?php
            $count_img++;
        }
    } 

    if ($count_img <= 1) {
        ?><i class="ti-arrow-right"></i><?php
    }
}
function Requests($data) { 

    ?>
    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Total Requests</h4>

            <div class="widget-chart-1">
                <div class="widget-chart-box-1">
                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                        data-bgColor="#F9B9B9" value="<?=count($public_user_bio_all)?>"
                        data-skin="tron" data-angleOffset="180" data-readOnly=true
                        data-thickness=".15"/>
                </div>

                <div class="widget-detail-1">
                    <h2 class="p-t-10 m-b-0"> <?=count($public_user_bio_today)?> </h2>
                    <p class="text-muted">Requests today</p>
                </div>
            </div>
        </div>
    </div><!-- end col -->
    <?php
}
function Loans($data) {
    ?>

    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30"> Loans</h4>

            <div class="widget-box-2">
                <div class="widget-detail-2">
                    <span class="badge badge-success pull-left m-t-20">32% <i
                            class="zmdi zmdi-trending-up"></i> </span>
                    <h2 class="m-b-0"> <?=dump(json_decode($loans))?> </h2>
                    <p class="text-muted m-b-25">loans </p>
                </div>
                <div class="progress progress-bar-success-alt progress-sm m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar"
                        aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"
                        style="width: 77%;">
                        <span class="sr-only">77% Complete</span>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end col -->
    <?php
}
function Statistics($data) {
    ?>

    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Statistics</h4>

            <div class="widget-chart-1">
                <div class="widget-chart-box-1">
                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                        data-bgColor="#FFE6BA" value="80"
                        data-skin="tron" data-angleOffset="180" data-readOnly=true
                        data-thickness=".15"/>
                </div>
                <div class="widget-detail-1">
                    <h2 class="p-t-10 m-b-0"> 4569 </h2>
                    <p class="text-muted"> today</p>
                </div>
            </div>
        </div>
    </div><!-- end col -->
    <?php
}
function Provident($data) {
    ?>

    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Provident Funds</h4>

            <div class="widget-box-2">
                <div class="widget-detail-2">
                    <span class="badge badge-pink pull-left m-t-20">32% <i
                            class="zmdi zmdi-trending-up"></i> </span>
                    <h2 class="m-b-0"> 158 </h2>
                    <p class="text-muted m-b-25">withdrawable </p>
                </div>
                <div class="progress progress-bar-pink-alt progress-sm m-b-0">
                    <div class="progress-bar progress-bar-pink" role="progressbar"
                        aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"
                        style="width: 77%;">
                        <span class="sr-only">77% Complete</span>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end col -->
    <?php
}





?>




