<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function _nav ($url)
{
  foreach ($url as $fkey => $flink) {
      ?>
      <li class="nav-item start  open">
          <a href="<?php echo is_array($flink) ? 'javascript:;' : site_url('control/'.$flink) ?>" class="nav-link nav-toggle">
              <?php 
                  $fkey = explode("|", $fkey);
                  echo '<i class="'.$fkey[0].'"></i>';
                  if (is_array($flink)) {
                      echo '<span class="arrow open"></span>';
                  } else if (uri_string() == $flink) {
                      echo '<span class="selected"></span>';
                  }
                  echo '<span class="title">'.$fkey[1].'</span>';
              ?>
          </a>
          <?php if (is_array($flink)) { ?>
            <ul class="sub-menu">
              <?php foreach ($flink as $skey => $slink) { ?>
                <li class="nav-item  ">
                  <a href="<?php echo is_array($slink) ? 'javascript:;' : $slink ?>" class="nav-link nav-toggle">
                      <?php
                          $skey = explode("|", $skey);
                          echo '<i class="'.$skey[0].'"></i>';
                          if (is_array($slink)) {
                              echo '<span class="arrow open"></span>';
                          } else if (uri_string() == $slink) {
                              echo '<span class="selected"></span>';
                          }
                          echo '<span class="title">'.$skey[1].'</span>';
                      ?>
                  </a>
                  <?php if (is_array($slink)) { ?>
                    <ul class="sub-menu">
                      <?php foreach ($slink as $tkey => $tlink) { ?>
                        <li class="nav-item  ">
                          <a href="<?php echo is_array($tlink) ? 'javascript:;' : $tlink ?>" class="nav-link nav-toggle">
                              <?php
                                  $tkey = explode("|", $tkey);
                                  echo '<i class="'.$tkey[0].'"></i>';
                                  if (is_array($tlink)) {
                                      echo '<span class="arrow open"></span>';
                                  } else if (uri_string() == $tlink) {
                                      echo '<span class="selected"></span>';
                                  }
                                  echo '<span class="title">'.$tkey[1].'</span>';
                              ?>
                          </a>
                        </li>
                      <?php } ?>
                    </ul>
                  <?php } ?>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
      </li>
      <?php
  }
}
function _nav_edit_lable ($control_center)
{
      echo form_open(null , array('role' => 'form')); ?>

      <?php echo validation_errors() ? "<h5>Error</h5>".validation_errors('<p><code class="language-markup" style="color:red !important;">', '</code></p>') : "" ; ?>
      <div class="form-body">
          <?php $countmania = 0; ?>
          <?php $countmini = 0; ?>
          <?php foreach (unserialize($control_center->uri) as $key => $value) { ?>
              <?php $key = explode("|", $key); ?>
              <?php if (is_array($value)) { ?>
                  <div class="form-group form-md-line-input form-md-floating-label has-info col-md-6" style="background: #e1e5ec !important; ">
                      <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][font]"; ?>" value="<?php echo $key[0]; ?>">
                      <label for="font_awsome"><i style="padding-left: 2em; ">Font Icon Class</i></label>
                      <span class="help-block">Font Awsome ...E.g. "fa fa-plus"</span>
                  </div>
                  <div class="form-group form-md-line-input form-md-floating-label has-info col-md-6" style="background: #e1e5ec !important; ">
                      <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][title]"; ?>" value="<?php echo $key[1]; ?>">
                      <label for="font_awsome"><i style="padding-left: 2em; ">Name</i></label>
                      <span class="help-block">name ...E.g. "title"</span>
                  </div>
                  <?php foreach ($value as $fakey => $favalue) { ?>
                    <?php $fakey = explode("|", $fakey); ?>
                      <div>
                          <div style="padding: 1px 1px 1px 1px; height: 100%; ">
                              <div class=" col-md-1" >
                                  <img src="https://cdn0.iconfinder.com/data/icons/arrows-android-l-lollipop-icon-pack/24/down_right-512.png" class="dvs">
                              </div>
                              <div class="form-group form-md-line-input form-md-floating-label has-info col-md-3" style="background: #e1e5ec !important; ">
                                  <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][uri][{$countmini}][font]"; ?>" value="<?php echo $fakey[0]; ?>">
                                  <label for="font_awsome"><i style="padding-left: 2em; ">Font Icon Class</i></label>
                                  <span class="help-block">Font Awsome ...E.g. "fa fa-plus"</span>
                              </div>
                              <div class="form-group form-md-line-input form-md-floating-label has-info col-md-4" style="background: #e1e5ec !important; ">
                                  <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][uri][{$countmini}][title]"; ?>" value="<?php echo $fakey[1];?>">
                                  <label for="font_awsome"><i style="padding-left: 2em; ">Name</i></label>
                                  <span class="help-block">name ...E.g. "title"</span>
                              </div>
                              <div class="form-group form-md-line-input form-md-floating-label has-info col-md-4" style="background: #e1e5ec !important; ">
                                  <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][uri][{$countmini}][uri]"; ?>" value="<?php echo $favalue;?>">
                                  <label for="font_awsome"><i style="padding-left: 2em; ">Uniform_Resource_Locator</i></label>
                                  <span class="help-block">uri ...E.g. "control/anatomy/anatomy"</span>
                              </div>
                          </div>
                      </div>
                  <?php $countmini++; ?>
              <?php } ?>
              <?php } else { ?>
                  <div style="padding: 1px 1px -10px 1px; height: 100%;">
                      <div class="form-group form-md-line-input form-md-floating-label has-info col-md-4" style="background: #e1e5ec !important; ">
                          <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][font]"; ?>;" value="<?php echo $key[0]; ?>">
                          <label for="font_awsome"><i style="padding-left: 2em; ">Font Icon Class</i></label>
                          <span class="help-block">Font Awsome ...E.g. "fa fa-plus"</span>
                      </div>
                      <div class="form-group form-md-line-input form-md-floating-label has-info col-md-4" style="background: #e1e5ec !important; ">
                          <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][title]"; ?>" value="<?php echo $key[1];?>">
                          <label for="font_awsome"><i style="padding-left: 2em; ">Name</i></label>
                          <span class="help-block">name ...E.g. "title"</span>
                      </div>
                      <div class="form-group form-md-line-input form-md-floating-label has-info col-md-4" style="background: #e1e5ec !important; ">
                          <input type="text" class="form-control" id="font_awsome" name="<?php echo "uri[{$countmania}][uri]"; ?>" value="<?php echo $value;?>">
                          <label for="font_awsome"><i style="padding-left: 2em; ">Uniform_Resource_Locator</i></label>
                          <span class="help-block">uri ...E.g. "control/anatomy/anatomy"</span>
                      </div>
                  </div>
                  <?php $countmania++; ?>
              <?php } ?>
          <?php } ?>
      </div>
      <div class="form-actions noborder">
          <?php echo form_submit(null , 'Submit Post!', array("class" => "btn blue")); ?>
          <button type="button" class="btn default">Cancel</button>
      </div>
  <?php echo form_close();
}
function _navgadd ($url)
{
  foreach ($url as $fkey => $flink) {
      ?>
      <li class="nav-item start  open">
          <a href="<?php echo is_array($flink) ? 'javascript:;' : site_url('control/'.$flink) ?>" class="nav-link nav-toggle">
              <?php 
                  $fkey = explode("|", $fkey);
                  echo '<i class="'.$fkey[0].'"></i>';
                  if (is_array($flink)) {
                      echo '<span class="arrow open"></span>';
                  } else if (uri_string() == $flink) {
                      echo '<span class="selected"></span>';
                  }
                  echo '<span class="title">'.$fkey[1].'</span>';
              ?>
          </a>
          <?php if (is_array($flink)) { ?>
            <ul class="sub-menu">
              <?php foreach ($flink as $skey => $slink) { ?>
                <li class="nav-item  ">
                  <a href="<?php echo is_array($slink) ? 'javascript:;' : $slink ?>" class="nav-link nav-toggle">
                      <?php
                          $skey = explode("|", $skey);
                          echo '<i class="'.$skey[0].'"></i>';
                          if (is_array($slink)) {
                              echo '<span class="arrow open"></span>';
                          } else if (uri_string() == $slink) {
                              echo '<span class="selected"></span>';
                          }
                          echo '<span class="title">'.$skey[1].'</span>';
                      ?>
                  </a>
                  <?php if (is_array($slink)) { ?>
                    <ul class="sub-menu">
                      <?php foreach ($slink as $tkey => $tlink) { ?>
                        <li class="nav-item  ">
                          <a href="<?php echo is_array($tlink) ? 'javascript:;' : $tlink ?>" class="nav-link nav-toggle">
                              <?php
                                  $tkey = explode("|", $tkey);
                                  echo '<i class="'.$tkey[0].'"></i>';
                                  if (is_array($tlink)) {
                                      echo '<span class="arrow open"></span>';
                                  } else if (uri_string() == $tlink) {
                                      echo '<span class="selected"></span>';
                                  }
                                  echo '<span class="title">'.$tkey[1].'</span>';
                              ?>
                          </a>
                        </li>
                      <?php } ?>
                    </ul>
                  <?php } ?>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
      </li>
      <?php
  }
}