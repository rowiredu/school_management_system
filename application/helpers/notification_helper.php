<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function notification ( $notify )
{
  return '
          <script type="text/javascript">
              
              // Display an info toast with no title
              toastr.options = {
                "closeButton": true,
                "debug": true,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000000",
                "timeOut": "7000000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              }
              toastr.'.$notify['type'].'("'.$notify['message'].'" , "'.$notify['title'].'");
          </script>
  ';
}

?>
