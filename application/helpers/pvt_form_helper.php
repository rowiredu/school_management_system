<?php
function switches ($array )
{

	foreach ($array as $key => $value) { ?>
		<!-- Switch -->
            <div class="row">
                <!-- Switch -->
                <label for="<?php echo $key; ?>"><?php echo self_humanize($key); ?></label>
                <div class="switch">
                    <label>
                        deactivate
                        <input type="checkbox" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="Activate" <?php echo $value != "Activate" ? "" :  "checked" ?> >
                        <span class="lever"></span>
                        Activate
                    </label>
                </div>
                <span class="active" id="<?php echo $key; ?>"></span>
                <!-- End Switch -->
            </div>
	<?php }
  }


function privileges ($array = null , $post = null ){
	$arrayprivileges = array(
		'dashboard' 				=> 		"", 
		'direct_website' 			=> 		"", 
		'live_support' 				=> 		"", 
		'messages' 					=> 		"", 
		'client_dropdown' 			=> 		"", 
			'client_directory' 		=> 		"", 
			'criticism'				=> 		"", 
			'criticism_divisions' 	=> 		"", 
		'directive_dropdown' 		=> 		"", 
			'bullets' 				=> 		"", 
			'testimonails' 			=> 		"", 
			'price_tag' 			=> 		"", 
			'price_tag_category' 	=> 		"", 
			'portfolio' 			=> 		"", 
			'portfolio_category' 	=> 		"", 
			'page' 					=> 		"", 
			'page_cat' 				=> 		"", 
			'hr_staff' 				=> 		"", 
			'hr_department' 		=> 		"", 
			'sliders' 				=> 		"", 
		'managers_dropdown' 		=> 		"", 
			'news' 					=> 		"", 
			'news_categorizing' 	=> 		"", 
			'article' 				=> 		"", 
			'events' 				=> 		"", 
			'user' 					=> 		"", 
			'multimedia' 			=> 		"", 
		'preview_dropdown' 			=> 		"", 
			'HD' 					=> 		"", 
			'mobile' 				=> 		"", 
		'settings_dropdown' 		=> 		"", 
			'settings' 				=> 		"", 
			'partner' 				=> 		"", 
			'affiliates' 			=> 		"", 
			'advertisement' 		=> 		"", 
		'username_dropdown'	 		=> 		"", 
			'logs' 					=> 		"", 
			'edit' 					=> 		"", 
	);
	if ($array != null) {
		$arrayprivileges = array_replace($arrayprivileges, $array );
	} 

	if ($post != null) {
		$arrayprivileges = array_replace($arrayprivileges , $post );
	} 
	
	return $arrayprivileges;
}

function formgroups ($db_array = null )
{
	if (isset($db_array['portfolio_category_id'])) {
		$portfolio_category_id_array = array('"disabled selected"' => 'Choose category');
		foreach ($db_array['portfolio_category_id'] as $key => $value) {
			$portfolio_category_id_array[$value->id] = $value->name;
		}
	}
	if (isset($db_array['news_categorizing_id'])) {
		$news_categorizing_id_array = array('"disabled selected"' => 'Choose category');
		foreach ($db_array['news_categorizing_id'] as $key => $value) {
			$news_categorizing_id_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['naggers_id'])) {
		$naggers_id_array = array('"disabled selected"' => 'Choose user');
		foreach ($db_array['naggers_id'] as $key => $value) {
			$naggers_id_array[$value->id] = $value->full_name;
		}
	}
	if (isset($db_array['complaints_division_id'])) {
		$complaints_division_id_array = array('"disabled selected"' => 'Choose category');
		foreach ($db_array['complaints_division_id'] as $key => $value) {
			$complaints_division_id_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['page_id'])) {
		$page_id_array = array('' => 'Home Page');
		foreach ($db_array['page_id'] as $key => $value) {
			$page_id_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['self_id'])) {
		$self_id_array = array('' => 'Main Title');
		foreach ($db_array['self_id'] as $key => $value) {
			$self_id_array[$value->id] = $value->parent_brief;
		}
	}
	if (isset($db_array['self_id_division'])) {
		$self_id_division_array = array('' => 'Parent Division');
		foreach ($db_array['self_id_division'] as $key => $value) {
			$self_id_division_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['department_id'])) {
		$department_id_array = array('' => 'Choose Corporate Department');
		foreach ($db_array['department_id'] as $key => $value) {
			$department_id_array[$value->id] = $value->name;
		}
	}
	if (isset($db_array['parent_id'])) {
		$parent_id_array = array('Main menu' => 'Main menu');
		foreach ($db_array['parent_id'] as $key => $value) {
			$parent_id_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['subcat_id'])) {
		$subcat_id_array = array('' => 'Choose Navigation Divitions');
		foreach ($db_array['subcat_id'] as $key => $value) {
			$subcat_id_array[$value->id] = $value->title;
		}
	}
	if (isset($db_array['product_category_id'])) {
		$product_category_id_array = array('' => 'Choose Product Divitions');
		foreach ($db_array['product_category_id'] as $key => $value) {
			$product_category_id_array[$value->id] = $value->name;
		}
	}
	if (isset($db_array['staff_in_charge_id'])) {
		$staff_in_charge_id_array = array('' => 'Choose Staff');
		foreach ($db_array['staff_in_charge_id'] as $key => $value) {
			$staff_in_charge_id_array[$value->id] = $value->name;
		}
	}
	if (isset($db_array['location'])) {
		$location_array = array('"disabled selected"' => 'Choose Location');
		foreach ($db_array['location'] as $key => $value) {
			$location_array[$value->id] = $value->name;
		}
	}

	return array
	( 
		'id' => null,
		'naggers_id' => null,
		'rights' => null,
		'user_id' => null,
		'password' => null,
		'transfer_status' => null,
		'client_id' => null,
		'key' => null,
		'alpha' => null,
		'company_id' => null,
		'time_stamp' => null,
		'created' => null,
		'modified' => null,
		'video' => array(
				'label' => 'Video',
				'id' => 'video',
				'name' => 'video',
				'type' => 'file',
				'placeholder' => 'Insert video File'
			),
		'photo' => array(
				'label' => 'Picture',
				'id' => 'photo',
				'name' => 'photo',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'web_favicon_icon' => array(
				'label' => 'Browser Icon',
				'id' => 'web_favicon_icon',
				'name' => 'web_favicon_icon',
				'type' => 'file',
				'placeholder' => 'Insert Browser Icon'
			),
		'picture' => array(
				'label' => 'Picture',
				'id' => 'picture',
				'name' => 'picture',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'pro_formal_invoice' => array(
				'label' => 'Pro Formal Invoice',
				'id' => 'pro_formal_invoice',
				'name' => 'pro_formal_invoice',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'cover_picture' => array(
				'label' => 'Cover Picture',
				'id' => 'cover_picture',
				'name' => 'cover_picture',
				'type' => 'file',
				'placeholder' => 'Insert Cover Pictire File'
			),
		'image' => array(
				'label' => 'Image Representitive',
				'id' => 'image',
				'name' => 'image',
				'type' => 'file',
				'placeholder' => 'Insert Image Representitive'
			),
		'picture_two' => array(
				'label' => 'Second Picture',
				'id' => 'picture_two',
				'name' => 'picture_two',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'download_file' => array(
				'label' => 'Download File',
				'id' => 'download_file',
				'name' => 'download_file',
				'type' => 'file',
				'placeholder' => 'Insert download File'
			),
		'background_photo' => array(
				'label' => 'Background Picture',
				'id' => 'background_photo',
				'name' => 'background_photo',
				'type' => 'file',
				'placeholder' => 'Insert Background photo'
			),
		'logo' => array(
				'label' => 'Logo',
				'id' => 'logo',
				'name' => 'logo',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'certification_document' => array(
				'label' => 'Certification PDF Document',
				'id' => 'certification_document',
				'name' => 'certification_document',
				'type' => 'file',
				'placeholder' => 'Insert Certification PDF Document'
			), 
		'company_logo' => array(
				'label' => 'Company Logo',
				'id' => 'company_logo',
				'name' => 'company_logo',
				'type' => 'file',
				'placeholder' => 'Insert Company Logo'
			), 
		'house_picture' => array(
				'label' => 'House Picture',
				'id' => 'house_picture',
				'name' => 'house_picture',
				'type' => 'file',
				'placeholder' => 'Insert Pictire File'
			),
		'slug' => array(
				'label' => 'Slogan',
				'id' => 'slug',
				'name' => 'slug',
				'type' => 'text',
				'placeholder' => 'Slogan'
			), 
		'website' => array(
				'label' => 'Website',
				'id' => 'website',
				'name' => 'website',
				'type' => 'url',
				'placeholder' => 'Enter Web Address with http:// eg: http://lustergh.com/'
			),
		'icon' => array(
				'label' => 'Font-awsome icon',
				'id' => 'icon',
				'name' => 'icon',
				'type' => 'text',
				'placeholder' => 'Please Visit Fontawsome.com to get code for more'
			),
		'video_source' => array(
				'label' => 'Video Source',
				'id' => 'video_source',
				'name' => 'video_source',
				'type' => 'url',
				'placeholder' => 'Enter Url Vedeo Address with http:// eg: https://www.youtube.com/embed/Z9bqIYbDuns'
			),
		'dob' => array(
				'label' => 'Date Of Birth',
				'id' => 'dob',
				'name' => 'dob',
				'type' => 'date',
				'placeholder' => 'Insert Date of Birth'
			),
		'pubdate' => array(
				'label' => 'Date Of Publication',
				'id' => 'pubdate',
				'name' => 'pubdate',
				'type' => 'date',
				'placeholder' => 'Insert Date of Publication'
			),
		'eventdate' => array(
				'label' => 'Date Of Event',
				'id' => 'eventdate',
				'name' => 'eventdate',
				'type' => 'date',
				'placeholder' => 'Insert Date of Event'
			),
		'comment' => array(
				'label' => 'Comment',
				'id' => 'comment',
				'name' => 'comment',
				'type' => 'textarea',
				'placeholder' => 'Enter Comment'
			),
		'memo' => array(
				'label' => 'memo',
				'id' => 'memo',
				'name' => 'memo',
				'type' => 'textarea',
				'placeholder' => 'Enter memo'
			),
		'privacy' => array(
				'label' => 'Privicy',
				'id' => 'WUSIWUGA',
				'name' => 'privacy',
				'type' => 'textarea',
				'placeholder' => 'Enter Privicy'
			),
		'licenses' => array(
				'label' => 'Licenses',
				'id' => 'WUSIWUGS',
				'name' => 'licenses',
				'type' => 'textarea',
				'placeholder' => 'Enter Licenses'
			),
		'terms_and_agreement' => array(
				'label' => 'Terms &amp; Agreement',
				'id' => 'WUSIWUGG',
				'name' => 'terms_and_agreement',
				'type' => 'textarea',
				'placeholder' => 'Enter Terms &amp; Agreement'
			),
		'copyright_policy' => array(
				'label' => 'Copyright Policy',
				'id' => 'WUSIWUG',
				'name' => 'copyright_policy',
				'type' => 'textarea',
				'placeholder' => 'Enter Copyright Policy'
			),
		'mail_policy' => array(
				'label' => 'Mail Policy',
				'id' => 'WUSIWUGF',
				'name' => 'mail_policy',
				'type' => 'textarea',
				'placeholder' => 'Enter Mail Policy'
			),
		'address' => array(
				'label' => 'Address',
				'id' => 'WUSIWUGD',
				'name' => 'address',
				'type' => 'textarea',
				'placeholder' => 'Enter Address'
			),
		'resolved_description' => array(
				'label' => 'Resolved Description',
				'id' => 'resolved_description',
				'name' => 'resolved_description',
				'type' => 'textarea',
				'placeholder' => 'Enter Description'
			),
		'origin' => array(
				'label' => 'Beginning Year of Company',
				'id' => 'origin',
				'name' => 'origin',
				'type' => 'text',
				'placeholder' => 'Starting of company'
			),
		'parent_brief' => array(
				'label' => 'Parent Brief',
				'id' => 'WUSIWUGS',
				'name' => 'parent_brief',
				'type' => 'textarea',
				'placeholder' => 'Parent Brief'
			),
		'opening_times' => array(
				'label' => 'Opening Times',
				'id' => 'opening_times',
				'name' => 'opening_times',
				'type' => 'textarea',
				'placeholder' => 'Opening Times'
			),
		'bio' => array(
				'label' => 'Biography',
				'id' => 'WUSIWUGA',
				'name' => 'bio',
				'type' => 'textarea',
				'placeholder' => 'Biography'
			),
		'description' => array(
				'label' => 'Description',
				'id' => 'WUSIWUGA',
				'name' => 'description',
				'type' => 'textarea',
				'placeholder' => 'Discription'
			),
		'plugin_ads' => array(
				'label' => 'Plugin Ads',
				'id' => 'plugin_ads',
				'name' => 'plugin_ads',
				'type' => 'textarea',
				'placeholder' => 'plugin_ads'
			),
		'body' => array(
				'label' => 'Body',
				'id' => 'WUSIWUGA',
				'name' => 'body',
				'type' => 'textarea',
				'placeholder' => 'Body'
			),
		'gps' => array(
				'label' => 'Global Positioning System',
				'id' => 'map',
				'name' => 'gps',
				'type' => 'text',
				'placeholder' => 'Choose Global Positioning System'
			),
		'email' => array(
				'label' => 'Electronic Mail',
				'id' => 'email',
				'name' => 'email',
				'type' => 'email',
				'placeholder' => 'Enter Your E-Mail'
			),
		'portfolio_category_id' => array(
				'label' => 'Portfolio Category',
				'id' => 'portfolio_category_id',
				'name' => 'portfolio_category_id',
				'type' => 'select',
				'placeholder' => 'Choose category',
				'options' => isset($portfolio_category_id_array) ?  $portfolio_category_id_array: array() 
			),
		'parent_id' => array(
				'label' => 'List dropdown',
				'id' => 'parent_id',
				'name' => 'parent_id',
				'type' => 'select',
				'placeholder' => 'Choose category',
				'options' => isset($parent_id_array) ?  $parent_id_array: array() 
			),
		'subcat_id' => array(
				'label' => 'Grouping Selection',
				'id' => 'subcat_id',
				'name' => 'subcat_id',
				'type' => 'select',
				'placeholder' => 'Choose category',
				'options' => isset($subcat_id_array) ?  $subcat_id_array: array() 
			),
		'page_id' => array(
				'label' => 'Page Reference',
				'id' => 'page_id',
				'name' => 'page_id',
				'type' => 'select',
				'placeholder' => 'Choose Page',
				'options' => isset($page_id_array) ?  $page_id_array: array() 
			),
		'self_id' => array(
				'label' => 'Self Parent Reference',
				'id' => 'self_id',
				'name' => 'self_id',
				'type' => 'select',
				'placeholder' => 'Choose Parent',
				'options' => isset($self_id_array) ?  $self_id_array: array() 
			),
		'self_id_division' => array(
				'label' => 'Self Parent Reference',
				'id' => 'self_id_division',
				'name' => 'self_id_division',
				'type' => 'select',
				'placeholder' => 'Choose Parent',
				'options' => isset($self_id_division_array) ?  $self_id_division_array: array() 
			),
		'news_categorizing_id' => array(
				'label' => 'news Category',
				'id' => 'news_categorizing_id',
				'name' => 'news_categorizing_id',
				'type' => 'select',
				'placeholder' => 'Choose News category',
				'options' => isset($news_categorizing_id_array) ?  $news_categorizing_id_array: array() 
			),
		'department_id' => array(
				'label' => 'Corporate Department',
				'id' => 'department_id',
				'name' => 'department_id',
				'type' => 'select',
				'placeholder' => 'Corporate Department',
				'options' => isset($department_id_array) ?  $department_id_array: array() 
			),
		'complaints_division_id' => array(
				'label' => 'news Category',
				'id' => 'complaints_division_id',
				'name' => 'complaints_division_id',
				'type' => 'select',
				'placeholder' => 'Choose News category',
				'options' => isset($complaints_division_id_array) ?  $complaints_division_id_array: array() 
			),
		'product_category_id' => array(
				'label' => 'Prizing Groups',
				'id' => 'product_category_id',
				'name' => 'product_category_id',
				'type' => 'select',
				'placeholder' => 'Choose News category',
				'options' => isset($product_category_id_array) ?  $product_category_id_array: array() 
			),
		'staff_in_charge_id' => array(
				'label' => 'Responsible Staff if any',
				'id' => 'staff_in_charge_id',
				'name' => 'staff_in_charge_id',
				'type' => 'select',
				'placeholder' => 'Choose News category',
				'options' => isset($staff_in_charge_id_array) ?  $staff_in_charge_id_array: array() 
			),
		'location' => array(
				'label' => 'Location in Accra Only',
				'id' => 'location',
				'name' => 'location',
				'type' => 'select',
				'placeholder' => 'Choose News category',
				'options' => isset($location_array) ?  $location_array: array() 
			),
		'status' => array(
				'label' => 'Status',
				'id' => 'status',
				'name' => 'status',
				'type' => 'select',
				'placeholder' => 'Select Status',
				'switch' => array(
						'Active' => 'Active',
						'Non-Active' => 'Non-Active'
					)
			),
		'slider_type' => array(
				'label' => 'Slider Type',
				'id' => 'slider_type',
				'name' => 'slider_type',
				'type' => 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'image' => 'Image',
						'uploaded_video' => 'Uploaded-Video',
						'youtube_video' => 'Youtube Video',
					)
			),
		'gender' => array(
				'label' => 'Sex',
				'id' => 'gender',
				'name' => 'gender',
				'type' => 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'male' 		=> 'male',
						'female' 	=> 'female',
					)
			),
		'bulleting_type' => array(
				'label' => 'Type',
				'id' => 'bulleting_type',
				'name' => 'bulleting_type',
				'type' => 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'list' 					=> 'List',
						'BoxAwsome' 			=> 'BoxAwsome',
						'Box' 					=> 'Box',
						'icon_picture' 			=> 'Icon Picture',
						'image-details' 		=> 'Image &amp; Details',
						'side-info' 			=> 'SIde info',
						'image-details-image' 	=> 'Image &amp; Details &amp; Image'
					)
			),
		'partispation' => array(
				'label' => 'Parteners Link to US',
				'id' => 'partispation',
				'name' => 'partispation',
				'type' => 'select',
				'placeholder' => 'Select partispation',
				'options' => array(
						'Client' 					=> 'Major Client',
						'In-Agreement-With' 	=> 'In Agreement With(Partener)',
					)
			),
		'price_value' => array(
				'label' => 'type',
				'id' => 'price_value',
				'name' => 'price_value',
				'type' => 'select',
				'placeholder' => 'Select price_value',
				'options' => array(
						'rent' => 'rent',
						'sale' 	=> 'sale',
						'service' 	=> 'service',
					)
			),
		'template' => array(
				'label' => 'Type',
				'id' => 'template',
				'name' => 'template',
				'type' => 'select',
				'placeholder' => 'Select template',
				'options' => array(
						'Page' 					=> 'Page',
						'Gallery' 				=> 'Gallery',
						'Download' 				=> 'Download',
					)
			),
		'footer_include' => array(
				'label' => 'Footer Include',
				'id' => 'footer_include',
				'name' => 'footer_include',
				'type' => 'select',
				'placeholder' => 'Select template',
				'options' => array(
						'None' 					=> 'None',
						'Dropdown-list' 		=> 'Dropdown-list',
						'Page-info' 			=> 'Page-info',
					)
			),
		'dropdown_style' => array(
				'label' => 'Navigation Dropdown Style',
				'id' => 'dropdown_style',
				'name' => 'dropdown_style',
				'type' => 'select',
				'placeholder' => 'Select template',
				'options' => array(
						'None' 				=> 'None',
						'list' 				=> 'list',
						'Group' 			=> 'Group',
						'list-in-groups' 	=> 'list-in-groups',
					)
			),
		'level' => array(
				'label' => 'level',
				'id' 	=> 'level',
				'name' 	=> 'level',
				'type' 	=> 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'High' 					=> 'High',
						'Neutural' 				=> 'Neutural',
						'Low' 					=> 'Low',
					)
			),
		'headlines_slider' => array(
				'label' => 'Headlines Slider',
				'id' => 'headlines_slider',
				'name' => 'headlines_slider',
				'type' => 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'Yes'	 	=> 'Yes',
						'No' 		=> 'No'
					)
			),
		'sidebar_show' 	=> array(
				'label' => 'Sidebar Show',
				'id' 	=> 'sidebar_show',
				'name' 	=> 'sidebar_show',
				'type' 	=> 'select',
				'placeholder' => 'Select Status',
				'options' => array(
						'Yes' 	=> 'Yes',
						'No' 	=> 'No'
					)
			),
		'password' => array(
				'label' 	=> 'Enter Password',
				'id' 		=> 'password',
				'name' 		=> 'password',
				'type' 		=> 'password',
			),
	);
}

function view_records ($array)
{
	$nonDisplay = array("id" , "password" , "rights" , "company_id");
	$picture_display = array("house_picture" , "sensers_layout_pictures" , "logo" , "photo");
	foreach ($array as $key => $value) {
		if (in_array($key , $nonDisplay)) {
			//display None
		} else if (in_array($key , $picture_display)) {
			echo "<div class='input-group'>"; 
				echo "<label for='".$key."'>".self_humanize($key)."</label> : "; 
                	echo empty($value) ? '<div class="form-control"> Empty '.self_humanize($key)."</div>" : '<p><img src="' . Base_url($value) . '" style="height:200px;" /></p>'; 
			echo "</div>";
		} else if ($key == "gps") {
			?>
                <div><h1>GPS Map</h1></div> 
                <span id='locator'><?php echo $value; ?></span>
                <div id="map"></div>
            <?php
		} else {
			echo "<div class='input-group'>"; 
  				echo "<label>".self_humanize($key)."</label>"; 
  				echo "<div class='form-control'>";
  				echo empty($value) ? "" : $value;
  				echo "</div>"; 
			echo "</div>";
		}
	}
}
