<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function d_submit ($pending)
{
    if ($pending > 0) { ?>
        <center>
            <h4 class="header-title m-t-0 m-b-30">You have a Pending Request</h4>
        </center>
    <?php } else { ?>

    <div class="row">
        <div class="form-group text-right m-b-0">
            <button class="btn btn-primary waves-effect waves-light" type="submit">
                Submit
            </button>
            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                Cancel
            </button>
        </div>
    </div>

    <?php }
}
/*
function div_clean_render ($data = null)
{
    
    foreach( $data as $kin => $spouse ){ ?>
        <div class=" col-md-12" style="float:right;">
            <label><?=humanize("<h4>".$kin."</h4>")?></label>
            <div class="row">
                <?php if( is_object($spouse)  ){ ?>
                    <?php foreach( $spouse as $kin_inner => $spouse_inner ){ ?>
                        <div class=" col-md-6" style="float:left;">
                            <label><?=humanize("<h5>".$kin_inner."</h5>")?></label>
                            <div class="row">
                                <?php if( is_array($spouse_inner) ){ ?>
                                    <?php foreach( $spouse_inner as $kin_inner_two => $spouse_inner_two ){ ?>
                                        <div class=" col-md-6" style="float:left;">
                                            <label><?=$kin_inner_two?></label>

                                            <?php if( is_object($spouse_inner_two) ){ ?>
                                                <?php foreach( $spouse_inner_two as $kin_inner_three => $spouse_inner_three ){ ?>
                                                    <div class=" col-md-12" style="float:left;">
                                                        <label><?=$kin_inner_three?></label>
                                                        <?php if($kin_inner_three == "picture") { ?>
                                                            <span class="form-control" > <?=$spouse_inner_three  == "" ? "none" : "<a  target='_blank' href='".base_url($spouse_inner_three)."' ><img style='width:100%;' src='".base_url($spouse_inner_three)."'/></a>" ?> </span>
                                                        <?php } else { ?>
                                                            <span class="form-control" > <?=$spouse_inner_three  == "" ? "none" : $spouse_inner_three ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <span class="form-control" > <?=$spouse_inner_two == "" ? "none" : $spouse_inner_two ?> </span>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <span class="form-control" > <?=$spouse_inner == "" ? "none" : $spouse_inner ?> </span>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <span class="form-control" > <?=$spouse == "" ? "none" : $spouse ?> </span>
                <?php } ?>
            </div>
        </div>
    <?php } 
}

*/

function _cleaner($var = null){
	$reform = array(
		"0" 				=> "",
		"1" 				=> "",
		"2" 				=> "",
		"3" 				=> "",
		"4" 				=> "",
		"5" 				=> "",
		"6" 				=> "",
		"7" 				=> "",
		"8" 				=> "",
		"9" 				=> "",
		"10" 				=> "",
		"grades" 			=> "",
		"others" 			=> "",
		"user" 				=> "Personal details <div id='profile'></div>",
		"emergency" 		=> "Emergency contact",
		"properties" 		=> "Personal details continuation",
		"pursue" 			=> "Why do you wish to pursue this programme you have chosen",
		"impression" 		=> "Please give a frank impression of yourself, including what you believe to be your main strengths and weaknesses ",
		"achievement" 		=> "What do you consider your most significant achievement to date? why is it significant to you?",
	);
	if(array_key_exists($var , $reform)){
		return humanize($reform[$var]);
	}
	return humanize($var);
}


function div_clean_render($data = null , $bind = "div class='col-md-6'", $indent = array( "div",  "class='col-md-6") ) {
        
    
	$skip = array(
		"others" 			=> "",
		"emergency" 		=> "",
		"reference" 		=> "",
		"grades" 		    => "",
		"qualification" 	=> "",
		"experience" 		=> "",
		
    );
    $special = array(
		"grades_lable" 		=> "",
		"grades_grades"     => "",
    );
    
	foreach($data as $key => $value){
		if ( (is_array($value) || is_object($value)) && !array_key_exists($key , $skip) ){
			echo    "<div class='col-md-6' style='float:right;'><div class='row'><h3>"._cleaner($key)."</h3></div>";
			div_clean_render((object)$value);
        } else if(is_array($value) || is_object($value)){
			echo    "<div class='row col-md-12'><div class=' col-md-12'><h3>"._cleaner($key)."</h3></div>";
			div_clean_render((object)$value);
        }
        

        if((is_string($value)) && !array_key_exists($key , $special) ){
            if(startsWith($value , "./")){
                echo "<".$bind." class='pagebreak'><".$indent[0]."> Image attached </".$indent[0]."></".$bind.">";
            } else {
                echo "<div class='row'>";
                    echo "<div class='col-md-6'>";
                        echo _cleaner($key);
                    echo "</div>";

                    echo "<div class='col-md-6'>";
                        echo "<b>".strtoupper($value)."</b>";
                    echo "</div>";
                echo "</div>";
            }
        } else if((is_string($value)) && array_key_exists($key , $special) ){
                echo "<div class='col-md-6' style='float:left;'>";
                    echo "<b>".strtoupper($value)."</b>";
                echo "</div>";
        }

		if ( (is_array($value) || is_object($value)) ){
			echo "</div>";
        }
		
	}
}