<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function curl_request($url , $data ) {
        $CI = & get_instance();
        $session = $CI->session;
        $data["employee"] = $session->userdata('employee');
        
	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 
	header('Content-type: application/pdf');

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  


	//this option avoid retrieving HTTP response headers in answer
	//curl_setopt($ch, CURLOPT_HEADER, 0);
	
	//execute request
	curl_exec($ch);
}


function curl_request_returns($url , $data) {


	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  

	curl_setopt($ch, CURLOPT_HEADER, 0);
	//Get data to send
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
	//execute request
	return curl_exec($ch);
}

function curl_request_init($url , $data , $div = null) {


	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  

	curl_setopt($ch, CURLOPT_HEADER, 0);
	//Get data to send
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 


	/*execute request
	$html_dom = new simple_html_dom();
	$html_dom->load(curl_exec($ch));
	return $html_dom->find('[name="suggest"]',0)->value;
	*/
	return curl_exec($ch);
}

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);

    return $length === 0 || 
    (substr($haystack, -$length) === $needle);
}

function _rl($var = null , $user_picture = ""){
	$reform = array(
		"0" 				=> "info",
		"1" 				=> "info",
		"others" 			=> "",
		"user" 				=> "Personal details <img src='".$user_picture."' class='profile'>",
		"emergency" 		=> "Emergency contact",
		"properties" 		=> "Personal details continuation",
		"pursue" 			=> "Why do you wish to pursue this programme you have chosen",
		"impression" 		=> "Please give a frank impression of yourself, including what you believe to be your main strengths and weaknesses ",
		"achievement" 		=> "What do you consider your most significant achievement to date? why is it significant to you?",
	);
	if(array_key_exists($var , $reform)){
		return $reform[$var];
	}
	return humanize($var);
}
function repeat_self($user_picture = "", $data = null , $bind = "tr", $indent = array( "td",  "style='width:33%; padding:5px;'") ) {
	
	$skip = array(
		"others" 			=> "",
		"emergency" 		=> "",
		"reference" 		=> "",
		"qualification" 	=> "",
		"experience" 		=> "",
		
	);
	
	foreach($data as $key => $value){
		if((is_array($value) || is_object($value)) && !array_key_exists($key , $skip) ){
			$cal = count((array)$value)+1;
			$colspan = 'rowspan="'.$cal.'"';
			echo "<".$bind."><".$indent[0]." colspan='4' style='height:35px;'></".$indent[0]."></".$bind.">";
			echo "<".$bind."  style='".$indent[1]."'>
				<".$indent[0]." ".$colspan." valign='top' ><h3>"._rl($key , $user_picture)."</h3></".$indent[0].">
				<".$indent[0]."></".$indent[0].">
				<".$indent[0]."></".$indent[0].">";
			repeat_self($user_picture , (object)$value);
		} else if ( (is_array($value) || is_object($value)) ){
			echo "<".$bind." style='".$indent[1]."' >
				<".$indent[0]." colspan='4' ><h3>"._rl($key)."</h3></".$indent[0].">
				</".$bind.">";
			repeat_self($user_picture , (object)$value);
		}


		if(is_string($value)){
			if(startsWith($value , "./")){
				echo "<".$bind." class='pagebreak'><".$indent[0]." colspan='4'> Image attached </".$indent[0]."></".$bind.">";
			} else {
				echo "<".$bind.">";
					echo "<".$indent[0]." ".$indent[1].">";
						echo _rl($key);
					echo "</".$indent[0].">";

					echo "<".$indent[0].">";
					echo "</".$indent[0].">";

					echo "<".$indent[0]." ".$indent[1].">";
						echo "<b>".strtoupper($value)."</b>";
					echo "</".$indent[0].">";
				echo "</".$bind.">";
			}
		} 

		

		
		if((is_array($value) || is_object($value)) && !array_key_exists($key , $skip) ){
			echo "</".$bind.">";
		}
		
	}
}

function img_only($data = null ) {

	foreach($data as $key => $value){
		
		if ( (is_array($value) || is_object($value)) ){
			img_only((object)$value);
		}
		if(is_string($value)){
			if(startsWith($value , "./")){
				echo "<div class='pagebreak' >".img(array('src'=>base_url($value) , 'style'=>'height: 250mm;'))."</div>";
			}
		} 
	}
}


?>

