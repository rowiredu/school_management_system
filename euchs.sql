-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 20, 2020 at 11:50 AM
-- Server version: 5.7.31-0ubuntu0.18.04.1-log
-- PHP Version: 5.6.40-29+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `entrance_university`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_year_with_fee`
--

CREATE TABLE `academic_year_with_fee` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `year` varchar(100) NOT NULL,
  `start` varchar(100) NOT NULL,
  `end` varchar(100) NOT NULL,
  `gh_currency` varchar(100) NOT NULL,
  `gh_amount` varchar(100) NOT NULL,
  `fr_currency` varchar(100) NOT NULL,
  `fr_amount` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academic_year_with_fee`
--

INSERT INTO `academic_year_with_fee` (`id`, `user_id`, `year`, `start`, `end`, `gh_currency`, `gh_amount`, `fr_currency`, `fr_amount`, `created`, `modified`) VALUES
(1, 1, '2020/2021', '10/01/2020', '12/31/2020', 'GHC', '1500', 'USD', '2000', '2020-10-18 20:22:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `app_forms`
--

CREATE TABLE `app_forms` (
  `id` int(11) UNSIGNED NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `status` text NOT NULL,
  `json_values` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `active_form` varchar(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) UNSIGNED NOT NULL,
  `iso` varchar(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) DEFAULT NULL,
  `numcode` int(8) DEFAULT NULL,
  `phonecode` varchar(5) DEFAULT NULL,
  `iso3` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `numcode`, `phonecode`, `iso3`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 4, '93', 'AFG'),
(2, 'AL', 'ALBANIA', 'Albania', 8, '355', 'ALB'),
(3, 'DZ', 'ALGERIA', 'Algeria', 12, '213', 'DZA'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 16, '1684', 'ASM'),
(5, 'AD', 'ANDORRA', 'Andorra', 20, '376', 'AND'),
(6, 'AO', 'ANGOLA', 'Angola', 24, '244', 'AGO'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 660, '1264', 'AIA'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, '0', ''),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 28, '1268', 'ATG'),
(10, 'AR', 'ARGENTINA', 'Argentina', 32, '54', 'ARG'),
(11, 'AM', 'ARMENIA', 'Armenia', 51, '374', 'ARM'),
(12, 'AW', 'ARUBA', 'Aruba', 533, '297', 'ABW'),
(13, 'AU', 'AUSTRALIA', 'Australia', 36, '61', 'AUS'),
(14, 'AT', 'AUSTRIA', 'Austria', 40, '43', 'AUT'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 31, '994', 'AZE'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 44, '1242', 'BHS'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 48, '973', 'BHR'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 50, '880', 'BGD'),
(19, 'BB', 'BARBADOS', 'Barbados', 52, '1246', 'BRB'),
(20, 'BY', 'BELARUS', 'Belarus', 112, '375', 'BLR'),
(21, 'BE', 'BELGIUM', 'Belgium', 56, '32', 'BEL'),
(22, 'BZ', 'BELIZE', 'Belize', 84, '501', 'BLZ'),
(23, 'BJ', 'BENIN', 'Benin', 204, '229', 'BEN'),
(24, 'BM', 'BERMUDA', 'Bermuda', 60, '1441', 'BMU'),
(25, 'BT', 'BHUTAN', 'Bhutan', 64, '975', 'BTN'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 68, '591', 'BOL'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 70, '387', 'BIH'),
(28, 'BW', 'BOTSWANA', 'Botswana', 72, '267', 'BWA'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, '0', ''),
(30, 'BR', 'BRAZIL', 'Brazil', 76, '55', 'BRA'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, '246', ''),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 96, '673', 'BRN'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 100, '359', 'BGR'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 854, '226', 'BFA'),
(35, 'BI', 'BURUNDI', 'Burundi', 108, '257', 'BDI'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 116, '855', 'KHM'),
(37, 'CM', 'CAMEROON', 'Cameroon', 120, '237', 'CMR'),
(38, 'CA', 'CANADA', 'Canada', 124, '1', 'CAN'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 132, '238', 'CPV'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 136, '1345', 'CYM'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 140, '236', 'CAF'),
(42, 'TD', 'CHAD', 'Chad', 148, '235', 'TCD'),
(43, 'CL', 'CHILE', 'Chile', 152, '56', 'CHL'),
(44, 'CN', 'CHINA', 'China', 156, '86', 'CHN'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, '61', ''),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, '672', ''),
(47, 'CO', 'COLOMBIA', 'Colombia', 170, '57', 'COL'),
(48, 'KM', 'COMOROS', 'Comoros', 174, '269', 'COM'),
(49, 'CG', 'CONGO', 'Congo', 178, '242', 'COG'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 180, '242', 'COD'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 184, '682', 'COK'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 188, '506', 'CRI'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 384, '225', 'CIV'),
(54, 'HR', 'CROATIA', 'Croatia', 191, '385', 'HRV'),
(55, 'CU', 'CUBA', 'Cuba', 192, '53', 'CUB'),
(56, 'CY', 'CYPRUS', 'Cyprus', 196, '357', 'CYP'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 203, '420', 'CZE'),
(58, 'DK', 'DENMARK', 'Denmark', 208, '45', 'DNK'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 262, '253', 'DJI'),
(60, 'DM', 'DOMINICA', 'Dominica', 212, '1767', 'DMA'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 214, '1809', 'DOM'),
(62, 'EC', 'ECUADOR', 'Ecuador', 218, '593', 'ECU'),
(63, 'EG', 'EGYPT', 'Egypt', 818, '20', 'EGY'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 222, '503', 'SLV'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 226, '240', 'GNQ'),
(66, 'ER', 'ERITREA', 'Eritrea', 232, '291', 'ERI'),
(67, 'EE', 'ESTONIA', 'Estonia', 233, '372', 'EST'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 231, '251', 'ETH'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 238, '500', 'FLK'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 234, '298', 'FRO'),
(71, 'FJ', 'FIJI', 'Fiji', 242, '679', 'FJI'),
(72, 'FI', 'FINLAND', 'Finland', 246, '358', 'FIN'),
(73, 'FR', 'FRANCE', 'France', 250, '33', 'FRA'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 254, '594', 'GUF'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 258, '689', 'PYF'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, '0', ''),
(77, 'GA', 'GABON', 'Gabon', 266, '241', 'GAB'),
(78, 'GM', 'GAMBIA', 'Gambia', 270, '220', 'GMB'),
(79, 'GE', 'GEORGIA', 'Georgia', 268, '995', 'GEO'),
(80, 'DE', 'GERMANY', 'Germany', 276, '49', 'DEU'),
(81, 'GH', 'GHANA', 'Ghana', 288, '233', 'GHA'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 292, '350', 'GIB'),
(83, 'GR', 'GREECE', 'Greece', 300, '30', 'GRC'),
(84, 'GL', 'GREENLAND', 'Greenland', 304, '299', 'GRL'),
(85, 'GD', 'GRENADA', 'Grenada', 308, '1473', 'GRD'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 312, '590', 'GLP'),
(87, 'GU', 'GUAM', 'Guam', 316, '1671', 'GUM'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 320, '502', 'GTM'),
(89, 'GN', 'GUINEA', 'Guinea', 324, '224', 'GIN'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 624, '245', 'GNB'),
(91, 'GY', 'GUYANA', 'Guyana', 328, '592', 'GUY'),
(92, 'HT', 'HAITI', 'Haiti', 332, '509', 'HTI'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, '0', ''),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 336, '39', 'VAT'),
(95, 'HN', 'HONDURAS', 'Honduras', 340, '504', 'HND'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 344, '852', 'HKG'),
(97, 'HU', 'HUNGARY', 'Hungary', 348, '36', 'HUN'),
(98, 'IS', 'ICELAND', 'Iceland', 352, '354', 'ISL'),
(99, 'IN', 'INDIA', 'India', 356, '91', 'IND'),
(100, 'ID', 'INDONESIA', 'Indonesia', 360, '62', 'IDN'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 364, '98', 'IRN'),
(102, 'IQ', 'IRAQ', 'Iraq', 368, '964', 'IRQ'),
(103, 'IE', 'IRELAND', 'Ireland', 372, '353', 'IRL'),
(104, 'IL', 'ISRAEL', 'Israel', 376, '972', 'ISR'),
(105, 'IT', 'ITALY', 'Italy', 380, '39', 'ITA'),
(106, 'JM', 'JAMAICA', 'Jamaica', 388, '1876', 'JAM'),
(107, 'JP', 'JAPAN', 'Japan', 392, '81', 'JPN'),
(108, 'JO', 'JORDAN', 'Jordan', 400, '962', 'JOR'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 398, '7', 'KAZ'),
(110, 'KE', 'KENYA', 'Kenya', 404, '254', 'KEN'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 296, '686', 'KIR'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 408, '850', 'PRK'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 410, '82', 'KOR'),
(114, 'KW', 'KUWAIT', 'Kuwait', 414, '965', 'KWT'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 417, '996', 'KGZ'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 418, '856', 'LAO'),
(117, 'LV', 'LATVIA', 'Latvia', 428, '371', 'LVA'),
(118, 'LB', 'LEBANON', 'Lebanon', 422, '961', 'LBN'),
(119, 'LS', 'LESOTHO', 'Lesotho', 426, '266', 'LSO'),
(120, 'LR', 'LIBERIA', 'Liberia', 430, '231', 'LBR'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 434, '218', 'LBY'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 438, '423', 'LIE'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 440, '370', 'LTU'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 442, '352', 'LUX'),
(125, 'MO', 'MACAO', 'Macao', 446, '853', 'MAC'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 807, '389', 'MKD'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 450, '261', 'MDG'),
(128, 'MW', 'MALAWI', 'Malawi', 454, '265', 'MWI'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 458, '60', 'MYS'),
(130, 'MV', 'MALDIVES', 'Maldives', 462, '960', 'MDV'),
(131, 'ML', 'MALI', 'Mali', 466, '223', 'MLI'),
(132, 'MT', 'MALTA', 'Malta', 470, '356', 'MLT'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 584, '692', 'MHL'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 474, '596', 'MTQ'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 478, '222', 'MRT'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 480, '230', 'MUS'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, '269', ''),
(138, 'MX', 'MEXICO', 'Mexico', 484, '52', 'MEX'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 583, '691', 'FSM'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 498, '373', 'MDA'),
(141, 'MC', 'MONACO', 'Monaco', 492, '377', 'MCO'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 496, '976', 'MNG'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 500, '1664', 'MSR'),
(144, 'MA', 'MOROCCO', 'Morocco', 504, '212', 'MAR'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 508, '258', 'MOZ'),
(146, 'MM', 'MYANMAR', 'Myanmar', 104, '95', 'MMR'),
(147, 'NA', 'NAMIBIA', 'Namibia', 516, '264', 'NAM'),
(148, 'NR', 'NAURU', 'Nauru', 520, '674', 'NRU'),
(149, 'NP', 'NEPAL', 'Nepal', 524, '977', 'NPL'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 528, '31', 'NLD'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 530, '599', 'ANT'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 540, '687', 'NCL'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 554, '64', 'NZL'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 558, '505', 'NIC'),
(155, 'NE', 'NIGER', 'Niger', 562, '227', 'NER'),
(156, 'NG', 'NIGERIA', 'Nigeria', 566, '234', 'NGA'),
(157, 'NU', 'NIUE', 'Niue', 570, '683', 'NIU'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 574, '672', 'NFK'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 580, '1670', 'MNP'),
(160, 'NO', 'NORWAY', 'Norway', 578, '47', 'NOR'),
(161, 'OM', 'OMAN', 'Oman', 512, '968', 'OMN'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 586, '92', 'PAK'),
(163, 'PW', 'PALAU', 'Palau', 585, '680', 'PLW'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, '970', ''),
(165, 'PA', 'PANAMA', 'Panama', 591, '507', 'PAN'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 598, '675', 'PNG'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 600, '595', 'PRY'),
(168, 'PE', 'PERU', 'Peru', 604, '51', 'PER'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 608, '63', 'PHL'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 612, '0', 'PCN'),
(171, 'PL', 'POLAND', 'Poland', 616, '48', 'POL'),
(172, 'PT', 'PORTUGAL', 'Portugal', 620, '351', 'PRT'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 630, '1787', 'PRI'),
(174, 'QA', 'QATAR', 'Qatar', 634, '974', 'QAT'),
(175, 'RE', 'REUNION', 'Reunion', 638, '262', 'REU'),
(176, 'RO', 'ROMANIA', 'Romania', 642, '40', 'ROM'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 643, '70', 'RUS'),
(178, 'RW', 'RWANDA', 'Rwanda', 646, '250', 'RWA'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 654, '290', 'SHN'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 659, '1869', 'KNA'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 662, '1758', 'LCA'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 666, '508', 'SPM'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 670, '1784', 'VCT'),
(184, 'WS', 'SAMOA', 'Samoa', 882, '684', 'WSM'),
(185, 'SM', 'SAN MARINO', 'San Marino', 674, '378', 'SMR'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 678, '239', 'STP'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 682, '966', 'SAU'),
(188, 'SN', 'SENEGAL', 'Senegal', 686, '221', 'SEN'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, '381', ''),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 690, '248', 'SYC'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 694, '232', 'SLE'),
(192, 'SG', 'SINGAPORE', 'Singapore', 702, '65', 'SGP'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 703, '421', 'SVK'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 705, '386', 'SVN'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 90, '677', 'SLB'),
(196, 'SO', 'SOMALIA', 'Somalia', 706, '252', 'SOM'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 710, '27', 'ZAF'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, '0', ''),
(199, 'ES', 'SPAIN', 'Spain', 724, '34', 'ESP'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 144, '94', 'LKA'),
(201, 'SD', 'SUDAN', 'Sudan', 736, '249', 'SDN'),
(202, 'SR', 'SURINAME', 'Suriname', 740, '597', 'SUR'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 744, '47', 'SJM'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 748, '268', 'SWZ'),
(205, 'SE', 'SWEDEN', 'Sweden', 752, '46', 'SWE'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 756, '41', 'CHE'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 760, '963', 'SYR'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 158, '886', 'TWN'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 762, '992', 'TJK'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 834, '255', 'TZA'),
(211, 'TH', 'THAILAND', 'Thailand', 764, '66', 'THA'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, '670', ''),
(213, 'TG', 'TOGO', 'Togo', 768, '228', 'TGO'),
(214, 'TK', 'TOKELAU', 'Tokelau', 772, '690', 'TKL'),
(215, 'TO', 'TONGA', 'Tonga', 776, '676', 'TON'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 780, '1868', 'TTO'),
(217, 'TN', 'TUNISIA', 'Tunisia', 788, '216', 'TUN'),
(218, 'TR', 'TURKEY', 'Turkey', 792, '90', 'TUR'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 795, '7370', 'TKM'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 796, '1649', 'TCA'),
(221, 'TV', 'TUVALU', 'Tuvalu', 798, '688', 'TUV'),
(222, 'UG', 'UGANDA', 'Uganda', 800, '256', 'UGA'),
(223, 'UA', 'UKRAINE', 'Ukraine', 804, '380', 'UKR'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 784, '971', 'ARE'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 826, '44', 'GBR'),
(226, 'US', 'UNITED STATES', 'United States', 840, '1', 'USA'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, '1', ''),
(228, 'UY', 'URUGUAY', 'Uruguay', 858, '598', 'URY'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 860, '998', 'UZB'),
(230, 'VU', 'VANUATU', 'Vanuatu', 548, '678', 'VUT'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 862, '58', 'VEN'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 704, '84', 'VNM'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 92, '1284', 'VGB'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 850, '1340', 'VIR'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 876, '681', 'WLF'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 732, '212', 'ESH'),
(237, 'YE', 'YEMEN', 'Yemen', 887, '967', 'YEM'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 894, '260', 'ZMB'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 716, '263', 'ZWE');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) UNSIGNED NOT NULL,
  `courses` varchar(1024) NOT NULL DEFAULT '[{"wassce":["English","Core Mathematics","Integrated Science","Social Studies","Further Mathematics","Biology","Physics","Chemistry","Geography","ICT","French","Music"],"a_level":["Mathematics","English","Science","Social Studies","Chemistry","Physics","Elective Mathematics"]}]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `courses`) VALUES
(1, '[{\"wassce\":[\"English\",\"Core Mathematics\",\"Integrated Science\",\"Social Studies\",\"Further Mathematics\",\"Biology\",\"Physics\",\"Chemistry\",\"Geography\",\"ICT\",\"French\",\"Music\"],\"a_level\":[\"Mathematics\",\"English\",\"Science\",\"Social Studies\",\"Chemistry\",\"Physics\",\"Elective Mathematics\"]}]');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created`, `modified`) VALUES
(1, 'admin', 'Administrator', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'cashier', 'Accountant', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'teller', 'Banks', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`, `created`, `modified`) VALUES
(5, '::1', 'alex@gmail.com', 1603062163, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(27);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `bank_ref_code` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `year_id` varchar(11) NOT NULL,
  `serial_number` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `reference` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `email` varchar(254) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `bio` text,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `username`, `password`, `active`, `salt`, `picture`, `phone`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `ip_address`, `remember_code`, `bio`, `last_login`, `last_logout`, `created_on`, `created`, `modified`) VALUES
(1, 'emmanuel@zentechgh.com', 'Emmanuel', 'Dadzie', 'administrator', '$2b$10$PE.6a7Tdelpcn/VjdN.7i.zrkL40uM9qGFbd5vlkn1m3NJ6/oNt.6', 1, '', NULL, '0209137654', '', '', NULL, '127.0.0.1', NULL, NULL, 1603050132, NULL, 0, '0000-00-00 00:00:00', NULL),
(2, 'rowiredu.ing@gmail.com', 'richard', 'owiredu', 'rowiredu.ing@gmail.com', '$2b$10$PE.6a7Tdelpcn/VjdN.7i.zrkL40uM9qGFbd5vlkn1m3NJ6/oNt.6', 1, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, 1603048816, NULL, 0, '2020-10-18 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`, `created`, `modified`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_applicant`
--

CREATE TABLE `user_applicant` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `email` varchar(254) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `date_of_birth` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `bio` text,
  `json_encoded_prefield` text,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `phone_code` int(11) DEFAULT '233'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year_with_fee`
--
ALTER TABLE `academic_year_with_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_forms`
--
ALTER TABLE `app_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_applicant`
--
ALTER TABLE `user_applicant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year_with_fee`
--
ALTER TABLE `academic_year_with_fee`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_forms`
--
ALTER TABLE `app_forms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_applicant`
--
ALTER TABLE `user_applicant`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
